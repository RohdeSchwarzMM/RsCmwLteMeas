Maximum
----------------------------------------



.. rubric:: SCPI Commands :

.. index::
	single: READ:LTE:MEASurement<Instance>:MEValuation:PDYNamics:MAXimum
	single: FETCh:LTE:MEASurement<Instance>:MEValuation:PDYNamics:MAXimum
	single: CALCulate:LTE:MEASurement<Instance>:MEValuation:PDYNamics:MAXimum

.. code-block:: python

	READ:LTE:MEASurement<Instance>:MEValuation:PDYNamics:MAXimum
	FETCh:LTE:MEASurement<Instance>:MEValuation:PDYNamics:MAXimum
	CALCulate:LTE:MEASurement<Instance>:MEValuation:PDYNamics:MAXimum



.. autoclass:: RsCmwLteMeas.Implementations.MultiEval.Pdynamics.Maximum.MaximumCls
	:members:
	:undoc-members:
	:noindex: