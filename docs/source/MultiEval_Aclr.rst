Aclr
----------------------------------------





.. autoclass:: RsCmwLteMeas.Implementations.MultiEval.Aclr.AclrCls
	:members:
	:undoc-members:
	:noindex:


.. rubric:: Cloning the Group

.. code-block:: python

	# Create a clone of the original group, that exists independently
	group2 = driver.multiEval.aclr.clone()



.. rubric:: Subgroups
.. toctree::
	:maxdepth: 6
	:glob:

	MultiEval_Aclr_Average.rst
	MultiEval_Aclr_Current.rst
	MultiEval_Aclr_Dallocation.rst
	MultiEval_Aclr_DchType.rst