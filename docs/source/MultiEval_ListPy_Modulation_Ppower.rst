Ppower
----------------------------------------





.. autoclass:: RsCmwLteMeas.Implementations.MultiEval.ListPy.Modulation.Ppower.PpowerCls
	:members:
	:undoc-members:
	:noindex:


.. rubric:: Cloning the Group

.. code-block:: python

	# Create a clone of the original group, that exists independently
	group2 = driver.multiEval.listPy.modulation.ppower.clone()



.. rubric:: Subgroups
.. toctree::
	:maxdepth: 6
	:glob:

	MultiEval_ListPy_Modulation_Ppower_Average.rst
	MultiEval_ListPy_Modulation_Ppower_Current.rst
	MultiEval_ListPy_Modulation_Ppower_Maximum.rst
	MultiEval_ListPy_Modulation_Ppower_Minimum.rst
	MultiEval_ListPy_Modulation_Ppower_StandardDev.rst