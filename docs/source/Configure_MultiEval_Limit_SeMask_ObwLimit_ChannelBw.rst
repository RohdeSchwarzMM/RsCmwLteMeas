ChannelBw<ChannelBw>
----------------------------------------

.. rubric:: RepCap Settings

.. code-block:: python

	# Range: Bw14 .. Bw200
	rc = driver.configure.multiEval.limit.seMask.obwLimit.channelBw.repcap_channelBw_get()
	driver.configure.multiEval.limit.seMask.obwLimit.channelBw.repcap_channelBw_set(repcap.ChannelBw.Bw14)



.. rubric:: SCPI Command :

.. index::
	single: CONFigure:LTE:MEASurement<Instance>:MEValuation:LIMit:SEMask:OBWLimit:CBANdwidth<Band>

.. code-block:: python

	CONFigure:LTE:MEASurement<Instance>:MEValuation:LIMit:SEMask:OBWLimit:CBANdwidth<Band>



.. autoclass:: RsCmwLteMeas.Implementations.Configure.MultiEval.Limit.SeMask.ObwLimit.ChannelBw.ChannelBwCls
	:members:
	:undoc-members:
	:noindex:


.. rubric:: Cloning the Group

.. code-block:: python

	# Create a clone of the original group, that exists independently
	group2 = driver.configure.multiEval.limit.seMask.obwLimit.channelBw.clone()