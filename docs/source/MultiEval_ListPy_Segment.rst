Segment<Segment>
----------------------------------------

.. rubric:: RepCap Settings

.. code-block:: python

	# Range: Nr1 .. Nr128
	rc = driver.multiEval.listPy.segment.repcap_segment_get()
	driver.multiEval.listPy.segment.repcap_segment_set(repcap.Segment.Nr1)





.. autoclass:: RsCmwLteMeas.Implementations.MultiEval.ListPy.Segment.SegmentCls
	:members:
	:undoc-members:
	:noindex:


.. rubric:: Cloning the Group

.. code-block:: python

	# Create a clone of the original group, that exists independently
	group2 = driver.multiEval.listPy.segment.clone()



.. rubric:: Subgroups
.. toctree::
	:maxdepth: 6
	:glob:

	MultiEval_ListPy_Segment_Aclr.rst
	MultiEval_ListPy_Segment_EsFlatness.rst
	MultiEval_ListPy_Segment_InbandEmission.rst
	MultiEval_ListPy_Segment_Modulation.rst
	MultiEval_ListPy_Segment_Pmonitor.rst
	MultiEval_ListPy_Segment_Power.rst
	MultiEval_ListPy_Segment_SeMask.rst