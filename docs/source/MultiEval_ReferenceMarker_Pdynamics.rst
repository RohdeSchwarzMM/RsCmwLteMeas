Pdynamics
----------------------------------------



.. rubric:: SCPI Command :

.. index::
	single: FETCh:LTE:MEASurement<Instance>:MEValuation:REFMarker:PDYNamics

.. code-block:: python

	FETCh:LTE:MEASurement<Instance>:MEValuation:REFMarker:PDYNamics



.. autoclass:: RsCmwLteMeas.Implementations.MultiEval.ReferenceMarker.Pdynamics.PdynamicsCls
	:members:
	:undoc-members:
	:noindex: