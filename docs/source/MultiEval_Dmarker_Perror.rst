Perror
----------------------------------------



.. rubric:: SCPI Command :

.. index::
	single: FETCh:LTE:MEASurement<Instance>:MEValuation:DMARker<No>:PERRor

.. code-block:: python

	FETCh:LTE:MEASurement<Instance>:MEValuation:DMARker<No>:PERRor



.. autoclass:: RsCmwLteMeas.Implementations.MultiEval.Dmarker.Perror.PerrorCls
	:members:
	:undoc-members:
	:noindex: