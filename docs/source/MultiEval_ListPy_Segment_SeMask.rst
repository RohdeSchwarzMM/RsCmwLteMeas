SeMask
----------------------------------------





.. autoclass:: RsCmwLteMeas.Implementations.MultiEval.ListPy.Segment.SeMask.SeMaskCls
	:members:
	:undoc-members:
	:noindex:


.. rubric:: Cloning the Group

.. code-block:: python

	# Create a clone of the original group, that exists independently
	group2 = driver.multiEval.listPy.segment.seMask.clone()



.. rubric:: Subgroups
.. toctree::
	:maxdepth: 6
	:glob:

	MultiEval_ListPy_Segment_SeMask_Average.rst
	MultiEval_ListPy_Segment_SeMask_Current.rst
	MultiEval_ListPy_Segment_SeMask_Dallocation.rst
	MultiEval_ListPy_Segment_SeMask_DchType.rst
	MultiEval_ListPy_Segment_SeMask_Extreme.rst
	MultiEval_ListPy_Segment_SeMask_Margin.rst
	MultiEval_ListPy_Segment_SeMask_StandardDev.rst