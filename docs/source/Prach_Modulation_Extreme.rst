Extreme
----------------------------------------



.. rubric:: SCPI Commands :

.. index::
	single: READ:LTE:MEASurement<Instance>:PRACh:MODulation:EXTReme
	single: FETCh:LTE:MEASurement<Instance>:PRACh:MODulation:EXTReme
	single: CALCulate:LTE:MEASurement<Instance>:PRACh:MODulation:EXTReme

.. code-block:: python

	READ:LTE:MEASurement<Instance>:PRACh:MODulation:EXTReme
	FETCh:LTE:MEASurement<Instance>:PRACh:MODulation:EXTReme
	CALCulate:LTE:MEASurement<Instance>:PRACh:MODulation:EXTReme



.. autoclass:: RsCmwLteMeas.Implementations.Prach.Modulation.Extreme.ExtremeCls
	:members:
	:undoc-members:
	:noindex: