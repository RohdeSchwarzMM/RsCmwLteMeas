Evm
----------------------------------------





.. autoclass:: RsCmwLteMeas.Implementations.Prach.Trace.Evm.EvmCls
	:members:
	:undoc-members:
	:noindex:


.. rubric:: Cloning the Group

.. code-block:: python

	# Create a clone of the original group, that exists independently
	group2 = driver.prach.trace.evm.clone()



.. rubric:: Subgroups
.. toctree::
	:maxdepth: 6
	:glob:

	Prach_Trace_Evm_Average.rst
	Prach_Trace_Evm_Current.rst
	Prach_Trace_Evm_Maximum.rst