ChannelBw
----------------------------------------



.. rubric:: SCPI Command :

.. index::
	single: CONFigure:LTE:MEASurement<Instance>:CAGGregation:CBANdwidth:AGGRegated

.. code-block:: python

	CONFigure:LTE:MEASurement<Instance>:CAGGregation:CBANdwidth:AGGRegated



.. autoclass:: RsCmwLteMeas.Implementations.Configure.CarrierAggregation.ChannelBw.ChannelBwCls
	:members:
	:undoc-members:
	:noindex: