Pcc
----------------------------------------



.. rubric:: SCPI Command :

.. index::
	single: CONFigure:LTE:MEASurement<Instance>:MEValuation[:PCC]:PLCid

.. code-block:: python

	CONFigure:LTE:MEASurement<Instance>:MEValuation[:PCC]:PLCid



.. autoclass:: RsCmwLteMeas.Implementations.Configure.MultiEval.Pcc.PccCls
	:members:
	:undoc-members:
	:noindex: