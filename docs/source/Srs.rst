Srs
----------------------------------------



.. rubric:: SCPI Commands :

.. index::
	single: INITiate:LTE:MEASurement<Instance>:SRS
	single: STOP:LTE:MEASurement<Instance>:SRS
	single: ABORt:LTE:MEASurement<Instance>:SRS

.. code-block:: python

	INITiate:LTE:MEASurement<Instance>:SRS
	STOP:LTE:MEASurement<Instance>:SRS
	ABORt:LTE:MEASurement<Instance>:SRS



.. autoclass:: RsCmwLteMeas.Implementations.Srs.SrsCls
	:members:
	:undoc-members:
	:noindex:


.. rubric:: Cloning the Group

.. code-block:: python

	# Create a clone of the original group, that exists independently
	group2 = driver.srs.clone()



.. rubric:: Subgroups
.. toctree::
	:maxdepth: 6
	:glob:

	Srs_Pdynamics.rst
	Srs_State.rst
	Srs_Trace.rst