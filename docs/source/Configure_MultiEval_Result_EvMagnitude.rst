EvMagnitude
----------------------------------------



.. rubric:: SCPI Command :

.. index::
	single: CONFigure:LTE:MEASurement<Instance>:MEValuation:RESult:EVMagnitude

.. code-block:: python

	CONFigure:LTE:MEASurement<Instance>:MEValuation:RESult:EVMagnitude



.. autoclass:: RsCmwLteMeas.Implementations.Configure.MultiEval.Result.EvMagnitude.EvMagnitudeCls
	:members:
	:undoc-members:
	:noindex:


.. rubric:: Cloning the Group

.. code-block:: python

	# Create a clone of the original group, that exists independently
	group2 = driver.configure.multiEval.result.evMagnitude.clone()



.. rubric:: Subgroups
.. toctree::
	:maxdepth: 6
	:glob:

	Configure_MultiEval_Result_EvMagnitude_EvmSymbol.rst