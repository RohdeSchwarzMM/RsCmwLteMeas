Limit<Limit>
----------------------------------------

.. rubric:: RepCap Settings

.. code-block:: python

	# Range: Nr1 .. Nr12
	rc = driver.configure.multiEval.limit.seMask.limit.repcap_limit_get()
	driver.configure.multiEval.limit.seMask.limit.repcap_limit_set(repcap.Limit.Nr1)





.. autoclass:: RsCmwLteMeas.Implementations.Configure.MultiEval.Limit.SeMask.Limit.LimitCls
	:members:
	:undoc-members:
	:noindex:


.. rubric:: Cloning the Group

.. code-block:: python

	# Create a clone of the original group, that exists independently
	group2 = driver.configure.multiEval.limit.seMask.limit.clone()



.. rubric:: Subgroups
.. toctree::
	:maxdepth: 6
	:glob:

	Configure_MultiEval_Limit_SeMask_Limit_Additional.rst
	Configure_MultiEval_Limit_SeMask_Limit_CarrierAggregation.rst
	Configure_MultiEval_Limit_SeMask_Limit_ChannelBw.rst