Maximum
----------------------------------------



.. rubric:: SCPI Commands :

.. index::
	single: READ:LTE:MEASurement<Instance>:MEValuation:TRACe:EVMSymbol:MAXimum
	single: FETCh:LTE:MEASurement<Instance>:MEValuation:TRACe:EVMSymbol:MAXimum

.. code-block:: python

	READ:LTE:MEASurement<Instance>:MEValuation:TRACe:EVMSymbol:MAXimum
	FETCh:LTE:MEASurement<Instance>:MEValuation:TRACe:EVMSymbol:MAXimum



.. autoclass:: RsCmwLteMeas.Implementations.MultiEval.Trace.EvmSymbol.Maximum.MaximumCls
	:members:
	:undoc-members:
	:noindex: