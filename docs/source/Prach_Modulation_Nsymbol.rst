Nsymbol
----------------------------------------



.. rubric:: SCPI Command :

.. index::
	single: FETCh:LTE:MEASurement<Instance>:PRACh:MODulation:NSYMbol

.. code-block:: python

	FETCh:LTE:MEASurement<Instance>:PRACh:MODulation:NSYMbol



.. autoclass:: RsCmwLteMeas.Implementations.Prach.Modulation.Nsymbol.NsymbolCls
	:members:
	:undoc-members:
	:noindex: