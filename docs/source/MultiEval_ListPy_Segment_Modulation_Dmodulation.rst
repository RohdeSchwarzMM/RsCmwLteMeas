Dmodulation
----------------------------------------



.. rubric:: SCPI Command :

.. index::
	single: FETCh:LTE:MEASurement<Instance>:MEValuation:LIST:SEGMent<nr>:MODulation:DMODulation

.. code-block:: python

	FETCh:LTE:MEASurement<Instance>:MEValuation:LIST:SEGMent<nr>:MODulation:DMODulation



.. autoclass:: RsCmwLteMeas.Implementations.MultiEval.ListPy.Segment.Modulation.Dmodulation.DmodulationCls
	:members:
	:undoc-members:
	:noindex: