Dallocation
----------------------------------------



.. rubric:: SCPI Command :

.. index::
	single: FETCh:LTE:MEASurement<Instance>:MEValuation:LIST:SEGMent<nr>:ACLR:DALLocation

.. code-block:: python

	FETCh:LTE:MEASurement<Instance>:MEValuation:LIST:SEGMent<nr>:ACLR:DALLocation



.. autoclass:: RsCmwLteMeas.Implementations.MultiEval.ListPy.Segment.Aclr.Dallocation.DallocationCls
	:members:
	:undoc-members:
	:noindex: