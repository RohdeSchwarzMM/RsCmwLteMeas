DchType
----------------------------------------



.. rubric:: SCPI Command :

.. index::
	single: FETCh:LTE:MEASurement<Instance>:MEValuation:LIST:SEGMent<nr>:ACLR:DCHType

.. code-block:: python

	FETCh:LTE:MEASurement<Instance>:MEValuation:LIST:SEGMent<nr>:ACLR:DCHType



.. autoclass:: RsCmwLteMeas.Implementations.MultiEval.ListPy.Segment.Aclr.DchType.DchTypeCls
	:members:
	:undoc-members:
	:noindex: