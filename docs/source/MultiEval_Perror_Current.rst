Current
----------------------------------------



.. rubric:: SCPI Commands :

.. index::
	single: READ:LTE:MEASurement<Instance>:MEValuation:PERRor:CURRent
	single: FETCh:LTE:MEASurement<Instance>:MEValuation:PERRor:CURRent

.. code-block:: python

	READ:LTE:MEASurement<Instance>:MEValuation:PERRor:CURRent
	FETCh:LTE:MEASurement<Instance>:MEValuation:PERRor:CURRent



.. autoclass:: RsCmwLteMeas.Implementations.MultiEval.Perror.Current.CurrentCls
	:members:
	:undoc-members:
	:noindex: