Prach
----------------------------------------



.. rubric:: SCPI Commands :

.. index::
	single: CONFigure:LTE:MEASurement<Instance>:PRACh:TOUT
	single: CONFigure:LTE:MEASurement<Instance>:PRACh:REPetition
	single: CONFigure:LTE:MEASurement<Instance>:PRACh:SCONdition
	single: CONFigure:LTE:MEASurement<Instance>:PRACh:MOEXception
	single: CONFigure:LTE:MEASurement<Instance>:PRACh:PCINdex
	single: CONFigure:LTE:MEASurement<Instance>:PRACh:SSYMbol
	single: CONFigure:LTE:MEASurement<Instance>:PRACh:NOPReambles
	single: CONFigure:LTE:MEASurement<Instance>:PRACh:POPReambles

.. code-block:: python

	CONFigure:LTE:MEASurement<Instance>:PRACh:TOUT
	CONFigure:LTE:MEASurement<Instance>:PRACh:REPetition
	CONFigure:LTE:MEASurement<Instance>:PRACh:SCONdition
	CONFigure:LTE:MEASurement<Instance>:PRACh:MOEXception
	CONFigure:LTE:MEASurement<Instance>:PRACh:PCINdex
	CONFigure:LTE:MEASurement<Instance>:PRACh:SSYMbol
	CONFigure:LTE:MEASurement<Instance>:PRACh:NOPReambles
	CONFigure:LTE:MEASurement<Instance>:PRACh:POPReambles



.. autoclass:: RsCmwLteMeas.Implementations.Configure.Prach.PrachCls
	:members:
	:undoc-members:
	:noindex:


.. rubric:: Cloning the Group

.. code-block:: python

	# Create a clone of the original group, that exists independently
	group2 = driver.configure.prach.clone()



.. rubric:: Subgroups
.. toctree::
	:maxdepth: 6
	:glob:

	Configure_Prach_Limit.rst
	Configure_Prach_Modulation.rst
	Configure_Prach_PfOffset.rst
	Configure_Prach_Power.rst
	Configure_Prach_Result.rst
	Configure_Prach_Scount.rst