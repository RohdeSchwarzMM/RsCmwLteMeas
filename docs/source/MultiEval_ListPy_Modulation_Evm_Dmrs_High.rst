High
----------------------------------------





.. autoclass:: RsCmwLteMeas.Implementations.MultiEval.ListPy.Modulation.Evm.Dmrs.High.HighCls
	:members:
	:undoc-members:
	:noindex:


.. rubric:: Cloning the Group

.. code-block:: python

	# Create a clone of the original group, that exists independently
	group2 = driver.multiEval.listPy.modulation.evm.dmrs.high.clone()



.. rubric:: Subgroups
.. toctree::
	:maxdepth: 6
	:glob:

	MultiEval_ListPy_Modulation_Evm_Dmrs_High_Average.rst
	MultiEval_ListPy_Modulation_Evm_Dmrs_High_Current.rst
	MultiEval_ListPy_Modulation_Evm_Dmrs_High_Extreme.rst
	MultiEval_ListPy_Modulation_Evm_Dmrs_High_StandardDev.rst