Ripple<Ripple>
----------------------------------------

.. rubric:: RepCap Settings

.. code-block:: python

	# Range: Nr1 .. Nr2
	rc = driver.multiEval.listPy.esFlatness.ripple.repcap_ripple_get()
	driver.multiEval.listPy.esFlatness.ripple.repcap_ripple_set(repcap.Ripple.Nr1)





.. autoclass:: RsCmwLteMeas.Implementations.MultiEval.ListPy.EsFlatness.Ripple.RippleCls
	:members:
	:undoc-members:
	:noindex:


.. rubric:: Cloning the Group

.. code-block:: python

	# Create a clone of the original group, that exists independently
	group2 = driver.multiEval.listPy.esFlatness.ripple.clone()



.. rubric:: Subgroups
.. toctree::
	:maxdepth: 6
	:glob:

	MultiEval_ListPy_EsFlatness_Ripple_Average.rst
	MultiEval_ListPy_EsFlatness_Ripple_Current.rst
	MultiEval_ListPy_EsFlatness_Ripple_Extreme.rst
	MultiEval_ListPy_EsFlatness_Ripple_StandardDev.rst