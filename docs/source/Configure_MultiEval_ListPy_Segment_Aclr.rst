Aclr
----------------------------------------



.. rubric:: SCPI Command :

.. index::
	single: CONFigure:LTE:MEASurement<Instance>:MEValuation:LIST:SEGMent<nr>:ACLR

.. code-block:: python

	CONFigure:LTE:MEASurement<Instance>:MEValuation:LIST:SEGMent<nr>:ACLR



.. autoclass:: RsCmwLteMeas.Implementations.Configure.MultiEval.ListPy.Segment.Aclr.AclrCls
	:members:
	:undoc-members:
	:noindex: