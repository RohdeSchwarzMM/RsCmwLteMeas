Current
----------------------------------------



.. rubric:: SCPI Commands :

.. index::
	single: READ:LTE:MEASurement<Instance>:PRACh:EVMSymbol:PEAK:CURRent
	single: FETCh:LTE:MEASurement<Instance>:PRACh:EVMSymbol:PEAK:CURRent

.. code-block:: python

	READ:LTE:MEASurement<Instance>:PRACh:EVMSymbol:PEAK:CURRent
	FETCh:LTE:MEASurement<Instance>:PRACh:EVMSymbol:PEAK:CURRent



.. autoclass:: RsCmwLteMeas.Implementations.Prach.EvmSymbol.Peak.Current.CurrentCls
	:members:
	:undoc-members:
	:noindex: