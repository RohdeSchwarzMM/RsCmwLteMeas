Current
----------------------------------------



.. rubric:: SCPI Command :

.. index::
	single: FETCh:LTE:MEASurement<Instance>:MEValuation:LIST:ESFLatness:SCINdex:MAXimum<nr>:CURRent

.. code-block:: python

	FETCh:LTE:MEASurement<Instance>:MEValuation:LIST:ESFLatness:SCINdex:MAXimum<nr>:CURRent



.. autoclass:: RsCmwLteMeas.Implementations.MultiEval.ListPy.EsFlatness.ScIndex.Maximum.Current.CurrentCls
	:members:
	:undoc-members:
	:noindex: