Extreme
----------------------------------------



.. rubric:: SCPI Commands :

.. index::
	single: FETCh:LTE:MEASurement<Instance>:MEValuation:LIST:SEGMent<nr>:SEMask:EXTReme
	single: CALCulate:LTE:MEASurement<Instance>:MEValuation:LIST:SEGMent<nr>:SEMask:EXTReme

.. code-block:: python

	FETCh:LTE:MEASurement<Instance>:MEValuation:LIST:SEGMent<nr>:SEMask:EXTReme
	CALCulate:LTE:MEASurement<Instance>:MEValuation:LIST:SEGMent<nr>:SEMask:EXTReme



.. autoclass:: RsCmwLteMeas.Implementations.MultiEval.ListPy.Segment.SeMask.Extreme.ExtremeCls
	:members:
	:undoc-members:
	:noindex: