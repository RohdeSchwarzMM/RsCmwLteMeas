StandardDev
----------------------------------------



.. rubric:: SCPI Command :

.. index::
	single: FETCh:LTE:MEASurement<Instance>:MEValuation:LIST:ESFLatness:MINR<nr>:SDEViation

.. code-block:: python

	FETCh:LTE:MEASurement<Instance>:MEValuation:LIST:ESFLatness:MINR<nr>:SDEViation



.. autoclass:: RsCmwLteMeas.Implementations.MultiEval.ListPy.EsFlatness.Minr.StandardDev.StandardDevCls
	:members:
	:undoc-members:
	:noindex: