EsFlatness
----------------------------------------





.. autoclass:: RsCmwLteMeas.Implementations.MultiEval.ListPy.Segment.EsFlatness.EsFlatnessCls
	:members:
	:undoc-members:
	:noindex:


.. rubric:: Cloning the Group

.. code-block:: python

	# Create a clone of the original group, that exists independently
	group2 = driver.multiEval.listPy.segment.esFlatness.clone()



.. rubric:: Subgroups
.. toctree::
	:maxdepth: 6
	:glob:

	MultiEval_ListPy_Segment_EsFlatness_Average.rst
	MultiEval_ListPy_Segment_EsFlatness_Current.rst
	MultiEval_ListPy_Segment_EsFlatness_Extreme.rst
	MultiEval_ListPy_Segment_EsFlatness_StandardDev.rst