StandardDev
----------------------------------------



.. rubric:: SCPI Command :

.. index::
	single: FETCh:LTE:MEASurement<Instance>:MEValuation:LIST:MODulation:PERRor:RMS:LOW:SDEViation

.. code-block:: python

	FETCh:LTE:MEASurement<Instance>:MEValuation:LIST:MODulation:PERRor:RMS:LOW:SDEViation



.. autoclass:: RsCmwLteMeas.Implementations.MultiEval.ListPy.Modulation.Perror.Rms.Low.StandardDev.StandardDevCls
	:members:
	:undoc-members:
	:noindex: