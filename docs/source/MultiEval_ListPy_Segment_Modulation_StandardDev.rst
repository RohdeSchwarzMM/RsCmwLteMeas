StandardDev
----------------------------------------



.. rubric:: SCPI Command :

.. index::
	single: FETCh:LTE:MEASurement<Instance>:MEValuation:LIST:SEGMent<nr>:MODulation:SDEViation

.. code-block:: python

	FETCh:LTE:MEASurement<Instance>:MEValuation:LIST:SEGMent<nr>:MODulation:SDEViation



.. autoclass:: RsCmwLteMeas.Implementations.MultiEval.ListPy.Segment.Modulation.StandardDev.StandardDevCls
	:members:
	:undoc-members:
	:noindex: