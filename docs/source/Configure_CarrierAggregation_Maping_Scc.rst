Scc<SecondaryCC>
----------------------------------------

.. rubric:: RepCap Settings

.. code-block:: python

	# Range: CC1 .. CC7
	rc = driver.configure.carrierAggregation.maping.scc.repcap_secondaryCC_get()
	driver.configure.carrierAggregation.maping.scc.repcap_secondaryCC_set(repcap.SecondaryCC.CC1)



.. rubric:: SCPI Command :

.. index::
	single: CONFigure:LTE:MEASurement<Instance>:CAGGregation:MAPing:SCC<Carrier>

.. code-block:: python

	CONFigure:LTE:MEASurement<Instance>:CAGGregation:MAPing:SCC<Carrier>



.. autoclass:: RsCmwLteMeas.Implementations.Configure.CarrierAggregation.Maping.Scc.SccCls
	:members:
	:undoc-members:
	:noindex:


.. rubric:: Cloning the Group

.. code-block:: python

	# Create a clone of the original group, that exists independently
	group2 = driver.configure.carrierAggregation.maping.scc.clone()