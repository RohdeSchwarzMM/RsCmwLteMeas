Scenario
----------------------------------------



.. rubric:: SCPI Command :

.. index::
	single: ROUTe:LTE:MEASurement<Instance>:SCENario

.. code-block:: python

	ROUTe:LTE:MEASurement<Instance>:SCENario



.. autoclass:: RsCmwLteMeas.Implementations.Route.Scenario.ScenarioCls
	:members:
	:undoc-members:
	:noindex:


.. rubric:: Cloning the Group

.. code-block:: python

	# Create a clone of the original group, that exists independently
	group2 = driver.route.scenario.clone()



.. rubric:: Subgroups
.. toctree::
	:maxdepth: 6
	:glob:

	Route_Scenario_CombinedSignalPath.rst
	Route_Scenario_MaProtocol.rst
	Route_Scenario_Salone.rst