Mcarrier
----------------------------------------



.. rubric:: SCPI Command :

.. index::
	single: CONFigure:LTE:MEASurement<Instance>:CAGGregation:MCARrier:ENHanced

.. code-block:: python

	CONFigure:LTE:MEASurement<Instance>:CAGGregation:MCARrier:ENHanced



.. autoclass:: RsCmwLteMeas.Implementations.Configure.CarrierAggregation.Mcarrier.McarrierCls
	:members:
	:undoc-members:
	:noindex: