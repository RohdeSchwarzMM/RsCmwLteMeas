Aclr
----------------------------------------





.. autoclass:: RsCmwLteMeas.Implementations.MultiEval.ListPy.Aclr.AclrCls
	:members:
	:undoc-members:
	:noindex:


.. rubric:: Cloning the Group

.. code-block:: python

	# Create a clone of the original group, that exists independently
	group2 = driver.multiEval.listPy.aclr.clone()



.. rubric:: Subgroups
.. toctree::
	:maxdepth: 6
	:glob:

	MultiEval_ListPy_Aclr_Dallocation.rst
	MultiEval_ListPy_Aclr_DchType.rst
	MultiEval_ListPy_Aclr_Eutra.rst
	MultiEval_ListPy_Aclr_Utra.rst