Perror
----------------------------------------





.. autoclass:: RsCmwLteMeas.Implementations.MultiEval.Perror.PerrorCls
	:members:
	:undoc-members:
	:noindex:


.. rubric:: Cloning the Group

.. code-block:: python

	# Create a clone of the original group, that exists independently
	group2 = driver.multiEval.perror.clone()



.. rubric:: Subgroups
.. toctree::
	:maxdepth: 6
	:glob:

	MultiEval_Perror_Average.rst
	MultiEval_Perror_Current.rst
	MultiEval_Perror_Maximum.rst