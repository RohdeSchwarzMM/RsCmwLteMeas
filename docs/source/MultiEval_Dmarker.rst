Dmarker<DeltaMarker>
----------------------------------------

.. rubric:: RepCap Settings

.. code-block:: python

	# Range: Nr1 .. Nr2
	rc = driver.multiEval.dmarker.repcap_deltaMarker_get()
	driver.multiEval.dmarker.repcap_deltaMarker_set(repcap.DeltaMarker.Nr1)





.. autoclass:: RsCmwLteMeas.Implementations.MultiEval.Dmarker.DmarkerCls
	:members:
	:undoc-members:
	:noindex:


.. rubric:: Cloning the Group

.. code-block:: python

	# Create a clone of the original group, that exists independently
	group2 = driver.multiEval.dmarker.clone()



.. rubric:: Subgroups
.. toctree::
	:maxdepth: 6
	:glob:

	MultiEval_Dmarker_EvMagnitude.rst
	MultiEval_Dmarker_Merror.rst
	MultiEval_Dmarker_Pdynamics.rst
	MultiEval_Dmarker_Perror.rst
	MultiEval_Dmarker_Pmonitor.rst