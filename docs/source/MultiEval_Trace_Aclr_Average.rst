Average
----------------------------------------



.. rubric:: SCPI Commands :

.. index::
	single: READ:LTE:MEASurement<Instance>:MEValuation:TRACe:ACLR:AVERage
	single: FETCh:LTE:MEASurement<Instance>:MEValuation:TRACe:ACLR:AVERage

.. code-block:: python

	READ:LTE:MEASurement<Instance>:MEValuation:TRACe:ACLR:AVERage
	FETCh:LTE:MEASurement<Instance>:MEValuation:TRACe:ACLR:AVERage



.. autoclass:: RsCmwLteMeas.Implementations.MultiEval.Trace.Aclr.Average.AverageCls
	:members:
	:undoc-members:
	:noindex: