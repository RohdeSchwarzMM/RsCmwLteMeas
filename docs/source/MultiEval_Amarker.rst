Amarker<AbsMarker>
----------------------------------------

.. rubric:: RepCap Settings

.. code-block:: python

	# Range: Nr1 .. Nr2
	rc = driver.multiEval.amarker.repcap_absMarker_get()
	driver.multiEval.amarker.repcap_absMarker_set(repcap.AbsMarker.Nr1)





.. autoclass:: RsCmwLteMeas.Implementations.MultiEval.Amarker.AmarkerCls
	:members:
	:undoc-members:
	:noindex:


.. rubric:: Cloning the Group

.. code-block:: python

	# Create a clone of the original group, that exists independently
	group2 = driver.multiEval.amarker.clone()



.. rubric:: Subgroups
.. toctree::
	:maxdepth: 6
	:glob:

	MultiEval_Amarker_EvMagnitude.rst
	MultiEval_Amarker_Merror.rst
	MultiEval_Amarker_Pdynamics.rst
	MultiEval_Amarker_Perror.rst
	MultiEval_Amarker_Pmonitor.rst