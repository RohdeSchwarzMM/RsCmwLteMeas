Srs
----------------------------------------



.. rubric:: SCPI Command :

.. index::
	single: CONFigure:LTE:MEASurement<Instance>:MEValuation:SRS:ENABle

.. code-block:: python

	CONFigure:LTE:MEASurement<Instance>:MEValuation:SRS:ENABle



.. autoclass:: RsCmwLteMeas.Implementations.Configure.MultiEval.Srs.SrsCls
	:members:
	:undoc-members:
	:noindex: