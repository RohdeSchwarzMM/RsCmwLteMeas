Pdynamics
----------------------------------------





.. autoclass:: RsCmwLteMeas.Implementations.MultiEval.Trace.Pdynamics.PdynamicsCls
	:members:
	:undoc-members:
	:noindex:


.. rubric:: Cloning the Group

.. code-block:: python

	# Create a clone of the original group, that exists independently
	group2 = driver.multiEval.trace.pdynamics.clone()



.. rubric:: Subgroups
.. toctree::
	:maxdepth: 6
	:glob:

	MultiEval_Trace_Pdynamics_Average.rst
	MultiEval_Trace_Pdynamics_Current.rst
	MultiEval_Trace_Pdynamics_Maximum.rst