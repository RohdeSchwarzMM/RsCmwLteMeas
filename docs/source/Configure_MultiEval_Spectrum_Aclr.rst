Aclr
----------------------------------------





.. autoclass:: RsCmwLteMeas.Implementations.Configure.MultiEval.Spectrum.Aclr.AclrCls
	:members:
	:undoc-members:
	:noindex:


.. rubric:: Cloning the Group

.. code-block:: python

	# Create a clone of the original group, that exists independently
	group2 = driver.configure.multiEval.spectrum.aclr.clone()



.. rubric:: Subgroups
.. toctree::
	:maxdepth: 6
	:glob:

	Configure_MultiEval_Spectrum_Aclr_Enable.rst