DchType
----------------------------------------



.. rubric:: SCPI Command :

.. index::
	single: FETCh:LTE:MEASurement<Instance>:MEValuation:ACLR:DCHType

.. code-block:: python

	FETCh:LTE:MEASurement<Instance>:MEValuation:ACLR:DCHType



.. autoclass:: RsCmwLteMeas.Implementations.MultiEval.Aclr.DchType.DchTypeCls
	:members:
	:undoc-members:
	:noindex: