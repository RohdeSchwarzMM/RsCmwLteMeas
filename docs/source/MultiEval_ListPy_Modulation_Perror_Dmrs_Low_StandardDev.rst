StandardDev
----------------------------------------



.. rubric:: SCPI Command :

.. index::
	single: FETCh:LTE:MEASurement<Instance>:MEValuation:LIST:MODulation:PERRor:DMRS:LOW:SDEViation

.. code-block:: python

	FETCh:LTE:MEASurement<Instance>:MEValuation:LIST:MODulation:PERRor:DMRS:LOW:SDEViation



.. autoclass:: RsCmwLteMeas.Implementations.MultiEval.ListPy.Modulation.Perror.Dmrs.Low.StandardDev.StandardDevCls
	:members:
	:undoc-members:
	:noindex: