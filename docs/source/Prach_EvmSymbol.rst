EvmSymbol
----------------------------------------





.. autoclass:: RsCmwLteMeas.Implementations.Prach.EvmSymbol.EvmSymbolCls
	:members:
	:undoc-members:
	:noindex:


.. rubric:: Cloning the Group

.. code-block:: python

	# Create a clone of the original group, that exists independently
	group2 = driver.prach.evmSymbol.clone()



.. rubric:: Subgroups
.. toctree::
	:maxdepth: 6
	:glob:

	Prach_EvmSymbol_Average.rst
	Prach_EvmSymbol_Current.rst
	Prach_EvmSymbol_Maximum.rst
	Prach_EvmSymbol_Peak.rst