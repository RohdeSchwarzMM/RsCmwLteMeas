Maximum
----------------------------------------



.. rubric:: SCPI Commands :

.. index::
	single: READ:LTE:MEASurement<Instance>:MEValuation:EVMC:PEAK:MAXimum
	single: FETCh:LTE:MEASurement<Instance>:MEValuation:EVMC:PEAK:MAXimum

.. code-block:: python

	READ:LTE:MEASurement<Instance>:MEValuation:EVMC:PEAK:MAXimum
	FETCh:LTE:MEASurement<Instance>:MEValuation:EVMC:PEAK:MAXimum



.. autoclass:: RsCmwLteMeas.Implementations.MultiEval.Evmc.Peak.Maximum.MaximumCls
	:members:
	:undoc-members:
	:noindex: