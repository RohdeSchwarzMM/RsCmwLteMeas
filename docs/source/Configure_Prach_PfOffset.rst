PfOffset
----------------------------------------



.. rubric:: SCPI Commands :

.. index::
	single: CONFigure:LTE:MEASurement<Instance>:PRACh:PFOFfset:AUTO
	single: CONFigure:LTE:MEASurement<Instance>:PRACh:PFOFfset

.. code-block:: python

	CONFigure:LTE:MEASurement<Instance>:PRACh:PFOFfset:AUTO
	CONFigure:LTE:MEASurement<Instance>:PRACh:PFOFfset



.. autoclass:: RsCmwLteMeas.Implementations.Configure.Prach.PfOffset.PfOffsetCls
	:members:
	:undoc-members:
	:noindex: