Maximum
----------------------------------------



.. rubric:: SCPI Commands :

.. index::
	single: READ:LTE:MEASurement<Instance>:MEValuation:PERRor:MAXimum
	single: FETCh:LTE:MEASurement<Instance>:MEValuation:PERRor:MAXimum

.. code-block:: python

	READ:LTE:MEASurement<Instance>:MEValuation:PERRor:MAXimum
	FETCh:LTE:MEASurement<Instance>:MEValuation:PERRor:MAXimum



.. autoclass:: RsCmwLteMeas.Implementations.MultiEval.Perror.Maximum.MaximumCls
	:members:
	:undoc-members:
	:noindex: