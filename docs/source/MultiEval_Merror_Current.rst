Current
----------------------------------------



.. rubric:: SCPI Commands :

.. index::
	single: READ:LTE:MEASurement<Instance>:MEValuation:MERRor:CURRent
	single: FETCh:LTE:MEASurement<Instance>:MEValuation:MERRor:CURRent

.. code-block:: python

	READ:LTE:MEASurement<Instance>:MEValuation:MERRor:CURRent
	FETCh:LTE:MEASurement<Instance>:MEValuation:MERRor:CURRent



.. autoclass:: RsCmwLteMeas.Implementations.MultiEval.Merror.Current.CurrentCls
	:members:
	:undoc-members:
	:noindex: