RepCaps
=========



Instance (Global)
----------------------------------------------------

.. code-block:: python

	# Setting:
	driver.repcap_instance_set(repcap.Instance.Inst1)
	# Range:
	Inst1 .. Inst16
	# All values (16x):
	Inst1 | Inst2 | Inst3 | Inst4 | Inst5 | Inst6 | Inst7 | Inst8
	Inst9 | Inst10 | Inst11 | Inst12 | Inst13 | Inst14 | Inst15 | Inst16

AbsMarker
----------------------------------------------------

.. code-block:: python

	# First value:
	value = repcap.AbsMarker.Nr1
	# Values (2x):
	Nr1 | Nr2

Area
----------------------------------------------------

.. code-block:: python

	# First value:
	value = repcap.Area.Nr1
	# Range:
	Nr1 .. Nr12
	# All values (12x):
	Nr1 | Nr2 | Nr3 | Nr4 | Nr5 | Nr6 | Nr7 | Nr8
	Nr9 | Nr10 | Nr11 | Nr12

CarrierComponent
----------------------------------------------------

.. code-block:: python

	# First value:
	value = repcap.CarrierComponent.Nr1
	# Values (4x):
	Nr1 | Nr2 | Nr3 | Nr4

ChannelBw
----------------------------------------------------

.. code-block:: python

	# First value:
	value = repcap.ChannelBw.Bw14
	# Range:
	Bw14 .. Bw200
	# All values (6x):
	Bw14 | Bw30 | Bw50 | Bw100 | Bw150 | Bw200

DeltaMarker
----------------------------------------------------

.. code-block:: python

	# First value:
	value = repcap.DeltaMarker.Nr1
	# Values (2x):
	Nr1 | Nr2

Difference
----------------------------------------------------

.. code-block:: python

	# First value:
	value = repcap.Difference.Nr1
	# Values (2x):
	Nr1 | Nr2

EutraBand
----------------------------------------------------

.. code-block:: python

	# First value:
	value = repcap.EutraBand.Nr30
	# Values (2x):
	Nr30 | Nr50

FirstChannelBw
----------------------------------------------------

.. code-block:: python

	# First value:
	value = repcap.FirstChannelBw.Bw100
	# Values (3x):
	Bw100 | Bw150 | Bw200

Limit
----------------------------------------------------

.. code-block:: python

	# First value:
	value = repcap.Limit.Nr1
	# Range:
	Nr1 .. Nr12
	# All values (12x):
	Nr1 | Nr2 | Nr3 | Nr4 | Nr5 | Nr6 | Nr7 | Nr8
	Nr9 | Nr10 | Nr11 | Nr12

MaxRange
----------------------------------------------------

.. code-block:: python

	# First value:
	value = repcap.MaxRange.Nr1
	# Values (2x):
	Nr1 | Nr2

MinRange
----------------------------------------------------

.. code-block:: python

	# First value:
	value = repcap.MinRange.Nr1
	# Values (2x):
	Nr1 | Nr2

Preamble
----------------------------------------------------

.. code-block:: python

	# First value:
	value = repcap.Preamble.Nr1
	# Range:
	Nr1 .. Nr400
	# All values (400x):
	Nr1 | Nr2 | Nr3 | Nr4 | Nr5 | Nr6 | Nr7 | Nr8
	Nr9 | Nr10 | Nr11 | Nr12 | Nr13 | Nr14 | Nr15 | Nr16
	Nr17 | Nr18 | Nr19 | Nr20 | Nr21 | Nr22 | Nr23 | Nr24
	Nr25 | Nr26 | Nr27 | Nr28 | Nr29 | Nr30 | Nr31 | Nr32
	Nr33 | Nr34 | Nr35 | Nr36 | Nr37 | Nr38 | Nr39 | Nr40
	Nr41 | Nr42 | Nr43 | Nr44 | Nr45 | Nr46 | Nr47 | Nr48
	Nr49 | Nr50 | Nr51 | Nr52 | Nr53 | Nr54 | Nr55 | Nr56
	Nr57 | Nr58 | Nr59 | Nr60 | Nr61 | Nr62 | Nr63 | Nr64
	Nr65 | Nr66 | Nr67 | Nr68 | Nr69 | Nr70 | Nr71 | Nr72
	Nr73 | Nr74 | Nr75 | Nr76 | Nr77 | Nr78 | Nr79 | Nr80
	Nr81 | Nr82 | Nr83 | Nr84 | Nr85 | Nr86 | Nr87 | Nr88
	Nr89 | Nr90 | Nr91 | Nr92 | Nr93 | Nr94 | Nr95 | Nr96
	Nr97 | Nr98 | Nr99 | Nr100 | Nr101 | Nr102 | Nr103 | Nr104
	Nr105 | Nr106 | Nr107 | Nr108 | Nr109 | Nr110 | Nr111 | Nr112
	Nr113 | Nr114 | Nr115 | Nr116 | Nr117 | Nr118 | Nr119 | Nr120
	Nr121 | Nr122 | Nr123 | Nr124 | Nr125 | Nr126 | Nr127 | Nr128
	Nr129 | Nr130 | Nr131 | Nr132 | Nr133 | Nr134 | Nr135 | Nr136
	Nr137 | Nr138 | Nr139 | Nr140 | Nr141 | Nr142 | Nr143 | Nr144
	Nr145 | Nr146 | Nr147 | Nr148 | Nr149 | Nr150 | Nr151 | Nr152
	Nr153 | Nr154 | Nr155 | Nr156 | Nr157 | Nr158 | Nr159 | Nr160
	Nr161 | Nr162 | Nr163 | Nr164 | Nr165 | Nr166 | Nr167 | Nr168
	Nr169 | Nr170 | Nr171 | Nr172 | Nr173 | Nr174 | Nr175 | Nr176
	Nr177 | Nr178 | Nr179 | Nr180 | Nr181 | Nr182 | Nr183 | Nr184
	Nr185 | Nr186 | Nr187 | Nr188 | Nr189 | Nr190 | Nr191 | Nr192
	Nr193 | Nr194 | Nr195 | Nr196 | Nr197 | Nr198 | Nr199 | Nr200
	Nr201 | Nr202 | Nr203 | Nr204 | Nr205 | Nr206 | Nr207 | Nr208
	Nr209 | Nr210 | Nr211 | Nr212 | Nr213 | Nr214 | Nr215 | Nr216
	Nr217 | Nr218 | Nr219 | Nr220 | Nr221 | Nr222 | Nr223 | Nr224
	Nr225 | Nr226 | Nr227 | Nr228 | Nr229 | Nr230 | Nr231 | Nr232
	Nr233 | Nr234 | Nr235 | Nr236 | Nr237 | Nr238 | Nr239 | Nr240
	Nr241 | Nr242 | Nr243 | Nr244 | Nr245 | Nr246 | Nr247 | Nr248
	Nr249 | Nr250 | Nr251 | Nr252 | Nr253 | Nr254 | Nr255 | Nr256
	Nr257 | Nr258 | Nr259 | Nr260 | Nr261 | Nr262 | Nr263 | Nr264
	Nr265 | Nr266 | Nr267 | Nr268 | Nr269 | Nr270 | Nr271 | Nr272
	Nr273 | Nr274 | Nr275 | Nr276 | Nr277 | Nr278 | Nr279 | Nr280
	Nr281 | Nr282 | Nr283 | Nr284 | Nr285 | Nr286 | Nr287 | Nr288
	Nr289 | Nr290 | Nr291 | Nr292 | Nr293 | Nr294 | Nr295 | Nr296
	Nr297 | Nr298 | Nr299 | Nr300 | Nr301 | Nr302 | Nr303 | Nr304
	Nr305 | Nr306 | Nr307 | Nr308 | Nr309 | Nr310 | Nr311 | Nr312
	Nr313 | Nr314 | Nr315 | Nr316 | Nr317 | Nr318 | Nr319 | Nr320
	Nr321 | Nr322 | Nr323 | Nr324 | Nr325 | Nr326 | Nr327 | Nr328
	Nr329 | Nr330 | Nr331 | Nr332 | Nr333 | Nr334 | Nr335 | Nr336
	Nr337 | Nr338 | Nr339 | Nr340 | Nr341 | Nr342 | Nr343 | Nr344
	Nr345 | Nr346 | Nr347 | Nr348 | Nr349 | Nr350 | Nr351 | Nr352
	Nr353 | Nr354 | Nr355 | Nr356 | Nr357 | Nr358 | Nr359 | Nr360
	Nr361 | Nr362 | Nr363 | Nr364 | Nr365 | Nr366 | Nr367 | Nr368
	Nr369 | Nr370 | Nr371 | Nr372 | Nr373 | Nr374 | Nr375 | Nr376
	Nr377 | Nr378 | Nr379 | Nr380 | Nr381 | Nr382 | Nr383 | Nr384
	Nr385 | Nr386 | Nr387 | Nr388 | Nr389 | Nr390 | Nr391 | Nr392
	Nr393 | Nr394 | Nr395 | Nr396 | Nr397 | Nr398 | Nr399 | Nr400

PreambleFormat
----------------------------------------------------

.. code-block:: python

	# First value:
	value = repcap.PreambleFormat.Fmt1
	# Range:
	Fmt1 .. Fmt5
	# All values (5x):
	Fmt1 | Fmt2 | Fmt3 | Fmt4 | Fmt5

QAMmodOrder
----------------------------------------------------

.. code-block:: python

	# First value:
	value = repcap.QAMmodOrder.Qam16
	# Values (3x):
	Qam16 | Qam64 | Qam256

RBcount
----------------------------------------------------

.. code-block:: python

	# First value:
	value = repcap.RBcount.Nr1
	# Values (2x):
	Nr1 | Nr2

RBoffset
----------------------------------------------------

.. code-block:: python

	# First value:
	value = repcap.RBoffset.Nr1
	# Values (2x):
	Nr1 | Nr2

RBWkHz
----------------------------------------------------

.. code-block:: python

	# First value:
	value = repcap.RBWkHz.Rbw30
	# Range:
	Rbw30 .. Rbw1000
	# All values (6x):
	Rbw30 | Rbw50 | Rbw100 | Rbw150 | Rbw200 | Rbw1000

Ripple
----------------------------------------------------

.. code-block:: python

	# First value:
	value = repcap.Ripple.Nr1
	# Values (2x):
	Nr1 | Nr2

SecondaryCC
----------------------------------------------------

.. code-block:: python

	# First value:
	value = repcap.SecondaryCC.CC1
	# Range:
	CC1 .. CC7
	# All values (7x):
	CC1 | CC2 | CC3 | CC4 | CC5 | CC6 | CC7

SecondChannelBw
----------------------------------------------------

.. code-block:: python

	# First value:
	value = repcap.SecondChannelBw.Bw50
	# Values (4x):
	Bw50 | Bw100 | Bw150 | Bw200

Segment
----------------------------------------------------

.. code-block:: python

	# First value:
	value = repcap.Segment.Nr1
	# Range:
	Nr1 .. Nr128
	# All values (128x):
	Nr1 | Nr2 | Nr3 | Nr4 | Nr5 | Nr6 | Nr7 | Nr8
	Nr9 | Nr10 | Nr11 | Nr12 | Nr13 | Nr14 | Nr15 | Nr16
	Nr17 | Nr18 | Nr19 | Nr20 | Nr21 | Nr22 | Nr23 | Nr24
	Nr25 | Nr26 | Nr27 | Nr28 | Nr29 | Nr30 | Nr31 | Nr32
	Nr33 | Nr34 | Nr35 | Nr36 | Nr37 | Nr38 | Nr39 | Nr40
	Nr41 | Nr42 | Nr43 | Nr44 | Nr45 | Nr46 | Nr47 | Nr48
	Nr49 | Nr50 | Nr51 | Nr52 | Nr53 | Nr54 | Nr55 | Nr56
	Nr57 | Nr58 | Nr59 | Nr60 | Nr61 | Nr62 | Nr63 | Nr64
	Nr65 | Nr66 | Nr67 | Nr68 | Nr69 | Nr70 | Nr71 | Nr72
	Nr73 | Nr74 | Nr75 | Nr76 | Nr77 | Nr78 | Nr79 | Nr80
	Nr81 | Nr82 | Nr83 | Nr84 | Nr85 | Nr86 | Nr87 | Nr88
	Nr89 | Nr90 | Nr91 | Nr92 | Nr93 | Nr94 | Nr95 | Nr96
	Nr97 | Nr98 | Nr99 | Nr100 | Nr101 | Nr102 | Nr103 | Nr104
	Nr105 | Nr106 | Nr107 | Nr108 | Nr109 | Nr110 | Nr111 | Nr112
	Nr113 | Nr114 | Nr115 | Nr116 | Nr117 | Nr118 | Nr119 | Nr120
	Nr121 | Nr122 | Nr123 | Nr124 | Nr125 | Nr126 | Nr127 | Nr128

Table
----------------------------------------------------

.. code-block:: python

	# First value:
	value = repcap.Table.Nr1
	# Range:
	Nr1 .. Nr5
	# All values (5x):
	Nr1 | Nr2 | Nr3 | Nr4 | Nr5

ThirdChannelBw
----------------------------------------------------

.. code-block:: python

	# First value:
	value = repcap.ThirdChannelBw.Bw100
	# Values (3x):
	Bw100 | Bw150 | Bw200

UtraAdjChannel
----------------------------------------------------

.. code-block:: python

	# First value:
	value = repcap.UtraAdjChannel.Ch1
	# Values (2x):
	Ch1 | Ch2

