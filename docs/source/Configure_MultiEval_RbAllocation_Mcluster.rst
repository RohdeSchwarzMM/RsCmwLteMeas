Mcluster
----------------------------------------



.. rubric:: SCPI Command :

.. index::
	single: CONFigure:LTE:MEASurement<Instance>:MEValuation:RBALlocation:MCLuster

.. code-block:: python

	CONFigure:LTE:MEASurement<Instance>:MEValuation:RBALlocation:MCLuster



.. autoclass:: RsCmwLteMeas.Implementations.Configure.MultiEval.RbAllocation.Mcluster.MclusterCls
	:members:
	:undoc-members:
	:noindex:


.. rubric:: Cloning the Group

.. code-block:: python

	# Create a clone of the original group, that exists independently
	group2 = driver.configure.multiEval.rbAllocation.mcluster.clone()



.. rubric:: Subgroups
.. toctree::
	:maxdepth: 6
	:glob:

	Configure_MultiEval_RbAllocation_Mcluster_Nrb.rst
	Configure_MultiEval_RbAllocation_Mcluster_Orb.rst