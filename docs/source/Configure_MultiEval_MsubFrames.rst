MsubFrames
----------------------------------------



.. rubric:: SCPI Command :

.. index::
	single: CONFigure:LTE:MEASurement<Instance>:MEValuation:MSUBframes

.. code-block:: python

	CONFigure:LTE:MEASurement<Instance>:MEValuation:MSUBframes



.. autoclass:: RsCmwLteMeas.Implementations.Configure.MultiEval.MsubFrames.MsubFramesCls
	:members:
	:undoc-members:
	:noindex: