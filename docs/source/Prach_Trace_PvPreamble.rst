PvPreamble
----------------------------------------



.. rubric:: SCPI Commands :

.. index::
	single: READ:LTE:MEASurement<Instance>:PRACh:TRACe:PVPReamble
	single: FETCh:LTE:MEASurement<Instance>:PRACh:TRACe:PVPReamble

.. code-block:: python

	READ:LTE:MEASurement<Instance>:PRACh:TRACe:PVPReamble
	FETCh:LTE:MEASurement<Instance>:PRACh:TRACe:PVPReamble



.. autoclass:: RsCmwLteMeas.Implementations.Prach.Trace.PvPreamble.PvPreambleCls
	:members:
	:undoc-members:
	:noindex: