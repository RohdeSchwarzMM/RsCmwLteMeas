EvMagnitude
----------------------------------------



.. rubric:: SCPI Command :

.. index::
	single: CONFigure:LTE:MEASurement<Instance>:MEValuation:LIMit:QAM<ModOrder>:EVMagnitude

.. code-block:: python

	CONFigure:LTE:MEASurement<Instance>:MEValuation:LIMit:QAM<ModOrder>:EVMagnitude



.. autoclass:: RsCmwLteMeas.Implementations.Configure.MultiEval.Limit.Qam.EvMagnitude.EvMagnitudeCls
	:members:
	:undoc-members:
	:noindex: