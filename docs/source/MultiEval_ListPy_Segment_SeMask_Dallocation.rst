Dallocation
----------------------------------------



.. rubric:: SCPI Command :

.. index::
	single: FETCh:LTE:MEASurement<Instance>:MEValuation:LIST:SEGMent<nr>:SEMask:DALLocation

.. code-block:: python

	FETCh:LTE:MEASurement<Instance>:MEValuation:LIST:SEGMent<nr>:SEMask:DALLocation



.. autoclass:: RsCmwLteMeas.Implementations.MultiEval.ListPy.Segment.SeMask.Dallocation.DallocationCls
	:members:
	:undoc-members:
	:noindex: