Current
----------------------------------------



.. rubric:: SCPI Commands :

.. index::
	single: FETCh:LTE:MEASurement<Instance>:MEValuation:LIST:MODulation:TPOWer:CURRent
	single: CALCulate:LTE:MEASurement<Instance>:MEValuation:LIST:MODulation:TPOWer:CURRent

.. code-block:: python

	FETCh:LTE:MEASurement<Instance>:MEValuation:LIST:MODulation:TPOWer:CURRent
	CALCulate:LTE:MEASurement<Instance>:MEValuation:LIST:MODulation:TPOWer:CURRent



.. autoclass:: RsCmwLteMeas.Implementations.MultiEval.ListPy.Modulation.Tpower.Current.CurrentCls
	:members:
	:undoc-members:
	:noindex: