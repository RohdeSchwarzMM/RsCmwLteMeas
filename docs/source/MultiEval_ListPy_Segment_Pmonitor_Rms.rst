Rms
----------------------------------------



.. rubric:: SCPI Command :

.. index::
	single: FETCh:LTE:MEASurement<Instance>:MEValuation:LIST:SEGMent<nr>:PMONitor:RMS

.. code-block:: python

	FETCh:LTE:MEASurement<Instance>:MEValuation:LIST:SEGMent<nr>:PMONitor:RMS



.. autoclass:: RsCmwLteMeas.Implementations.MultiEval.ListPy.Segment.Pmonitor.Rms.RmsCls
	:members:
	:undoc-members:
	:noindex: