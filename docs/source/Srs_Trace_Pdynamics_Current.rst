Current
----------------------------------------



.. rubric:: SCPI Commands :

.. index::
	single: READ:LTE:MEASurement<Instance>:SRS:TRACe:PDYNamics:CURRent
	single: FETCh:LTE:MEASurement<Instance>:SRS:TRACe:PDYNamics:CURRent

.. code-block:: python

	READ:LTE:MEASurement<Instance>:SRS:TRACe:PDYNamics:CURRent
	FETCh:LTE:MEASurement<Instance>:SRS:TRACe:PDYNamics:CURRent



.. autoclass:: RsCmwLteMeas.Implementations.Srs.Trace.Pdynamics.Current.CurrentCls
	:members:
	:undoc-members:
	:noindex: