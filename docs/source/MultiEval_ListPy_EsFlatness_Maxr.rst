Maxr<MaxRange>
----------------------------------------

.. rubric:: RepCap Settings

.. code-block:: python

	# Range: Nr1 .. Nr2
	rc = driver.multiEval.listPy.esFlatness.maxr.repcap_maxRange_get()
	driver.multiEval.listPy.esFlatness.maxr.repcap_maxRange_set(repcap.MaxRange.Nr1)





.. autoclass:: RsCmwLteMeas.Implementations.MultiEval.ListPy.EsFlatness.Maxr.MaxrCls
	:members:
	:undoc-members:
	:noindex:


.. rubric:: Cloning the Group

.. code-block:: python

	# Create a clone of the original group, that exists independently
	group2 = driver.multiEval.listPy.esFlatness.maxr.clone()



.. rubric:: Subgroups
.. toctree::
	:maxdepth: 6
	:glob:

	MultiEval_ListPy_EsFlatness_Maxr_Average.rst
	MultiEval_ListPy_EsFlatness_Maxr_Current.rst
	MultiEval_ListPy_EsFlatness_Maxr_Extreme.rst
	MultiEval_ListPy_EsFlatness_Maxr_StandardDev.rst