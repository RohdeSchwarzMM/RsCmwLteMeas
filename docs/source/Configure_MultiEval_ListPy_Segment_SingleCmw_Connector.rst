Connector
----------------------------------------



.. rubric:: SCPI Command :

.. index::
	single: CONFigure:LTE:MEASurement<Instance>:MEValuation:LIST:SEGMent<nr>:CMWS:CONNector

.. code-block:: python

	CONFigure:LTE:MEASurement<Instance>:MEValuation:LIST:SEGMent<nr>:CMWS:CONNector



.. autoclass:: RsCmwLteMeas.Implementations.Configure.MultiEval.ListPy.Segment.SingleCmw.Connector.ConnectorCls
	:members:
	:undoc-members:
	:noindex: