MaProtocol
----------------------------------------



.. rubric:: SCPI Command :

.. index::
	single: ROUTe:LTE:MEASurement<Instance>:SCENario:MAPRotocol

.. code-block:: python

	ROUTe:LTE:MEASurement<Instance>:SCENario:MAPRotocol



.. autoclass:: RsCmwLteMeas.Implementations.Route.Scenario.MaProtocol.MaProtocolCls
	:members:
	:undoc-members:
	:noindex: