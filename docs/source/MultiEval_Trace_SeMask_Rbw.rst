Rbw<RBWkHz>
----------------------------------------

.. rubric:: RepCap Settings

.. code-block:: python

	# Range: Rbw30 .. Rbw1000
	rc = driver.multiEval.trace.seMask.rbw.repcap_rBWkHz_get()
	driver.multiEval.trace.seMask.rbw.repcap_rBWkHz_set(repcap.RBWkHz.Rbw30)





.. autoclass:: RsCmwLteMeas.Implementations.MultiEval.Trace.SeMask.Rbw.RbwCls
	:members:
	:undoc-members:
	:noindex:


.. rubric:: Cloning the Group

.. code-block:: python

	# Create a clone of the original group, that exists independently
	group2 = driver.multiEval.trace.seMask.rbw.clone()



.. rubric:: Subgroups
.. toctree::
	:maxdepth: 6
	:glob:

	MultiEval_Trace_SeMask_Rbw_Average.rst
	MultiEval_Trace_SeMask_Rbw_Current.rst
	MultiEval_Trace_SeMask_Rbw_Maximum.rst