Merror
----------------------------------------





.. autoclass:: RsCmwLteMeas.Implementations.MultiEval.Merror.MerrorCls
	:members:
	:undoc-members:
	:noindex:


.. rubric:: Cloning the Group

.. code-block:: python

	# Create a clone of the original group, that exists independently
	group2 = driver.multiEval.merror.clone()



.. rubric:: Subgroups
.. toctree::
	:maxdepth: 6
	:glob:

	MultiEval_Merror_Average.rst
	MultiEval_Merror_Current.rst
	MultiEval_Merror_Maximum.rst