Average
----------------------------------------





.. autoclass:: RsCmwLteMeas.Implementations.MultiEval.SeMask.Margin.Average.AverageCls
	:members:
	:undoc-members:
	:noindex:


.. rubric:: Cloning the Group

.. code-block:: python

	# Create a clone of the original group, that exists independently
	group2 = driver.multiEval.seMask.margin.average.clone()



.. rubric:: Subgroups
.. toctree::
	:maxdepth: 6
	:glob:

	MultiEval_SeMask_Margin_Average_Negativ.rst
	MultiEval_SeMask_Margin_Average_Positiv.rst