StandardDev
----------------------------------------



.. rubric:: SCPI Command :

.. index::
	single: FETCh:LTE:MEASurement<Instance>:MEValuation:LIST:MODulation:PPOWer:SDEViation

.. code-block:: python

	FETCh:LTE:MEASurement<Instance>:MEValuation:LIST:MODulation:PPOWer:SDEViation



.. autoclass:: RsCmwLteMeas.Implementations.MultiEval.ListPy.Modulation.Ppower.StandardDev.StandardDevCls
	:members:
	:undoc-members:
	:noindex: