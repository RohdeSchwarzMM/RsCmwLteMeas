Modulation
----------------------------------------





.. autoclass:: RsCmwLteMeas.Implementations.MultiEval.ListPy.Segment.Modulation.ModulationCls
	:members:
	:undoc-members:
	:noindex:


.. rubric:: Cloning the Group

.. code-block:: python

	# Create a clone of the original group, that exists independently
	group2 = driver.multiEval.listPy.segment.modulation.clone()



.. rubric:: Subgroups
.. toctree::
	:maxdepth: 6
	:glob:

	MultiEval_ListPy_Segment_Modulation_Average.rst
	MultiEval_ListPy_Segment_Modulation_Current.rst
	MultiEval_ListPy_Segment_Modulation_Dallocation.rst
	MultiEval_ListPy_Segment_Modulation_DchType.rst
	MultiEval_ListPy_Segment_Modulation_Dmodulation.rst
	MultiEval_ListPy_Segment_Modulation_Extreme.rst
	MultiEval_ListPy_Segment_Modulation_SchType.rst
	MultiEval_ListPy_Segment_Modulation_StandardDev.rst