Extreme
----------------------------------------



.. rubric:: SCPI Commands :

.. index::
	single: FETCh:LTE:MEASurement<Instance>:MEValuation:LIST:MODulation:MERRor:PEAK:LOW:EXTReme
	single: CALCulate:LTE:MEASurement<Instance>:MEValuation:LIST:MODulation:MERRor:PEAK:LOW:EXTReme

.. code-block:: python

	FETCh:LTE:MEASurement<Instance>:MEValuation:LIST:MODulation:MERRor:PEAK:LOW:EXTReme
	CALCulate:LTE:MEASurement<Instance>:MEValuation:LIST:MODulation:MERRor:PEAK:LOW:EXTReme



.. autoclass:: RsCmwLteMeas.Implementations.MultiEval.ListPy.Modulation.Merror.Peak.Low.Extreme.ExtremeCls
	:members:
	:undoc-members:
	:noindex: