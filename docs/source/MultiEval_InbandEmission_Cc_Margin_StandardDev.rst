StandardDev
----------------------------------------



.. rubric:: SCPI Command :

.. index::
	single: FETCh:LTE:MEASurement<Instance>:MEValuation:IEMission:CC<Nr>:MARGin:SDEViation

.. code-block:: python

	FETCh:LTE:MEASurement<Instance>:MEValuation:IEMission:CC<Nr>:MARGin:SDEViation



.. autoclass:: RsCmwLteMeas.Implementations.MultiEval.InbandEmission.Cc.Margin.StandardDev.StandardDevCls
	:members:
	:undoc-members:
	:noindex: