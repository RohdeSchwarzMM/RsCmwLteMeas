Extreme
----------------------------------------



.. rubric:: SCPI Command :

.. index::
	single: FETCh:LTE:MEASurement<Instance>:MEValuation:LIST:IEMission:MARGin:EXTReme

.. code-block:: python

	FETCh:LTE:MEASurement<Instance>:MEValuation:LIST:IEMission:MARGin:EXTReme



.. autoclass:: RsCmwLteMeas.Implementations.MultiEval.ListPy.InbandEmission.Margin.Extreme.ExtremeCls
	:members:
	:undoc-members:
	:noindex: