Lrange
----------------------------------------



.. rubric:: SCPI Command :

.. index::
	single: CONFigure:LTE:MEASurement<Instance>:MEValuation:LIST:LRANge

.. code-block:: python

	CONFigure:LTE:MEASurement<Instance>:MEValuation:LIST:LRANge



.. autoclass:: RsCmwLteMeas.Implementations.Configure.MultiEval.ListPy.Lrange.LrangeCls
	:members:
	:undoc-members:
	:noindex: