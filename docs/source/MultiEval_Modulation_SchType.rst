SchType
----------------------------------------



.. rubric:: SCPI Command :

.. index::
	single: FETCh:LTE:MEASurement<Instance>:MEValuation:MODulation:SCHType

.. code-block:: python

	FETCh:LTE:MEASurement<Instance>:MEValuation:MODulation:SCHType



.. autoclass:: RsCmwLteMeas.Implementations.MultiEval.Modulation.SchType.SchTypeCls
	:members:
	:undoc-members:
	:noindex: