Average
----------------------------------------



.. rubric:: SCPI Commands :

.. index::
	single: FETCh:LTE:MEASurement<Instance>:MEValuation:LIST:ACLR:EUTRa:AVERage
	single: CALCulate:LTE:MEASurement<Instance>:MEValuation:LIST:ACLR:EUTRa:AVERage

.. code-block:: python

	FETCh:LTE:MEASurement<Instance>:MEValuation:LIST:ACLR:EUTRa:AVERage
	CALCulate:LTE:MEASurement<Instance>:MEValuation:LIST:ACLR:EUTRa:AVERage



.. autoclass:: RsCmwLteMeas.Implementations.MultiEval.ListPy.Aclr.Eutra.Average.AverageCls
	:members:
	:undoc-members:
	:noindex: