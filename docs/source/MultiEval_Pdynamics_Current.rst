Current
----------------------------------------



.. rubric:: SCPI Commands :

.. index::
	single: READ:LTE:MEASurement<Instance>:MEValuation:PDYNamics:CURRent
	single: FETCh:LTE:MEASurement<Instance>:MEValuation:PDYNamics:CURRent
	single: CALCulate:LTE:MEASurement<Instance>:MEValuation:PDYNamics:CURRent

.. code-block:: python

	READ:LTE:MEASurement<Instance>:MEValuation:PDYNamics:CURRent
	FETCh:LTE:MEASurement<Instance>:MEValuation:PDYNamics:CURRent
	CALCulate:LTE:MEASurement<Instance>:MEValuation:PDYNamics:CURRent



.. autoclass:: RsCmwLteMeas.Implementations.MultiEval.Pdynamics.Current.CurrentCls
	:members:
	:undoc-members:
	:noindex: