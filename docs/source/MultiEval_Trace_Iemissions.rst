Iemissions
----------------------------------------





.. autoclass:: RsCmwLteMeas.Implementations.MultiEval.Trace.Iemissions.IemissionsCls
	:members:
	:undoc-members:
	:noindex:


.. rubric:: Cloning the Group

.. code-block:: python

	# Create a clone of the original group, that exists independently
	group2 = driver.multiEval.trace.iemissions.clone()



.. rubric:: Subgroups
.. toctree::
	:maxdepth: 6
	:glob:

	MultiEval_Trace_Iemissions_Cc.rst