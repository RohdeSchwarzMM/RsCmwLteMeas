Pdynamics
----------------------------------------





.. autoclass:: RsCmwLteMeas.Implementations.Prach.Trace.Pdynamics.PdynamicsCls
	:members:
	:undoc-members:
	:noindex:


.. rubric:: Cloning the Group

.. code-block:: python

	# Create a clone of the original group, that exists independently
	group2 = driver.prach.trace.pdynamics.clone()



.. rubric:: Subgroups
.. toctree::
	:maxdepth: 6
	:glob:

	Prach_Trace_Pdynamics_Average.rst
	Prach_Trace_Pdynamics_Current.rst
	Prach_Trace_Pdynamics_Maximum.rst