Peak
----------------------------------------



.. rubric:: SCPI Command :

.. index::
	single: FETCh:LTE:MEASurement<Instance>:MEValuation:LIST:SEGMent<nr>:PMONitor:PEAK

.. code-block:: python

	FETCh:LTE:MEASurement<Instance>:MEValuation:LIST:SEGMent<nr>:PMONitor:PEAK



.. autoclass:: RsCmwLteMeas.Implementations.MultiEval.ListPy.Segment.Pmonitor.Peak.PeakCls
	:members:
	:undoc-members:
	:noindex: