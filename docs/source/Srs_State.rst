State
----------------------------------------



.. rubric:: SCPI Command :

.. index::
	single: FETCh:LTE:MEASurement<Instance>:SRS:STATe

.. code-block:: python

	FETCh:LTE:MEASurement<Instance>:SRS:STATe



.. autoclass:: RsCmwLteMeas.Implementations.Srs.State.StateCls
	:members:
	:undoc-members:
	:noindex:


.. rubric:: Cloning the Group

.. code-block:: python

	# Create a clone of the original group, that exists independently
	group2 = driver.srs.state.clone()



.. rubric:: Subgroups
.. toctree::
	:maxdepth: 6
	:glob:

	Srs_State_All.rst