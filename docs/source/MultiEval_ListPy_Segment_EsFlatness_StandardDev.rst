StandardDev
----------------------------------------



.. rubric:: SCPI Command :

.. index::
	single: FETCh:LTE:MEASurement<Instance>:MEValuation:LIST:SEGMent<nr>:ESFLatness:SDEViation

.. code-block:: python

	FETCh:LTE:MEASurement<Instance>:MEValuation:LIST:SEGMent<nr>:ESFLatness:SDEViation



.. autoclass:: RsCmwLteMeas.Implementations.MultiEval.ListPy.Segment.EsFlatness.StandardDev.StandardDevCls
	:members:
	:undoc-members:
	:noindex: