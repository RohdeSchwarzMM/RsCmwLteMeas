MultiEval
----------------------------------------



.. rubric:: SCPI Commands :

.. index::
	single: CONFigure:LTE:MEASurement<Instance>:MEValuation:TOUT
	single: CONFigure:LTE:MEASurement<Instance>:MEValuation:MMODe
	single: CONFigure:LTE:MEASurement<Instance>:MEValuation:REPetition
	single: CONFigure:LTE:MEASurement<Instance>:MEValuation:SCONdition
	single: CONFigure:LTE:MEASurement<Instance>:MEValuation:ULDL
	single: CONFigure:LTE:MEASurement<Instance>:MEValuation:SSUBframe
	single: CONFigure:LTE:MEASurement<Instance>:MEValuation:MOEXception
	single: CONFigure:LTE:MEASurement<Instance>:MEValuation:CPRefix
	single: CONFigure:LTE:MEASurement<Instance>:MEValuation:CTYPe
	single: CONFigure:LTE:MEASurement<Instance>:MEValuation:SCTYpe
	single: CONFigure:LTE:MEASurement<Instance>:MEValuation:PSEarch
	single: CONFigure:LTE:MEASurement<Instance>:MEValuation:PFORmat
	single: CONFigure:LTE:MEASurement<Instance>:MEValuation:NVFilter
	single: CONFigure:LTE:MEASurement<Instance>:MEValuation:ORVFilter
	single: CONFigure:LTE:MEASurement<Instance>:MEValuation:CTVFilter
	single: CONFigure:LTE:MEASurement<Instance>:MEValuation:DSSPusch
	single: CONFigure:LTE:MEASurement<Instance>:MEValuation:GHOPping
	single: CONFigure:LTE:MEASurement<Instance>:MEValuation:MSLot

.. code-block:: python

	CONFigure:LTE:MEASurement<Instance>:MEValuation:TOUT
	CONFigure:LTE:MEASurement<Instance>:MEValuation:MMODe
	CONFigure:LTE:MEASurement<Instance>:MEValuation:REPetition
	CONFigure:LTE:MEASurement<Instance>:MEValuation:SCONdition
	CONFigure:LTE:MEASurement<Instance>:MEValuation:ULDL
	CONFigure:LTE:MEASurement<Instance>:MEValuation:SSUBframe
	CONFigure:LTE:MEASurement<Instance>:MEValuation:MOEXception
	CONFigure:LTE:MEASurement<Instance>:MEValuation:CPRefix
	CONFigure:LTE:MEASurement<Instance>:MEValuation:CTYPe
	CONFigure:LTE:MEASurement<Instance>:MEValuation:SCTYpe
	CONFigure:LTE:MEASurement<Instance>:MEValuation:PSEarch
	CONFigure:LTE:MEASurement<Instance>:MEValuation:PFORmat
	CONFigure:LTE:MEASurement<Instance>:MEValuation:NVFilter
	CONFigure:LTE:MEASurement<Instance>:MEValuation:ORVFilter
	CONFigure:LTE:MEASurement<Instance>:MEValuation:CTVFilter
	CONFigure:LTE:MEASurement<Instance>:MEValuation:DSSPusch
	CONFigure:LTE:MEASurement<Instance>:MEValuation:GHOPping
	CONFigure:LTE:MEASurement<Instance>:MEValuation:MSLot



.. autoclass:: RsCmwLteMeas.Implementations.Configure.MultiEval.MultiEvalCls
	:members:
	:undoc-members:
	:noindex:


.. rubric:: Cloning the Group

.. code-block:: python

	# Create a clone of the original group, that exists independently
	group2 = driver.configure.multiEval.clone()



.. rubric:: Subgroups
.. toctree::
	:maxdepth: 6
	:glob:

	Configure_MultiEval_Bler.rst
	Configure_MultiEval_Cc.rst
	Configure_MultiEval_Limit.rst
	Configure_MultiEval_ListPy.rst
	Configure_MultiEval_Modulation.rst
	Configure_MultiEval_MsubFrames.rst
	Configure_MultiEval_NsValue.rst
	Configure_MultiEval_Pcc.rst
	Configure_MultiEval_Pdynamics.rst
	Configure_MultiEval_Power.rst
	Configure_MultiEval_RbAllocation.rst
	Configure_MultiEval_Result.rst
	Configure_MultiEval_Scount.rst
	Configure_MultiEval_Spectrum.rst
	Configure_MultiEval_Srs.rst
	Configure_MultiEval_Tmode.rst