Average
----------------------------------------



.. rubric:: SCPI Commands :

.. index::
	single: READ:LTE:MEASurement<Instance>:PRACh:TRACe:PDYNamics:AVERage
	single: FETCh:LTE:MEASurement<Instance>:PRACh:TRACe:PDYNamics:AVERage

.. code-block:: python

	READ:LTE:MEASurement<Instance>:PRACh:TRACe:PDYNamics:AVERage
	FETCh:LTE:MEASurement<Instance>:PRACh:TRACe:PDYNamics:AVERage



.. autoclass:: RsCmwLteMeas.Implementations.Prach.Trace.Pdynamics.Average.AverageCls
	:members:
	:undoc-members:
	:noindex: