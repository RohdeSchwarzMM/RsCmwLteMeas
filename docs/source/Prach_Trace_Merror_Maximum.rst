Maximum
----------------------------------------



.. rubric:: SCPI Commands :

.. index::
	single: READ:LTE:MEASurement<Instance>:PRACh:TRACe:MERRor:MAXimum
	single: FETCh:LTE:MEASurement<Instance>:PRACh:TRACe:MERRor:MAXimum

.. code-block:: python

	READ:LTE:MEASurement<Instance>:PRACh:TRACe:MERRor:MAXimum
	FETCh:LTE:MEASurement<Instance>:PRACh:TRACe:MERRor:MAXimum



.. autoclass:: RsCmwLteMeas.Implementations.Prach.Trace.Merror.Maximum.MaximumCls
	:members:
	:undoc-members:
	:noindex: