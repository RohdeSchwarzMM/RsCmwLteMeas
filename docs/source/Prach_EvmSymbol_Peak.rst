Peak
----------------------------------------





.. autoclass:: RsCmwLteMeas.Implementations.Prach.EvmSymbol.Peak.PeakCls
	:members:
	:undoc-members:
	:noindex:


.. rubric:: Cloning the Group

.. code-block:: python

	# Create a clone of the original group, that exists independently
	group2 = driver.prach.evmSymbol.peak.clone()



.. rubric:: Subgroups
.. toctree::
	:maxdepth: 6
	:glob:

	Prach_EvmSymbol_Peak_Average.rst
	Prach_EvmSymbol_Peak_Current.rst
	Prach_EvmSymbol_Peak_Maximum.rst