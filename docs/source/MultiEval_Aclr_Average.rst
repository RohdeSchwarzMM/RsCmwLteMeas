Average
----------------------------------------



.. rubric:: SCPI Commands :

.. index::
	single: READ:LTE:MEASurement<Instance>:MEValuation:ACLR:AVERage
	single: FETCh:LTE:MEASurement<Instance>:MEValuation:ACLR:AVERage
	single: CALCulate:LTE:MEASurement<Instance>:MEValuation:ACLR:AVERage

.. code-block:: python

	READ:LTE:MEASurement<Instance>:MEValuation:ACLR:AVERage
	FETCh:LTE:MEASurement<Instance>:MEValuation:ACLR:AVERage
	CALCulate:LTE:MEASurement<Instance>:MEValuation:ACLR:AVERage



.. autoclass:: RsCmwLteMeas.Implementations.MultiEval.Aclr.Average.AverageCls
	:members:
	:undoc-members:
	:noindex: