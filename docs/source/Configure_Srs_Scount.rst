Scount
----------------------------------------



.. rubric:: SCPI Command :

.. index::
	single: CONFigure:LTE:MEASurement<Instance>:SRS:SCOunt:PDYNamics

.. code-block:: python

	CONFigure:LTE:MEASurement<Instance>:SRS:SCOunt:PDYNamics



.. autoclass:: RsCmwLteMeas.Implementations.Configure.Srs.Scount.ScountCls
	:members:
	:undoc-members:
	:noindex: