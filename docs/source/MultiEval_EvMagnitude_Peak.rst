Peak
----------------------------------------





.. autoclass:: RsCmwLteMeas.Implementations.MultiEval.EvMagnitude.Peak.PeakCls
	:members:
	:undoc-members:
	:noindex:


.. rubric:: Cloning the Group

.. code-block:: python

	# Create a clone of the original group, that exists independently
	group2 = driver.multiEval.evMagnitude.peak.clone()



.. rubric:: Subgroups
.. toctree::
	:maxdepth: 6
	:glob:

	MultiEval_EvMagnitude_Peak_Average.rst
	MultiEval_EvMagnitude_Peak_Current.rst
	MultiEval_EvMagnitude_Peak_Maximum.rst