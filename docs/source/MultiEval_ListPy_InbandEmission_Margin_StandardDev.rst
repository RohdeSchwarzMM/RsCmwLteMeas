StandardDev
----------------------------------------



.. rubric:: SCPI Command :

.. index::
	single: FETCh:LTE:MEASurement<Instance>:MEValuation:LIST:IEMission:MARGin:SDEViation

.. code-block:: python

	FETCh:LTE:MEASurement<Instance>:MEValuation:LIST:IEMission:MARGin:SDEViation



.. autoclass:: RsCmwLteMeas.Implementations.MultiEval.ListPy.InbandEmission.Margin.StandardDev.StandardDevCls
	:members:
	:undoc-members:
	:noindex: