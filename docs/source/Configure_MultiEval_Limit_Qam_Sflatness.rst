Sflatness
----------------------------------------



.. rubric:: SCPI Command :

.. index::
	single: CONFigure:LTE:MEASurement<Instance>:MEValuation:LIMit:QAM<ModOrder>:SFLatness

.. code-block:: python

	CONFigure:LTE:MEASurement<Instance>:MEValuation:LIMit:QAM<ModOrder>:SFLatness



.. autoclass:: RsCmwLteMeas.Implementations.Configure.MultiEval.Limit.Qam.Sflatness.SflatnessCls
	:members:
	:undoc-members:
	:noindex: