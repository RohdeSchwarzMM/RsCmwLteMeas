Average
----------------------------------------



.. rubric:: SCPI Commands :

.. index::
	single: READ:LTE:MEASurement<Instance>:MEValuation:PMONitor:CC<Nr>:AVERage
	single: FETCh:LTE:MEASurement<Instance>:MEValuation:PMONitor:CC<Nr>:AVERage

.. code-block:: python

	READ:LTE:MEASurement<Instance>:MEValuation:PMONitor:CC<Nr>:AVERage
	FETCh:LTE:MEASurement<Instance>:MEValuation:PMONitor:CC<Nr>:AVERage



.. autoclass:: RsCmwLteMeas.Implementations.MultiEval.Pmonitor.Cc.Average.AverageCls
	:members:
	:undoc-members:
	:noindex: