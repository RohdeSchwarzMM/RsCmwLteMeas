CarrierAggregation
----------------------------------------





.. autoclass:: RsCmwLteMeas.Implementations.Configure.MultiEval.Limit.Aclr.Eutra.CarrierAggregation.CarrierAggregationCls
	:members:
	:undoc-members:
	:noindex:


.. rubric:: Cloning the Group

.. code-block:: python

	# Create a clone of the original group, that exists independently
	group2 = driver.configure.multiEval.limit.aclr.eutra.carrierAggregation.clone()



.. rubric:: Subgroups
.. toctree::
	:maxdepth: 6
	:glob:

	Configure_MultiEval_Limit_Aclr_Eutra_CarrierAggregation_ChannelBw1st.rst
	Configure_MultiEval_Limit_Aclr_Eutra_CarrierAggregation_Ocombination.rst