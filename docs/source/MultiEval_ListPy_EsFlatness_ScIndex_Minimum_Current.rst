Current
----------------------------------------



.. rubric:: SCPI Command :

.. index::
	single: FETCh:LTE:MEASurement<Instance>:MEValuation:LIST:ESFLatness:SCINdex:MINimum<nr>:CURRent

.. code-block:: python

	FETCh:LTE:MEASurement<Instance>:MEValuation:LIST:ESFLatness:SCINdex:MINimum<nr>:CURRent



.. autoclass:: RsCmwLteMeas.Implementations.MultiEval.ListPy.EsFlatness.ScIndex.Minimum.Current.CurrentCls
	:members:
	:undoc-members:
	:noindex: