Current
----------------------------------------



.. rubric:: SCPI Commands :

.. index::
	single: READ:LTE:MEASurement<Instance>:MEValuation:EVMagnitude:CURRent
	single: FETCh:LTE:MEASurement<Instance>:MEValuation:EVMagnitude:CURRent

.. code-block:: python

	READ:LTE:MEASurement<Instance>:MEValuation:EVMagnitude:CURRent
	FETCh:LTE:MEASurement<Instance>:MEValuation:EVMagnitude:CURRent



.. autoclass:: RsCmwLteMeas.Implementations.MultiEval.EvMagnitude.Current.CurrentCls
	:members:
	:undoc-members:
	:noindex: