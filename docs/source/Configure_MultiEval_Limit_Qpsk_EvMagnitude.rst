EvMagnitude
----------------------------------------



.. rubric:: SCPI Command :

.. index::
	single: CONFigure:LTE:MEASurement<Instance>:MEValuation:LIMit:QPSK:EVMagnitude

.. code-block:: python

	CONFigure:LTE:MEASurement<Instance>:MEValuation:LIMit:QPSK:EVMagnitude



.. autoclass:: RsCmwLteMeas.Implementations.Configure.MultiEval.Limit.Qpsk.EvMagnitude.EvMagnitudeCls
	:members:
	:undoc-members:
	:noindex: