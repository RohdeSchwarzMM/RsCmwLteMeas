Maximum
----------------------------------------



.. rubric:: SCPI Commands :

.. index::
	single: READ:LTE:MEASurement<Instance>:PRACh:EVMSymbol:PEAK:MAXimum
	single: FETCh:LTE:MEASurement<Instance>:PRACh:EVMSymbol:PEAK:MAXimum

.. code-block:: python

	READ:LTE:MEASurement<Instance>:PRACh:EVMSymbol:PEAK:MAXimum
	FETCh:LTE:MEASurement<Instance>:PRACh:EVMSymbol:PEAK:MAXimum



.. autoclass:: RsCmwLteMeas.Implementations.Prach.EvmSymbol.Peak.Maximum.MaximumCls
	:members:
	:undoc-members:
	:noindex: