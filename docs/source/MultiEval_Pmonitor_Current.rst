Current
----------------------------------------



.. rubric:: SCPI Commands :

.. index::
	single: READ:LTE:MEASurement<Instance>:MEValuation:PMONitor:CURRent
	single: FETCh:LTE:MEASurement<Instance>:MEValuation:PMONitor:CURRent
	single: CALCulate:LTE:MEASurement<Instance>:MEValuation:PMONitor:CURRent

.. code-block:: python

	READ:LTE:MEASurement<Instance>:MEValuation:PMONitor:CURRent
	FETCh:LTE:MEASurement<Instance>:MEValuation:PMONitor:CURRent
	CALCulate:LTE:MEASurement<Instance>:MEValuation:PMONitor:CURRent



.. autoclass:: RsCmwLteMeas.Implementations.MultiEval.Pmonitor.Current.CurrentCls
	:members:
	:undoc-members:
	:noindex: