Extreme
----------------------------------------



.. rubric:: SCPI Command :

.. index::
	single: FETCh:LTE:MEASurement<Instance>:MEValuation:IEMission:CC<Nr>:MARGin:EXTReme

.. code-block:: python

	FETCh:LTE:MEASurement<Instance>:MEValuation:IEMission:CC<Nr>:MARGin:EXTReme



.. autoclass:: RsCmwLteMeas.Implementations.MultiEval.InbandEmission.Cc.Margin.Extreme.ExtremeCls
	:members:
	:undoc-members:
	:noindex:


.. rubric:: Cloning the Group

.. code-block:: python

	# Create a clone of the original group, that exists independently
	group2 = driver.multiEval.inbandEmission.cc.margin.extreme.clone()



.. rubric:: Subgroups
.. toctree::
	:maxdepth: 6
	:glob:

	MultiEval_InbandEmission_Cc_Margin_Extreme_RbIndex.rst