StandardDev
----------------------------------------



.. rubric:: SCPI Commands :

.. index::
	single: READ:LTE:MEASurement<Instance>:MEValuation:PMONitor:SDEViation
	single: FETCh:LTE:MEASurement<Instance>:MEValuation:PMONitor:SDEViation

.. code-block:: python

	READ:LTE:MEASurement<Instance>:MEValuation:PMONitor:SDEViation
	FETCh:LTE:MEASurement<Instance>:MEValuation:PMONitor:SDEViation



.. autoclass:: RsCmwLteMeas.Implementations.MultiEval.Pmonitor.StandardDev.StandardDevCls
	:members:
	:undoc-members:
	:noindex: