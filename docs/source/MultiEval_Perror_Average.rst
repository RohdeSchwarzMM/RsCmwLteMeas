Average
----------------------------------------



.. rubric:: SCPI Commands :

.. index::
	single: READ:LTE:MEASurement<Instance>:MEValuation:PERRor:AVERage
	single: FETCh:LTE:MEASurement<Instance>:MEValuation:PERRor:AVERage

.. code-block:: python

	READ:LTE:MEASurement<Instance>:MEValuation:PERRor:AVERage
	FETCh:LTE:MEASurement<Instance>:MEValuation:PERRor:AVERage



.. autoclass:: RsCmwLteMeas.Implementations.MultiEval.Perror.Average.AverageCls
	:members:
	:undoc-members:
	:noindex: