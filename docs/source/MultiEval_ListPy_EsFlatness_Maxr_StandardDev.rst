StandardDev
----------------------------------------



.. rubric:: SCPI Command :

.. index::
	single: FETCh:LTE:MEASurement<Instance>:MEValuation:LIST:ESFLatness:MAXR<nr>:SDEViation

.. code-block:: python

	FETCh:LTE:MEASurement<Instance>:MEValuation:LIST:ESFLatness:MAXR<nr>:SDEViation



.. autoclass:: RsCmwLteMeas.Implementations.MultiEval.ListPy.EsFlatness.Maxr.StandardDev.StandardDevCls
	:members:
	:undoc-members:
	:noindex: