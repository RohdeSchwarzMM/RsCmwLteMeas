Maximum
----------------------------------------



.. rubric:: SCPI Commands :

.. index::
	single: FETCh:LTE:MEASurement<Instance>:MEValuation:LIST:MODulation:PPOWer:MAXimum
	single: CALCulate:LTE:MEASurement<Instance>:MEValuation:LIST:MODulation:PPOWer:MAXimum

.. code-block:: python

	FETCh:LTE:MEASurement<Instance>:MEValuation:LIST:MODulation:PPOWer:MAXimum
	CALCulate:LTE:MEASurement<Instance>:MEValuation:LIST:MODulation:PPOWer:MAXimum



.. autoclass:: RsCmwLteMeas.Implementations.MultiEval.ListPy.Modulation.Ppower.Maximum.MaximumCls
	:members:
	:undoc-members:
	:noindex: