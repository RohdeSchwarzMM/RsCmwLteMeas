Margin
----------------------------------------





.. autoclass:: RsCmwLteMeas.Implementations.MultiEval.ListPy.InbandEmission.Margin.MarginCls
	:members:
	:undoc-members:
	:noindex:


.. rubric:: Cloning the Group

.. code-block:: python

	# Create a clone of the original group, that exists independently
	group2 = driver.multiEval.listPy.inbandEmission.margin.clone()



.. rubric:: Subgroups
.. toctree::
	:maxdepth: 6
	:glob:

	MultiEval_ListPy_InbandEmission_Margin_Average.rst
	MultiEval_ListPy_InbandEmission_Margin_Current.rst
	MultiEval_ListPy_InbandEmission_Margin_Extreme.rst
	MultiEval_ListPy_InbandEmission_Margin_RbIndex.rst
	MultiEval_ListPy_InbandEmission_Margin_StandardDev.rst