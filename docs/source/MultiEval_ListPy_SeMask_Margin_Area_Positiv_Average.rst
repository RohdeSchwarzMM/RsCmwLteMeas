Average
----------------------------------------



.. rubric:: SCPI Command :

.. index::
	single: FETCh:LTE:MEASurement<Instance>:MEValuation:LIST:SEMask:MARGin:AREA<nr>:POSitiv:AVERage

.. code-block:: python

	FETCh:LTE:MEASurement<Instance>:MEValuation:LIST:SEMask:MARGin:AREA<nr>:POSitiv:AVERage



.. autoclass:: RsCmwLteMeas.Implementations.MultiEval.ListPy.SeMask.Margin.Area.Positiv.Average.AverageCls
	:members:
	:undoc-members:
	:noindex: