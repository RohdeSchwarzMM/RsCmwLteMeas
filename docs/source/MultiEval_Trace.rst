Trace
----------------------------------------





.. autoclass:: RsCmwLteMeas.Implementations.MultiEval.Trace.TraceCls
	:members:
	:undoc-members:
	:noindex:


.. rubric:: Cloning the Group

.. code-block:: python

	# Create a clone of the original group, that exists independently
	group2 = driver.multiEval.trace.clone()



.. rubric:: Subgroups
.. toctree::
	:maxdepth: 6
	:glob:

	MultiEval_Trace_Aclr.rst
	MultiEval_Trace_EsFlatness.rst
	MultiEval_Trace_Evmc.rst
	MultiEval_Trace_EvmSymbol.rst
	MultiEval_Trace_Iemissions.rst
	MultiEval_Trace_Iq.rst
	MultiEval_Trace_Pdynamics.rst
	MultiEval_Trace_Pmonitor.rst
	MultiEval_Trace_RbaTable.rst
	MultiEval_Trace_SeMask.rst