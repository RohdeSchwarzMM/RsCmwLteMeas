Minimum
----------------------------------------



.. rubric:: SCPI Commands :

.. index::
	single: READ:LTE:MEASurement<Instance>:SRS:PDYNamics:MINimum
	single: FETCh:LTE:MEASurement<Instance>:SRS:PDYNamics:MINimum
	single: CALCulate:LTE:MEASurement<Instance>:SRS:PDYNamics:MINimum

.. code-block:: python

	READ:LTE:MEASurement<Instance>:SRS:PDYNamics:MINimum
	FETCh:LTE:MEASurement<Instance>:SRS:PDYNamics:MINimum
	CALCulate:LTE:MEASurement<Instance>:SRS:PDYNamics:MINimum



.. autoclass:: RsCmwLteMeas.Implementations.Srs.Pdynamics.Minimum.MinimumCls
	:members:
	:undoc-members:
	:noindex: