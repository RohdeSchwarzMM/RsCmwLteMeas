Average
----------------------------------------



.. rubric:: SCPI Commands :

.. index::
	single: READ:LTE:MEASurement<Instance>:PRACh:TRACe:MERRor:AVERage
	single: FETCh:LTE:MEASurement<Instance>:PRACh:TRACe:MERRor:AVERage

.. code-block:: python

	READ:LTE:MEASurement<Instance>:PRACh:TRACe:MERRor:AVERage
	FETCh:LTE:MEASurement<Instance>:PRACh:TRACe:MERRor:AVERage



.. autoclass:: RsCmwLteMeas.Implementations.Prach.Trace.Merror.Average.AverageCls
	:members:
	:undoc-members:
	:noindex: