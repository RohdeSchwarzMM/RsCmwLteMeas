Pdynamics
----------------------------------------





.. autoclass:: RsCmwLteMeas.Implementations.Srs.Pdynamics.PdynamicsCls
	:members:
	:undoc-members:
	:noindex:


.. rubric:: Cloning the Group

.. code-block:: python

	# Create a clone of the original group, that exists independently
	group2 = driver.srs.pdynamics.clone()



.. rubric:: Subgroups
.. toctree::
	:maxdepth: 6
	:glob:

	Srs_Pdynamics_Average.rst
	Srs_Pdynamics_Current.rst
	Srs_Pdynamics_Maximum.rst
	Srs_Pdynamics_Minimum.rst
	Srs_Pdynamics_StandardDev.rst