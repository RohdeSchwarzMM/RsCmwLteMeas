Tpower
----------------------------------------





.. autoclass:: RsCmwLteMeas.Implementations.MultiEval.ListPy.Modulation.Tpower.TpowerCls
	:members:
	:undoc-members:
	:noindex:


.. rubric:: Cloning the Group

.. code-block:: python

	# Create a clone of the original group, that exists independently
	group2 = driver.multiEval.listPy.modulation.tpower.clone()



.. rubric:: Subgroups
.. toctree::
	:maxdepth: 6
	:glob:

	MultiEval_ListPy_Modulation_Tpower_Average.rst
	MultiEval_ListPy_Modulation_Tpower_Current.rst
	MultiEval_ListPy_Modulation_Tpower_Maximum.rst
	MultiEval_ListPy_Modulation_Tpower_Minimum.rst
	MultiEval_ListPy_Modulation_Tpower_StandardDev.rst