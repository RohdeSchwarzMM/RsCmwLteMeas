Average
----------------------------------------



.. rubric:: SCPI Commands :

.. index::
	single: READ:LTE:MEASurement<Instance>:MEValuation:SEMask:AVERage
	single: FETCh:LTE:MEASurement<Instance>:MEValuation:SEMask:AVERage
	single: CALCulate:LTE:MEASurement<Instance>:MEValuation:SEMask:AVERage

.. code-block:: python

	READ:LTE:MEASurement<Instance>:MEValuation:SEMask:AVERage
	FETCh:LTE:MEASurement<Instance>:MEValuation:SEMask:AVERage
	CALCulate:LTE:MEASurement<Instance>:MEValuation:SEMask:AVERage



.. autoclass:: RsCmwLteMeas.Implementations.MultiEval.SeMask.Average.AverageCls
	:members:
	:undoc-members:
	:noindex: