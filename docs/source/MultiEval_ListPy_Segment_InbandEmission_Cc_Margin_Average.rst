Average
----------------------------------------



.. rubric:: SCPI Command :

.. index::
	single: FETCh:LTE:MEASurement<Instance>:MEValuation:LIST:SEGMent<nr>:IEMission:CC<c>:MARGin:AVERage

.. code-block:: python

	FETCh:LTE:MEASurement<Instance>:MEValuation:LIST:SEGMent<nr>:IEMission:CC<c>:MARGin:AVERage



.. autoclass:: RsCmwLteMeas.Implementations.MultiEval.ListPy.Segment.InbandEmission.Cc.Margin.Average.AverageCls
	:members:
	:undoc-members:
	:noindex: