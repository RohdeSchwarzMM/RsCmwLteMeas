Extreme
----------------------------------------



.. rubric:: SCPI Commands :

.. index::
	single: FETCh:LTE:MEASurement<Instance>:MEValuation:LIST:MODulation:IQOFfset:EXTReme
	single: CALCulate:LTE:MEASurement<Instance>:MEValuation:LIST:MODulation:IQOFfset:EXTReme

.. code-block:: python

	FETCh:LTE:MEASurement<Instance>:MEValuation:LIST:MODulation:IQOFfset:EXTReme
	CALCulate:LTE:MEASurement<Instance>:MEValuation:LIST:MODulation:IQOFfset:EXTReme



.. autoclass:: RsCmwLteMeas.Implementations.MultiEval.ListPy.Modulation.IqOffset.Extreme.ExtremeCls
	:members:
	:undoc-members:
	:noindex: