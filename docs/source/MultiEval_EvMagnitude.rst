EvMagnitude
----------------------------------------





.. autoclass:: RsCmwLteMeas.Implementations.MultiEval.EvMagnitude.EvMagnitudeCls
	:members:
	:undoc-members:
	:noindex:


.. rubric:: Cloning the Group

.. code-block:: python

	# Create a clone of the original group, that exists independently
	group2 = driver.multiEval.evMagnitude.clone()



.. rubric:: Subgroups
.. toctree::
	:maxdepth: 6
	:glob:

	MultiEval_EvMagnitude_Average.rst
	MultiEval_EvMagnitude_Current.rst
	MultiEval_EvMagnitude_Maximum.rst
	MultiEval_EvMagnitude_Peak.rst