RsCmwLteMeas API Structure
========================================


.. rubric:: Global RepCaps

.. code-block:: python
	
	driver = RsCmwLteMeas('TCPIP::192.168.2.101::hislip0')
	# Instance range: Inst1 .. Inst16
	rc = driver.repcap_instance_get()
	driver.repcap_instance_set(repcap.Instance.Inst1)

.. autoclass:: RsCmwLteMeas.RsCmwLteMeas
	:members:
	:undoc-members:
	:noindex:

.. rubric:: Subgroups

.. toctree::
	:maxdepth: 6
	:glob:

	Configure.rst
	MultiEval.rst
	Prach.rst
	Route.rst
	Sense.rst
	Srs.rst
	Trigger.rst