FreqError
----------------------------------------



.. rubric:: SCPI Command :

.. index::
	single: CONFigure:LTE:MEASurement<Instance>:MEValuation:LIMit:QAM<ModOrder>:FERRor

.. code-block:: python

	CONFigure:LTE:MEASurement<Instance>:MEValuation:LIMit:QAM<ModOrder>:FERRor



.. autoclass:: RsCmwLteMeas.Implementations.Configure.MultiEval.Limit.Qam.FreqError.FreqErrorCls
	:members:
	:undoc-members:
	:noindex: