ScIndex
----------------------------------------



.. rubric:: SCPI Command :

.. index::
	single: FETCh:LTE:MEASurement<Instance>:MEValuation:ESFLatness:CURRent:SCINdex

.. code-block:: python

	FETCh:LTE:MEASurement<Instance>:MEValuation:ESFLatness:CURRent:SCINdex



.. autoclass:: RsCmwLteMeas.Implementations.MultiEval.EsFlatness.Current.ScIndex.ScIndexCls
	:members:
	:undoc-members:
	:noindex: