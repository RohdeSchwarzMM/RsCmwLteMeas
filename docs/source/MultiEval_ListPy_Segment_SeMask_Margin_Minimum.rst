Minimum
----------------------------------------





.. autoclass:: RsCmwLteMeas.Implementations.MultiEval.ListPy.Segment.SeMask.Margin.Minimum.MinimumCls
	:members:
	:undoc-members:
	:noindex:


.. rubric:: Cloning the Group

.. code-block:: python

	# Create a clone of the original group, that exists independently
	group2 = driver.multiEval.listPy.segment.seMask.margin.minimum.clone()



.. rubric:: Subgroups
.. toctree::
	:maxdepth: 6
	:glob:

	MultiEval_ListPy_Segment_SeMask_Margin_Minimum_Negativ.rst
	MultiEval_ListPy_Segment_SeMask_Margin_Minimum_Positiv.rst