Peak
----------------------------------------



.. rubric:: SCPI Command :

.. index::
	single: FETCh:LTE:MEASurement<Instance>:MEValuation:DMARker<No>:EVMagnitude:PEAK

.. code-block:: python

	FETCh:LTE:MEASurement<Instance>:MEValuation:DMARker<No>:EVMagnitude:PEAK



.. autoclass:: RsCmwLteMeas.Implementations.MultiEval.Dmarker.EvMagnitude.Peak.PeakCls
	:members:
	:undoc-members:
	:noindex: