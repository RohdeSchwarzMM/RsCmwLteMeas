Minr<MinRange>
----------------------------------------

.. rubric:: RepCap Settings

.. code-block:: python

	# Range: Nr1 .. Nr2
	rc = driver.multiEval.listPy.esFlatness.minr.repcap_minRange_get()
	driver.multiEval.listPy.esFlatness.minr.repcap_minRange_set(repcap.MinRange.Nr1)





.. autoclass:: RsCmwLteMeas.Implementations.MultiEval.ListPy.EsFlatness.Minr.MinrCls
	:members:
	:undoc-members:
	:noindex:


.. rubric:: Cloning the Group

.. code-block:: python

	# Create a clone of the original group, that exists independently
	group2 = driver.multiEval.listPy.esFlatness.minr.clone()



.. rubric:: Subgroups
.. toctree::
	:maxdepth: 6
	:glob:

	MultiEval_ListPy_EsFlatness_Minr_Average.rst
	MultiEval_ListPy_EsFlatness_Minr_Current.rst
	MultiEval_ListPy_EsFlatness_Minr_Extreme.rst
	MultiEval_ListPy_EsFlatness_Minr_StandardDev.rst