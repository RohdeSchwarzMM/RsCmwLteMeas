StandardDev
----------------------------------------



.. rubric:: SCPI Commands :

.. index::
	single: READ:LTE:MEASurement<Instance>:MEValuation:PMONitor:CC<Nr>:SDEViation
	single: FETCh:LTE:MEASurement<Instance>:MEValuation:PMONitor:CC<Nr>:SDEViation

.. code-block:: python

	READ:LTE:MEASurement<Instance>:MEValuation:PMONitor:CC<Nr>:SDEViation
	FETCh:LTE:MEASurement<Instance>:MEValuation:PMONitor:CC<Nr>:SDEViation



.. autoclass:: RsCmwLteMeas.Implementations.MultiEval.Pmonitor.Cc.StandardDev.StandardDevCls
	:members:
	:undoc-members:
	:noindex: