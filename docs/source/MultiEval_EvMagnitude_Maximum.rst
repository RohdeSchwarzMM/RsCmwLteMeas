Maximum
----------------------------------------



.. rubric:: SCPI Commands :

.. index::
	single: READ:LTE:MEASurement<Instance>:MEValuation:EVMagnitude:MAXimum
	single: FETCh:LTE:MEASurement<Instance>:MEValuation:EVMagnitude:MAXimum

.. code-block:: python

	READ:LTE:MEASurement<Instance>:MEValuation:EVMagnitude:MAXimum
	FETCh:LTE:MEASurement<Instance>:MEValuation:EVMagnitude:MAXimum



.. autoclass:: RsCmwLteMeas.Implementations.MultiEval.EvMagnitude.Maximum.MaximumCls
	:members:
	:undoc-members:
	:noindex: