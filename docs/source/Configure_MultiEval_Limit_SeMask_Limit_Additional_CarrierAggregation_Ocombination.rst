Ocombination
----------------------------------------



.. rubric:: SCPI Command :

.. index::
	single: CONFigure:LTE:MEASurement<Instance>:MEValuation:LIMit:SEMask:LIMit<nr>:ADDitional<Table>:CAGGregation:OCOMbination

.. code-block:: python

	CONFigure:LTE:MEASurement<Instance>:MEValuation:LIMit:SEMask:LIMit<nr>:ADDitional<Table>:CAGGregation:OCOMbination



.. autoclass:: RsCmwLteMeas.Implementations.Configure.MultiEval.Limit.SeMask.Limit.Additional.CarrierAggregation.Ocombination.OcombinationCls
	:members:
	:undoc-members:
	:noindex: