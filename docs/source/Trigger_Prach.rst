Prach
----------------------------------------



.. rubric:: SCPI Commands :

.. index::
	single: TRIGger:LTE:MEASurement<Instance>:PRACh:SOURce
	single: TRIGger:LTE:MEASurement<Instance>:PRACh:THReshold
	single: TRIGger:LTE:MEASurement<Instance>:PRACh:SLOPe
	single: TRIGger:LTE:MEASurement<Instance>:PRACh:TOUT
	single: TRIGger:LTE:MEASurement<Instance>:PRACh:MGAP

.. code-block:: python

	TRIGger:LTE:MEASurement<Instance>:PRACh:SOURce
	TRIGger:LTE:MEASurement<Instance>:PRACh:THReshold
	TRIGger:LTE:MEASurement<Instance>:PRACh:SLOPe
	TRIGger:LTE:MEASurement<Instance>:PRACh:TOUT
	TRIGger:LTE:MEASurement<Instance>:PRACh:MGAP



.. autoclass:: RsCmwLteMeas.Implementations.Trigger.Prach.PrachCls
	:members:
	:undoc-members:
	:noindex:


.. rubric:: Cloning the Group

.. code-block:: python

	# Create a clone of the original group, that exists independently
	group2 = driver.trigger.prach.clone()



.. rubric:: Subgroups
.. toctree::
	:maxdepth: 6
	:glob:

	Trigger_Prach_Catalog.rst