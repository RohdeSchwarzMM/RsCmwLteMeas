Merror
----------------------------------------



.. rubric:: SCPI Command :

.. index::
	single: CONFigure:LTE:MEASurement<Instance>:MEValuation:LIMit:QPSK:MERRor

.. code-block:: python

	CONFigure:LTE:MEASurement<Instance>:MEValuation:LIMit:QPSK:MERRor



.. autoclass:: RsCmwLteMeas.Implementations.Configure.MultiEval.Limit.Qpsk.Merror.MerrorCls
	:members:
	:undoc-members:
	:noindex: