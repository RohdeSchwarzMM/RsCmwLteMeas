MultiEval
----------------------------------------



.. rubric:: SCPI Commands :

.. index::
	single: TRIGger:LTE:MEASurement<Instance>:MEValuation:SOURce
	single: TRIGger:LTE:MEASurement<Instance>:MEValuation:THReshold
	single: TRIGger:LTE:MEASurement<Instance>:MEValuation:SLOPe
	single: TRIGger:LTE:MEASurement<Instance>:MEValuation:DELay
	single: TRIGger:LTE:MEASurement<Instance>:MEValuation:TOUT
	single: TRIGger:LTE:MEASurement<Instance>:MEValuation:MGAP
	single: TRIGger:LTE:MEASurement<Instance>:MEValuation:SMODe
	single: TRIGger:LTE:MEASurement<Instance>:MEValuation:AMODe

.. code-block:: python

	TRIGger:LTE:MEASurement<Instance>:MEValuation:SOURce
	TRIGger:LTE:MEASurement<Instance>:MEValuation:THReshold
	TRIGger:LTE:MEASurement<Instance>:MEValuation:SLOPe
	TRIGger:LTE:MEASurement<Instance>:MEValuation:DELay
	TRIGger:LTE:MEASurement<Instance>:MEValuation:TOUT
	TRIGger:LTE:MEASurement<Instance>:MEValuation:MGAP
	TRIGger:LTE:MEASurement<Instance>:MEValuation:SMODe
	TRIGger:LTE:MEASurement<Instance>:MEValuation:AMODe



.. autoclass:: RsCmwLteMeas.Implementations.Trigger.MultiEval.MultiEvalCls
	:members:
	:undoc-members:
	:noindex:


.. rubric:: Cloning the Group

.. code-block:: python

	# Create a clone of the original group, that exists independently
	group2 = driver.trigger.multiEval.clone()



.. rubric:: Subgroups
.. toctree::
	:maxdepth: 6
	:glob:

	Trigger_MultiEval_Catalog.rst
	Trigger_MultiEval_ListPy.rst