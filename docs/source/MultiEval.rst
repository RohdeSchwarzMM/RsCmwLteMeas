MultiEval
----------------------------------------



.. rubric:: SCPI Commands :

.. index::
	single: INITiate:LTE:MEASurement<Instance>:MEValuation
	single: STOP:LTE:MEASurement<Instance>:MEValuation
	single: ABORt:LTE:MEASurement<Instance>:MEValuation

.. code-block:: python

	INITiate:LTE:MEASurement<Instance>:MEValuation
	STOP:LTE:MEASurement<Instance>:MEValuation
	ABORt:LTE:MEASurement<Instance>:MEValuation



.. autoclass:: RsCmwLteMeas.Implementations.MultiEval.MultiEvalCls
	:members:
	:undoc-members:
	:noindex:


.. rubric:: Cloning the Group

.. code-block:: python

	# Create a clone of the original group, that exists independently
	group2 = driver.multiEval.clone()



.. rubric:: Subgroups
.. toctree::
	:maxdepth: 6
	:glob:

	MultiEval_Aclr.rst
	MultiEval_Amarker.rst
	MultiEval_Bler.rst
	MultiEval_Dmarker.rst
	MultiEval_EsFlatness.rst
	MultiEval_EvMagnitude.rst
	MultiEval_Evmc.rst
	MultiEval_InbandEmission.rst
	MultiEval_ListPy.rst
	MultiEval_Merror.rst
	MultiEval_Modulation.rst
	MultiEval_Pdynamics.rst
	MultiEval_Perror.rst
	MultiEval_Pmonitor.rst
	MultiEval_ReferenceMarker.rst
	MultiEval_SeMask.rst
	MultiEval_State.rst
	MultiEval_Trace.rst
	MultiEval_VfThroughput.rst