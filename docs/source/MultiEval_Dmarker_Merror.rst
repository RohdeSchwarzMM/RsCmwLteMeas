Merror
----------------------------------------



.. rubric:: SCPI Command :

.. index::
	single: FETCh:LTE:MEASurement<Instance>:MEValuation:DMARker<No>:MERRor

.. code-block:: python

	FETCh:LTE:MEASurement<Instance>:MEValuation:DMARker<No>:MERRor



.. autoclass:: RsCmwLteMeas.Implementations.MultiEval.Dmarker.Merror.MerrorCls
	:members:
	:undoc-members:
	:noindex: