Minimum
----------------------------------------



.. rubric:: SCPI Commands :

.. index::
	single: READ:LTE:MEASurement<Instance>:MEValuation:PMONitor:MINimum
	single: FETCh:LTE:MEASurement<Instance>:MEValuation:PMONitor:MINimum
	single: CALCulate:LTE:MEASurement<Instance>:MEValuation:PMONitor:MINimum

.. code-block:: python

	READ:LTE:MEASurement<Instance>:MEValuation:PMONitor:MINimum
	FETCh:LTE:MEASurement<Instance>:MEValuation:PMONitor:MINimum
	CALCulate:LTE:MEASurement<Instance>:MEValuation:PMONitor:MINimum



.. autoclass:: RsCmwLteMeas.Implementations.MultiEval.Pmonitor.Minimum.MinimumCls
	:members:
	:undoc-members:
	:noindex: