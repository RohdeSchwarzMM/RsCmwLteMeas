Cc<CarrierComponent>
----------------------------------------

.. rubric:: RepCap Settings

.. code-block:: python

	# Range: Nr1 .. Nr4
	rc = driver.multiEval.pmonitor.cc.repcap_carrierComponent_get()
	driver.multiEval.pmonitor.cc.repcap_carrierComponent_set(repcap.CarrierComponent.Nr1)





.. autoclass:: RsCmwLteMeas.Implementations.MultiEval.Pmonitor.Cc.CcCls
	:members:
	:undoc-members:
	:noindex:


.. rubric:: Cloning the Group

.. code-block:: python

	# Create a clone of the original group, that exists independently
	group2 = driver.multiEval.pmonitor.cc.clone()



.. rubric:: Subgroups
.. toctree::
	:maxdepth: 6
	:glob:

	MultiEval_Pmonitor_Cc_Average.rst
	MultiEval_Pmonitor_Cc_Current.rst
	MultiEval_Pmonitor_Cc_Maximum.rst
	MultiEval_Pmonitor_Cc_Minimum.rst
	MultiEval_Pmonitor_Cc_StandardDev.rst