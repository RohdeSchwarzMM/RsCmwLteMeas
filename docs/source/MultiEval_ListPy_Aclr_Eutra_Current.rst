Current
----------------------------------------



.. rubric:: SCPI Commands :

.. index::
	single: FETCh:LTE:MEASurement<Instance>:MEValuation:LIST:ACLR:EUTRa:CURRent
	single: CALCulate:LTE:MEASurement<Instance>:MEValuation:LIST:ACLR:EUTRa:CURRent

.. code-block:: python

	FETCh:LTE:MEASurement<Instance>:MEValuation:LIST:ACLR:EUTRa:CURRent
	CALCulate:LTE:MEASurement<Instance>:MEValuation:LIST:ACLR:EUTRa:CURRent



.. autoclass:: RsCmwLteMeas.Implementations.MultiEval.ListPy.Aclr.Eutra.Current.CurrentCls
	:members:
	:undoc-members:
	:noindex: