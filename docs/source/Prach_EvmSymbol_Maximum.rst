Maximum
----------------------------------------



.. rubric:: SCPI Commands :

.. index::
	single: READ:LTE:MEASurement<Instance>:PRACh:EVMSymbol:MAXimum
	single: FETCh:LTE:MEASurement<Instance>:PRACh:EVMSymbol:MAXimum

.. code-block:: python

	READ:LTE:MEASurement<Instance>:PRACh:EVMSymbol:MAXimum
	FETCh:LTE:MEASurement<Instance>:PRACh:EVMSymbol:MAXimum



.. autoclass:: RsCmwLteMeas.Implementations.Prach.EvmSymbol.Maximum.MaximumCls
	:members:
	:undoc-members:
	:noindex: