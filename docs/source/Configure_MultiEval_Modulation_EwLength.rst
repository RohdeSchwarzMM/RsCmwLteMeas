EwLength
----------------------------------------



.. rubric:: SCPI Command :

.. index::
	single: CONFigure:LTE:MEASurement<Instance>:MEValuation:MODulation:EWLength

.. code-block:: python

	CONFigure:LTE:MEASurement<Instance>:MEValuation:MODulation:EWLength



.. autoclass:: RsCmwLteMeas.Implementations.Configure.MultiEval.Modulation.EwLength.EwLengthCls
	:members:
	:undoc-members:
	:noindex:


.. rubric:: Cloning the Group

.. code-block:: python

	# Create a clone of the original group, that exists independently
	group2 = driver.configure.multiEval.modulation.ewLength.clone()



.. rubric:: Subgroups
.. toctree::
	:maxdepth: 6
	:glob:

	Configure_MultiEval_Modulation_EwLength_ChannelBw.rst