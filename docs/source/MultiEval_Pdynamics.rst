Pdynamics
----------------------------------------





.. autoclass:: RsCmwLteMeas.Implementations.MultiEval.Pdynamics.PdynamicsCls
	:members:
	:undoc-members:
	:noindex:


.. rubric:: Cloning the Group

.. code-block:: python

	# Create a clone of the original group, that exists independently
	group2 = driver.multiEval.pdynamics.clone()



.. rubric:: Subgroups
.. toctree::
	:maxdepth: 6
	:glob:

	MultiEval_Pdynamics_Average.rst
	MultiEval_Pdynamics_Current.rst
	MultiEval_Pdynamics_Maximum.rst
	MultiEval_Pdynamics_Minimum.rst
	MultiEval_Pdynamics_StandardDev.rst