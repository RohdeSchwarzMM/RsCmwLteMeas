DchType
----------------------------------------



.. rubric:: SCPI Command :

.. index::
	single: FETCh:LTE:MEASurement<Instance>:MEValuation:MODulation:DCHType

.. code-block:: python

	FETCh:LTE:MEASurement<Instance>:MEValuation:MODulation:DCHType



.. autoclass:: RsCmwLteMeas.Implementations.MultiEval.Modulation.DchType.DchTypeCls
	:members:
	:undoc-members:
	:noindex: