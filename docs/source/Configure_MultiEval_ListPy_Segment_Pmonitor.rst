Pmonitor
----------------------------------------



.. rubric:: SCPI Command :

.. index::
	single: CONFigure:LTE:MEASurement<Instance>:MEValuation:LIST:SEGMent<nr>:PMONitor

.. code-block:: python

	CONFigure:LTE:MEASurement<Instance>:MEValuation:LIST:SEGMent<nr>:PMONitor



.. autoclass:: RsCmwLteMeas.Implementations.Configure.MultiEval.ListPy.Segment.Pmonitor.PmonitorCls
	:members:
	:undoc-members:
	:noindex: