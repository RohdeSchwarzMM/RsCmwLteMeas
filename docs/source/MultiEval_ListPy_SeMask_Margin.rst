Margin
----------------------------------------





.. autoclass:: RsCmwLteMeas.Implementations.MultiEval.ListPy.SeMask.Margin.MarginCls
	:members:
	:undoc-members:
	:noindex:


.. rubric:: Cloning the Group

.. code-block:: python

	# Create a clone of the original group, that exists independently
	group2 = driver.multiEval.listPy.seMask.margin.clone()



.. rubric:: Subgroups
.. toctree::
	:maxdepth: 6
	:glob:

	MultiEval_ListPy_SeMask_Margin_Area.rst