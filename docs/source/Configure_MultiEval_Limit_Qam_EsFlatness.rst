EsFlatness
----------------------------------------



.. rubric:: SCPI Command :

.. index::
	single: CONFigure:LTE:MEASurement<Instance>:MEValuation:LIMit:QAM<ModOrder>:ESFLatness

.. code-block:: python

	CONFigure:LTE:MEASurement<Instance>:MEValuation:LIMit:QAM<ModOrder>:ESFLatness



.. autoclass:: RsCmwLteMeas.Implementations.Configure.MultiEval.Limit.Qam.EsFlatness.EsFlatnessCls
	:members:
	:undoc-members:
	:noindex: