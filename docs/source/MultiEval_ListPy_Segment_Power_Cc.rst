Cc<CarrierComponent>
----------------------------------------

.. rubric:: RepCap Settings

.. code-block:: python

	# Range: Nr1 .. Nr4
	rc = driver.multiEval.listPy.segment.power.cc.repcap_carrierComponent_get()
	driver.multiEval.listPy.segment.power.cc.repcap_carrierComponent_set(repcap.CarrierComponent.Nr1)





.. autoclass:: RsCmwLteMeas.Implementations.MultiEval.ListPy.Segment.Power.Cc.CcCls
	:members:
	:undoc-members:
	:noindex:


.. rubric:: Cloning the Group

.. code-block:: python

	# Create a clone of the original group, that exists independently
	group2 = driver.multiEval.listPy.segment.power.cc.clone()



.. rubric:: Subgroups
.. toctree::
	:maxdepth: 6
	:glob:

	MultiEval_ListPy_Segment_Power_Cc_Average.rst
	MultiEval_ListPy_Segment_Power_Cc_Current.rst
	MultiEval_ListPy_Segment_Power_Cc_Maximum.rst
	MultiEval_ListPy_Segment_Power_Cc_Minimum.rst
	MultiEval_ListPy_Segment_Power_Cc_StandardDev.rst