Mode
----------------------------------------



.. rubric:: SCPI Commands :

.. index::
	single: CONFigure:LTE:MEASurement<Instance>:CAGGregation:MODE:CSPath
	single: CONFigure:LTE:MEASurement<Instance>:CAGGregation:MODE

.. code-block:: python

	CONFigure:LTE:MEASurement<Instance>:CAGGregation:MODE:CSPath
	CONFigure:LTE:MEASurement<Instance>:CAGGregation:MODE



.. autoclass:: RsCmwLteMeas.Implementations.Configure.CarrierAggregation.Mode.ModeCls
	:members:
	:undoc-members:
	:noindex: