RbIndex
----------------------------------------



.. rubric:: SCPI Command :

.. index::
	single: FETCh:LTE:MEASurement<Instance>:MEValuation:IEMission:CC<Nr>:MARGin:EXTReme:RBINdex

.. code-block:: python

	FETCh:LTE:MEASurement<Instance>:MEValuation:IEMission:CC<Nr>:MARGin:EXTReme:RBINdex



.. autoclass:: RsCmwLteMeas.Implementations.MultiEval.InbandEmission.Cc.Margin.Extreme.RbIndex.RbIndexCls
	:members:
	:undoc-members:
	:noindex: