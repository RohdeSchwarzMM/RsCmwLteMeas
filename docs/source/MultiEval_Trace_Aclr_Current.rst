Current
----------------------------------------



.. rubric:: SCPI Commands :

.. index::
	single: READ:LTE:MEASurement<Instance>:MEValuation:TRACe:ACLR:CURRent
	single: FETCh:LTE:MEASurement<Instance>:MEValuation:TRACe:ACLR:CURRent

.. code-block:: python

	READ:LTE:MEASurement<Instance>:MEValuation:TRACe:ACLR:CURRent
	FETCh:LTE:MEASurement<Instance>:MEValuation:TRACe:ACLR:CURRent



.. autoclass:: RsCmwLteMeas.Implementations.MultiEval.Trace.Aclr.Current.CurrentCls
	:members:
	:undoc-members:
	:noindex: