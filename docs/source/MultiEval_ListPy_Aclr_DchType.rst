DchType
----------------------------------------



.. rubric:: SCPI Command :

.. index::
	single: FETCh:LTE:MEASurement<Instance>:MEValuation:LIST:ACLR:DCHType

.. code-block:: python

	FETCh:LTE:MEASurement<Instance>:MEValuation:LIST:ACLR:DCHType



.. autoclass:: RsCmwLteMeas.Implementations.MultiEval.ListPy.Aclr.DchType.DchTypeCls
	:members:
	:undoc-members:
	:noindex: