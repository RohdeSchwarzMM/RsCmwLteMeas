EvmSymbol
----------------------------------------





.. autoclass:: RsCmwLteMeas.Implementations.MultiEval.Trace.EvmSymbol.EvmSymbolCls
	:members:
	:undoc-members:
	:noindex:


.. rubric:: Cloning the Group

.. code-block:: python

	# Create a clone of the original group, that exists independently
	group2 = driver.multiEval.trace.evmSymbol.clone()



.. rubric:: Subgroups
.. toctree::
	:maxdepth: 6
	:glob:

	MultiEval_Trace_EvmSymbol_Average.rst
	MultiEval_Trace_EvmSymbol_Current.rst
	MultiEval_Trace_EvmSymbol_Maximum.rst