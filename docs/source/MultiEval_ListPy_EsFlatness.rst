EsFlatness
----------------------------------------





.. autoclass:: RsCmwLteMeas.Implementations.MultiEval.ListPy.EsFlatness.EsFlatnessCls
	:members:
	:undoc-members:
	:noindex:


.. rubric:: Cloning the Group

.. code-block:: python

	# Create a clone of the original group, that exists independently
	group2 = driver.multiEval.listPy.esFlatness.clone()



.. rubric:: Subgroups
.. toctree::
	:maxdepth: 6
	:glob:

	MultiEval_ListPy_EsFlatness_Difference.rst
	MultiEval_ListPy_EsFlatness_Maxr.rst
	MultiEval_ListPy_EsFlatness_Minr.rst
	MultiEval_ListPy_EsFlatness_Ripple.rst
	MultiEval_ListPy_EsFlatness_ScIndex.rst