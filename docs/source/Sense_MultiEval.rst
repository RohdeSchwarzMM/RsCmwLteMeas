MultiEval
----------------------------------------





.. autoclass:: RsCmwLteMeas.Implementations.Sense.MultiEval.MultiEvalCls
	:members:
	:undoc-members:
	:noindex:


.. rubric:: Cloning the Group

.. code-block:: python

	# Create a clone of the original group, that exists independently
	group2 = driver.sense.multiEval.clone()



.. rubric:: Subgroups
.. toctree::
	:maxdepth: 6
	:glob:

	Sense_MultiEval_Spectrum.rst