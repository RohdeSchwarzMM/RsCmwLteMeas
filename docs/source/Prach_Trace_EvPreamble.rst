EvPreamble
----------------------------------------



.. rubric:: SCPI Commands :

.. index::
	single: READ:LTE:MEASurement<Instance>:PRACh:TRACe:EVPReamble
	single: FETCh:LTE:MEASurement<Instance>:PRACh:TRACe:EVPReamble

.. code-block:: python

	READ:LTE:MEASurement<Instance>:PRACh:TRACe:EVPReamble
	FETCh:LTE:MEASurement<Instance>:PRACh:TRACe:EVPReamble



.. autoclass:: RsCmwLteMeas.Implementations.Prach.Trace.EvPreamble.EvPreambleCls
	:members:
	:undoc-members:
	:noindex: