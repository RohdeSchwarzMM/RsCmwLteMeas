Additional<Table>
----------------------------------------

.. rubric:: RepCap Settings

.. code-block:: python

	# Range: Nr1 .. Nr5
	rc = driver.configure.multiEval.limit.seMask.limit.additional.repcap_table_get()
	driver.configure.multiEval.limit.seMask.limit.additional.repcap_table_set(repcap.Table.Nr1)





.. autoclass:: RsCmwLteMeas.Implementations.Configure.MultiEval.Limit.SeMask.Limit.Additional.AdditionalCls
	:members:
	:undoc-members:
	:noindex:


.. rubric:: Cloning the Group

.. code-block:: python

	# Create a clone of the original group, that exists independently
	group2 = driver.configure.multiEval.limit.seMask.limit.additional.clone()



.. rubric:: Subgroups
.. toctree::
	:maxdepth: 6
	:glob:

	Configure_MultiEval_Limit_SeMask_Limit_Additional_CarrierAggregation.rst
	Configure_MultiEval_Limit_SeMask_Limit_Additional_ChannelBw.rst