Phase
----------------------------------------



.. rubric:: SCPI Commands :

.. index::
	single: READ:LTE:MEASurement<Instance>:MEValuation:TRACe:ESFLatness:PHASe
	single: FETCh:LTE:MEASurement<Instance>:MEValuation:TRACe:ESFLatness:PHASe

.. code-block:: python

	READ:LTE:MEASurement<Instance>:MEValuation:TRACe:ESFLatness:PHASe
	FETCh:LTE:MEASurement<Instance>:MEValuation:TRACe:ESFLatness:PHASe



.. autoclass:: RsCmwLteMeas.Implementations.MultiEval.Trace.EsFlatness.Phase.PhaseCls
	:members:
	:undoc-members:
	:noindex: