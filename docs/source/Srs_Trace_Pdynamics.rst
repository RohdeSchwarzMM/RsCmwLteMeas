Pdynamics
----------------------------------------





.. autoclass:: RsCmwLteMeas.Implementations.Srs.Trace.Pdynamics.PdynamicsCls
	:members:
	:undoc-members:
	:noindex:


.. rubric:: Cloning the Group

.. code-block:: python

	# Create a clone of the original group, that exists independently
	group2 = driver.srs.trace.pdynamics.clone()



.. rubric:: Subgroups
.. toctree::
	:maxdepth: 6
	:glob:

	Srs_Trace_Pdynamics_Average.rst
	Srs_Trace_Pdynamics_Current.rst
	Srs_Trace_Pdynamics_Maximum.rst