StandardDev
----------------------------------------



.. rubric:: SCPI Command :

.. index::
	single: FETCh:LTE:MEASurement<Instance>:MEValuation:LIST:MODulation:PSD:SDEViation

.. code-block:: python

	FETCh:LTE:MEASurement<Instance>:MEValuation:LIST:MODulation:PSD:SDEViation



.. autoclass:: RsCmwLteMeas.Implementations.MultiEval.ListPy.Modulation.Psd.StandardDev.StandardDevCls
	:members:
	:undoc-members:
	:noindex: