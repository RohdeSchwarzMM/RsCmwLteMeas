Merror
----------------------------------------



.. rubric:: SCPI Command :

.. index::
	single: FETCh:LTE:MEASurement<Instance>:MEValuation:REFMarker:MERRor

.. code-block:: python

	FETCh:LTE:MEASurement<Instance>:MEValuation:REFMarker:MERRor



.. autoclass:: RsCmwLteMeas.Implementations.MultiEval.ReferenceMarker.Merror.MerrorCls
	:members:
	:undoc-members:
	:noindex: