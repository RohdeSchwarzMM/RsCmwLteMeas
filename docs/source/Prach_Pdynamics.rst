Pdynamics
----------------------------------------





.. autoclass:: RsCmwLteMeas.Implementations.Prach.Pdynamics.PdynamicsCls
	:members:
	:undoc-members:
	:noindex:


.. rubric:: Cloning the Group

.. code-block:: python

	# Create a clone of the original group, that exists independently
	group2 = driver.prach.pdynamics.clone()



.. rubric:: Subgroups
.. toctree::
	:maxdepth: 6
	:glob:

	Prach_Pdynamics_Average.rst
	Prach_Pdynamics_Current.rst
	Prach_Pdynamics_Maximum.rst
	Prach_Pdynamics_Minimum.rst
	Prach_Pdynamics_StandardDev.rst