Maximum
----------------------------------------



.. rubric:: SCPI Commands :

.. index::
	single: READ:LTE:MEASurement<Instance>:PRACh:PDYNamics:MAXimum
	single: FETCh:LTE:MEASurement<Instance>:PRACh:PDYNamics:MAXimum
	single: CALCulate:LTE:MEASurement<Instance>:PRACh:PDYNamics:MAXimum

.. code-block:: python

	READ:LTE:MEASurement<Instance>:PRACh:PDYNamics:MAXimum
	FETCh:LTE:MEASurement<Instance>:PRACh:PDYNamics:MAXimum
	CALCulate:LTE:MEASurement<Instance>:PRACh:PDYNamics:MAXimum



.. autoclass:: RsCmwLteMeas.Implementations.Prach.Pdynamics.Maximum.MaximumCls
	:members:
	:undoc-members:
	:noindex: