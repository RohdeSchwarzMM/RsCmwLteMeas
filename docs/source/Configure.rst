Configure
----------------------------------------



.. rubric:: SCPI Commands :

.. index::
	single: CONFigure:LTE:MEASurement<Instance>:BAND
	single: CONFigure:LTE:MEASurement<Instance>:SPATh
	single: CONFigure:LTE:MEASurement<Instance>:STYPe
	single: CONFigure:LTE:MEASurement<Instance>:DMODe
	single: CONFigure:LTE:MEASurement<Instance>:FSTRucture

.. code-block:: python

	CONFigure:LTE:MEASurement<Instance>:BAND
	CONFigure:LTE:MEASurement<Instance>:SPATh
	CONFigure:LTE:MEASurement<Instance>:STYPe
	CONFigure:LTE:MEASurement<Instance>:DMODe
	CONFigure:LTE:MEASurement<Instance>:FSTRucture



.. autoclass:: RsCmwLteMeas.Implementations.Configure.ConfigureCls
	:members:
	:undoc-members:
	:noindex:


.. rubric:: Cloning the Group

.. code-block:: python

	# Create a clone of the original group, that exists independently
	group2 = driver.configure.clone()



.. rubric:: Subgroups
.. toctree::
	:maxdepth: 6
	:glob:

	Configure_CarrierAggregation.rst
	Configure_Cc.rst
	Configure_Emtc.rst
	Configure_MultiEval.rst
	Configure_Network.rst
	Configure_Pcc.rst
	Configure_Prach.rst
	Configure_RfSettings.rst
	Configure_Srs.rst