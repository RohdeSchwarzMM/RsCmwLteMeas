Extreme
----------------------------------------



.. rubric:: SCPI Commands :

.. index::
	single: FETCh:LTE:MEASurement<Instance>:MEValuation:LIST:MODulation:TERRor:EXTReme
	single: CALCulate:LTE:MEASurement<Instance>:MEValuation:LIST:MODulation:TERRor:EXTReme

.. code-block:: python

	FETCh:LTE:MEASurement<Instance>:MEValuation:LIST:MODulation:TERRor:EXTReme
	CALCulate:LTE:MEASurement<Instance>:MEValuation:LIST:MODulation:TERRor:EXTReme



.. autoclass:: RsCmwLteMeas.Implementations.MultiEval.ListPy.Modulation.Terror.Extreme.ExtremeCls
	:members:
	:undoc-members:
	:noindex: