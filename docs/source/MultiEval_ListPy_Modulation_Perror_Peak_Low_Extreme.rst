Extreme
----------------------------------------



.. rubric:: SCPI Commands :

.. index::
	single: FETCh:LTE:MEASurement<Instance>:MEValuation:LIST:MODulation:PERRor:PEAK:LOW:EXTReme
	single: CALCulate:LTE:MEASurement<Instance>:MEValuation:LIST:MODulation:PERRor:PEAK:LOW:EXTReme

.. code-block:: python

	FETCh:LTE:MEASurement<Instance>:MEValuation:LIST:MODulation:PERRor:PEAK:LOW:EXTReme
	CALCulate:LTE:MEASurement<Instance>:MEValuation:LIST:MODulation:PERRor:PEAK:LOW:EXTReme



.. autoclass:: RsCmwLteMeas.Implementations.MultiEval.ListPy.Modulation.Perror.Peak.Low.Extreme.ExtremeCls
	:members:
	:undoc-members:
	:noindex: