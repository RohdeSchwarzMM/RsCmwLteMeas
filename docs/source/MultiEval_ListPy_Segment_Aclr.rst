Aclr
----------------------------------------





.. autoclass:: RsCmwLteMeas.Implementations.MultiEval.ListPy.Segment.Aclr.AclrCls
	:members:
	:undoc-members:
	:noindex:


.. rubric:: Cloning the Group

.. code-block:: python

	# Create a clone of the original group, that exists independently
	group2 = driver.multiEval.listPy.segment.aclr.clone()



.. rubric:: Subgroups
.. toctree::
	:maxdepth: 6
	:glob:

	MultiEval_ListPy_Segment_Aclr_Average.rst
	MultiEval_ListPy_Segment_Aclr_Current.rst
	MultiEval_ListPy_Segment_Aclr_Dallocation.rst
	MultiEval_ListPy_Segment_Aclr_DchType.rst