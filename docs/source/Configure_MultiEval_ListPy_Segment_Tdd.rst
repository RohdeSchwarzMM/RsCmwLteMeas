Tdd
----------------------------------------



.. rubric:: SCPI Command :

.. index::
	single: CONFigure:LTE:MEASurement<Instance>:MEValuation:LIST:SEGMent<nr>:TDD

.. code-block:: python

	CONFigure:LTE:MEASurement<Instance>:MEValuation:LIST:SEGMent<nr>:TDD



.. autoclass:: RsCmwLteMeas.Implementations.Configure.MultiEval.ListPy.Segment.Tdd.TddCls
	:members:
	:undoc-members:
	:noindex: