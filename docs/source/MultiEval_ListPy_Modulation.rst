Modulation
----------------------------------------





.. autoclass:: RsCmwLteMeas.Implementations.MultiEval.ListPy.Modulation.ModulationCls
	:members:
	:undoc-members:
	:noindex:


.. rubric:: Cloning the Group

.. code-block:: python

	# Create a clone of the original group, that exists independently
	group2 = driver.multiEval.listPy.modulation.clone()



.. rubric:: Subgroups
.. toctree::
	:maxdepth: 6
	:glob:

	MultiEval_ListPy_Modulation_Dallocation.rst
	MultiEval_ListPy_Modulation_DchType.rst
	MultiEval_ListPy_Modulation_Dmodulation.rst
	MultiEval_ListPy_Modulation_Evm.rst
	MultiEval_ListPy_Modulation_FreqError.rst
	MultiEval_ListPy_Modulation_IqOffset.rst
	MultiEval_ListPy_Modulation_Merror.rst
	MultiEval_ListPy_Modulation_Perror.rst
	MultiEval_ListPy_Modulation_Ppower.rst
	MultiEval_ListPy_Modulation_Psd.rst
	MultiEval_ListPy_Modulation_SchType.rst
	MultiEval_ListPy_Modulation_Terror.rst
	MultiEval_ListPy_Modulation_Tpower.rst