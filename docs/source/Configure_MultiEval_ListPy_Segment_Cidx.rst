Cidx
----------------------------------------



.. rubric:: SCPI Command :

.. index::
	single: CONFigure:LTE:MEASurement<Instance>:MEValuation:LIST:SEGMent<nr>:CIDX

.. code-block:: python

	CONFigure:LTE:MEASurement<Instance>:MEValuation:LIST:SEGMent<nr>:CIDX



.. autoclass:: RsCmwLteMeas.Implementations.Configure.MultiEval.ListPy.Segment.Cidx.CidxCls
	:members:
	:undoc-members:
	:noindex: