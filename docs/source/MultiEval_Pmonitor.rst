Pmonitor
----------------------------------------





.. autoclass:: RsCmwLteMeas.Implementations.MultiEval.Pmonitor.PmonitorCls
	:members:
	:undoc-members:
	:noindex:


.. rubric:: Cloning the Group

.. code-block:: python

	# Create a clone of the original group, that exists independently
	group2 = driver.multiEval.pmonitor.clone()



.. rubric:: Subgroups
.. toctree::
	:maxdepth: 6
	:glob:

	MultiEval_Pmonitor_Average.rst
	MultiEval_Pmonitor_Cc.rst
	MultiEval_Pmonitor_Current.rst
	MultiEval_Pmonitor_Maximum.rst
	MultiEval_Pmonitor_Minimum.rst
	MultiEval_Pmonitor_StandardDev.rst