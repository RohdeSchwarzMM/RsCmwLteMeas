Iq
----------------------------------------





.. autoclass:: RsCmwLteMeas.Implementations.MultiEval.Trace.Iq.IqCls
	:members:
	:undoc-members:
	:noindex:


.. rubric:: Cloning the Group

.. code-block:: python

	# Create a clone of the original group, that exists independently
	group2 = driver.multiEval.trace.iq.clone()



.. rubric:: Subgroups
.. toctree::
	:maxdepth: 6
	:glob:

	MultiEval_Trace_Iq_High.rst
	MultiEval_Trace_Iq_Low.rst