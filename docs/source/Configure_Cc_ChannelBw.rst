ChannelBw
----------------------------------------



.. rubric:: SCPI Command :

.. index::
	single: CONFigure:LTE:MEASurement<Instance>:CC<Nr>:CBANdwidth

.. code-block:: python

	CONFigure:LTE:MEASurement<Instance>:CC<Nr>:CBANdwidth



.. autoclass:: RsCmwLteMeas.Implementations.Configure.Cc.ChannelBw.ChannelBwCls
	:members:
	:undoc-members:
	:noindex: