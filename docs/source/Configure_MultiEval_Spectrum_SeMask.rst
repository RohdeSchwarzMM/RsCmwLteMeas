SeMask
----------------------------------------



.. rubric:: SCPI Command :

.. index::
	single: CONFigure:LTE:MEASurement<Instance>:MEValuation:SPECtrum:SEMask:MFILter

.. code-block:: python

	CONFigure:LTE:MEASurement<Instance>:MEValuation:SPECtrum:SEMask:MFILter



.. autoclass:: RsCmwLteMeas.Implementations.Configure.MultiEval.Spectrum.SeMask.SeMaskCls
	:members:
	:undoc-members:
	:noindex: