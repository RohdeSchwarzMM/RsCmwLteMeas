Peak
----------------------------------------





.. autoclass:: RsCmwLteMeas.Implementations.MultiEval.Evmc.Peak.PeakCls
	:members:
	:undoc-members:
	:noindex:


.. rubric:: Cloning the Group

.. code-block:: python

	# Create a clone of the original group, that exists independently
	group2 = driver.multiEval.evmc.peak.clone()



.. rubric:: Subgroups
.. toctree::
	:maxdepth: 6
	:glob:

	MultiEval_Evmc_Peak_Average.rst
	MultiEval_Evmc_Peak_Current.rst
	MultiEval_Evmc_Peak_Maximum.rst
	MultiEval_Evmc_Peak_StandardDev.rst