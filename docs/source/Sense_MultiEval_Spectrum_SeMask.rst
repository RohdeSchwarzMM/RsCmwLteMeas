SeMask
----------------------------------------





.. autoclass:: RsCmwLteMeas.Implementations.Sense.MultiEval.Spectrum.SeMask.SeMaskCls
	:members:
	:undoc-members:
	:noindex:


.. rubric:: Cloning the Group

.. code-block:: python

	# Create a clone of the original group, that exists independently
	group2 = driver.sense.multiEval.spectrum.seMask.clone()



.. rubric:: Subgroups
.. toctree::
	:maxdepth: 6
	:glob:

	Sense_MultiEval_Spectrum_SeMask_Rbw.rst