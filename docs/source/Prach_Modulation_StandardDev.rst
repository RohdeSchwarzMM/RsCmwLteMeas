StandardDev
----------------------------------------



.. rubric:: SCPI Commands :

.. index::
	single: READ:LTE:MEASurement<Instance>:PRACh:MODulation:SDEViation
	single: FETCh:LTE:MEASurement<Instance>:PRACh:MODulation:SDEViation

.. code-block:: python

	READ:LTE:MEASurement<Instance>:PRACh:MODulation:SDEViation
	FETCh:LTE:MEASurement<Instance>:PRACh:MODulation:SDEViation



.. autoclass:: RsCmwLteMeas.Implementations.Prach.Modulation.StandardDev.StandardDevCls
	:members:
	:undoc-members:
	:noindex: