CarrierAggregation
----------------------------------------





.. autoclass:: RsCmwLteMeas.Implementations.Configure.CarrierAggregation.CarrierAggregationCls
	:members:
	:undoc-members:
	:noindex:


.. rubric:: Cloning the Group

.. code-block:: python

	# Create a clone of the original group, that exists independently
	group2 = driver.configure.carrierAggregation.clone()



.. rubric:: Subgroups
.. toctree::
	:maxdepth: 6
	:glob:

	Configure_CarrierAggregation_ChannelBw.rst
	Configure_CarrierAggregation_Frequency.rst
	Configure_CarrierAggregation_Maping.rst
	Configure_CarrierAggregation_Mcarrier.rst
	Configure_CarrierAggregation_Mode.rst
	Configure_CarrierAggregation_Scc.rst