Emtc
----------------------------------------





.. autoclass:: RsCmwLteMeas.Implementations.Configure.MultiEval.ListPy.Segment.Emtc.EmtcCls
	:members:
	:undoc-members:
	:noindex:


.. rubric:: Cloning the Group

.. code-block:: python

	# Create a clone of the original group, that exists independently
	group2 = driver.configure.multiEval.listPy.segment.emtc.clone()



.. rubric:: Subgroups
.. toctree::
	:maxdepth: 6
	:glob:

	Configure_MultiEval_ListPy_Segment_Emtc_Nband.rst