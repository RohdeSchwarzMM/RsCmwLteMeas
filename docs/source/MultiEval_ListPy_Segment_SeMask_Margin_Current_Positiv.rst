Positiv
----------------------------------------



.. rubric:: SCPI Command :

.. index::
	single: FETCh:LTE:MEASurement<Instance>:MEValuation:LIST:SEGMent<nr>:SEMask:MARGin:CURRent:POSitiv

.. code-block:: python

	FETCh:LTE:MEASurement<Instance>:MEValuation:LIST:SEGMent<nr>:SEMask:MARGin:CURRent:POSitiv



.. autoclass:: RsCmwLteMeas.Implementations.MultiEval.ListPy.Segment.SeMask.Margin.Current.Positiv.PositivCls
	:members:
	:undoc-members:
	:noindex: