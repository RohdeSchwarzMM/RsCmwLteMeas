Extreme
----------------------------------------



.. rubric:: SCPI Commands :

.. index::
	single: READ:LTE:MEASurement<Instance>:MEValuation:SEMask:EXTReme
	single: FETCh:LTE:MEASurement<Instance>:MEValuation:SEMask:EXTReme
	single: CALCulate:LTE:MEASurement<Instance>:MEValuation:SEMask:EXTReme

.. code-block:: python

	READ:LTE:MEASurement<Instance>:MEValuation:SEMask:EXTReme
	FETCh:LTE:MEASurement<Instance>:MEValuation:SEMask:EXTReme
	CALCulate:LTE:MEASurement<Instance>:MEValuation:SEMask:EXTReme



.. autoclass:: RsCmwLteMeas.Implementations.MultiEval.SeMask.Extreme.ExtremeCls
	:members:
	:undoc-members:
	:noindex: