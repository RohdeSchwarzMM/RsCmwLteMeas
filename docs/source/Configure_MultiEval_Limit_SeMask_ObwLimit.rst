ObwLimit
----------------------------------------





.. autoclass:: RsCmwLteMeas.Implementations.Configure.MultiEval.Limit.SeMask.ObwLimit.ObwLimitCls
	:members:
	:undoc-members:
	:noindex:


.. rubric:: Cloning the Group

.. code-block:: python

	# Create a clone of the original group, that exists independently
	group2 = driver.configure.multiEval.limit.seMask.obwLimit.clone()



.. rubric:: Subgroups
.. toctree::
	:maxdepth: 6
	:glob:

	Configure_MultiEval_Limit_SeMask_ObwLimit_CarrierAggregation.rst
	Configure_MultiEval_Limit_SeMask_ObwLimit_ChannelBw.rst