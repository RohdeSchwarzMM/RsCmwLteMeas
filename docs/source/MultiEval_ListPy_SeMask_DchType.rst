DchType
----------------------------------------



.. rubric:: SCPI Command :

.. index::
	single: FETCh:LTE:MEASurement<Instance>:MEValuation:LIST:SEMask:DCHType

.. code-block:: python

	FETCh:LTE:MEASurement<Instance>:MEValuation:LIST:SEMask:DCHType



.. autoclass:: RsCmwLteMeas.Implementations.MultiEval.ListPy.SeMask.DchType.DchTypeCls
	:members:
	:undoc-members:
	:noindex: