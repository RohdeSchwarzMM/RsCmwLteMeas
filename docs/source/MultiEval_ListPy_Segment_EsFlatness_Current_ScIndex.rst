ScIndex
----------------------------------------



.. rubric:: SCPI Command :

.. index::
	single: FETCh:LTE:MEASurement<Instance>:MEValuation:LIST:SEGMent<nr>:ESFLatness:CURRent:SCINdex

.. code-block:: python

	FETCh:LTE:MEASurement<Instance>:MEValuation:LIST:SEGMent<nr>:ESFLatness:CURRent:SCINdex



.. autoclass:: RsCmwLteMeas.Implementations.MultiEval.ListPy.Segment.EsFlatness.Current.ScIndex.ScIndexCls
	:members:
	:undoc-members:
	:noindex: