Qpsk
----------------------------------------



.. rubric:: SCPI Commands :

.. index::
	single: CONFigure:LTE:MEASurement<Instance>:MEValuation:LIMit:QPSK:FERRor
	single: CONFigure:LTE:MEASurement<Instance>:MEValuation:LIMit:QPSK:SFLatness
	single: CONFigure:LTE:MEASurement<Instance>:MEValuation:LIMit:QPSK:ESFLatness

.. code-block:: python

	CONFigure:LTE:MEASurement<Instance>:MEValuation:LIMit:QPSK:FERRor
	CONFigure:LTE:MEASurement<Instance>:MEValuation:LIMit:QPSK:SFLatness
	CONFigure:LTE:MEASurement<Instance>:MEValuation:LIMit:QPSK:ESFLatness



.. autoclass:: RsCmwLteMeas.Implementations.Configure.MultiEval.Limit.Qpsk.QpskCls
	:members:
	:undoc-members:
	:noindex:


.. rubric:: Cloning the Group

.. code-block:: python

	# Create a clone of the original group, that exists independently
	group2 = driver.configure.multiEval.limit.qpsk.clone()



.. rubric:: Subgroups
.. toctree::
	:maxdepth: 6
	:glob:

	Configure_MultiEval_Limit_Qpsk_EvMagnitude.rst
	Configure_MultiEval_Limit_Qpsk_Ibe.rst
	Configure_MultiEval_Limit_Qpsk_IqOffset.rst
	Configure_MultiEval_Limit_Qpsk_Merror.rst
	Configure_MultiEval_Limit_Qpsk_Perror.rst