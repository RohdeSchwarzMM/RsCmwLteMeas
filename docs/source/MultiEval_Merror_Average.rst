Average
----------------------------------------



.. rubric:: SCPI Commands :

.. index::
	single: READ:LTE:MEASurement<Instance>:MEValuation:MERRor:AVERage
	single: FETCh:LTE:MEASurement<Instance>:MEValuation:MERRor:AVERage

.. code-block:: python

	READ:LTE:MEASurement<Instance>:MEValuation:MERRor:AVERage
	FETCh:LTE:MEASurement<Instance>:MEValuation:MERRor:AVERage



.. autoclass:: RsCmwLteMeas.Implementations.MultiEval.Merror.Average.AverageCls
	:members:
	:undoc-members:
	:noindex: