Evmc
----------------------------------------





.. autoclass:: RsCmwLteMeas.Implementations.MultiEval.Evmc.EvmcCls
	:members:
	:undoc-members:
	:noindex:


.. rubric:: Cloning the Group

.. code-block:: python

	# Create a clone of the original group, that exists independently
	group2 = driver.multiEval.evmc.clone()



.. rubric:: Subgroups
.. toctree::
	:maxdepth: 6
	:glob:

	MultiEval_Evmc_Peak.rst