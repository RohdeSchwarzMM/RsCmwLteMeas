Current
----------------------------------------



.. rubric:: SCPI Commands :

.. index::
	single: READ:LTE:MEASurement<Instance>:PRACh:EVMSymbol:CURRent
	single: FETCh:LTE:MEASurement<Instance>:PRACh:EVMSymbol:CURRent

.. code-block:: python

	READ:LTE:MEASurement<Instance>:PRACh:EVMSymbol:CURRent
	FETCh:LTE:MEASurement<Instance>:PRACh:EVMSymbol:CURRent



.. autoclass:: RsCmwLteMeas.Implementations.Prach.EvmSymbol.Current.CurrentCls
	:members:
	:undoc-members:
	:noindex: