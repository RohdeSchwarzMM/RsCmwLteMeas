Enhanced
----------------------------------------



.. rubric:: SCPI Command :

.. index::
	single: CONFigure:LTE:MEASurement<Instance>:MEValuation:LIST:SEGMent<nr>:CAGGregation:MCARrier:ENHanced

.. code-block:: python

	CONFigure:LTE:MEASurement<Instance>:MEValuation:LIST:SEGMent<nr>:CAGGregation:MCARrier:ENHanced



.. autoclass:: RsCmwLteMeas.Implementations.Configure.MultiEval.ListPy.Segment.CarrierAggregation.Mcarrier.Enhanced.EnhancedCls
	:members:
	:undoc-members:
	:noindex: