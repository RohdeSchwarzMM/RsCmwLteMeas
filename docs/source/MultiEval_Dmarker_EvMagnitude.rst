EvMagnitude
----------------------------------------



.. rubric:: SCPI Command :

.. index::
	single: FETCh:LTE:MEASurement<Instance>:MEValuation:DMARker<No>:EVMagnitude

.. code-block:: python

	FETCh:LTE:MEASurement<Instance>:MEValuation:DMARker<No>:EVMagnitude



.. autoclass:: RsCmwLteMeas.Implementations.MultiEval.Dmarker.EvMagnitude.EvMagnitudeCls
	:members:
	:undoc-members:
	:noindex:


.. rubric:: Cloning the Group

.. code-block:: python

	# Create a clone of the original group, that exists independently
	group2 = driver.multiEval.dmarker.evMagnitude.clone()



.. rubric:: Subgroups
.. toctree::
	:maxdepth: 6
	:glob:

	MultiEval_Dmarker_EvMagnitude_Peak.rst