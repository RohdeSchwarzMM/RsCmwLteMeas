Frequency
----------------------------------------



.. rubric:: SCPI Command :

.. index::
	single: CONFigure:LTE:MEASurement<Instance>:RFSettings:CC<Nr>:FREQuency

.. code-block:: python

	CONFigure:LTE:MEASurement<Instance>:RFSettings:CC<Nr>:FREQuency



.. autoclass:: RsCmwLteMeas.Implementations.Configure.RfSettings.Cc.Frequency.FrequencyCls
	:members:
	:undoc-members:
	:noindex: