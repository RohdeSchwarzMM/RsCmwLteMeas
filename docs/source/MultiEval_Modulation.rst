Modulation
----------------------------------------





.. autoclass:: RsCmwLteMeas.Implementations.MultiEval.Modulation.ModulationCls
	:members:
	:undoc-members:
	:noindex:


.. rubric:: Cloning the Group

.. code-block:: python

	# Create a clone of the original group, that exists independently
	group2 = driver.multiEval.modulation.clone()



.. rubric:: Subgroups
.. toctree::
	:maxdepth: 6
	:glob:

	MultiEval_Modulation_Average.rst
	MultiEval_Modulation_Current.rst
	MultiEval_Modulation_Dallocation.rst
	MultiEval_Modulation_DchType.rst
	MultiEval_Modulation_Dmodulation.rst
	MultiEval_Modulation_Extreme.rst
	MultiEval_Modulation_SchType.rst
	MultiEval_Modulation_StandardDev.rst