Maping
----------------------------------------



.. rubric:: SCPI Commands :

.. index::
	single: CONFigure:LTE:MEASurement<Instance>:CAGGregation:MAPing:PCC
	single: CONFigure:LTE:MEASurement<Instance>:CAGGregation:MAPing

.. code-block:: python

	CONFigure:LTE:MEASurement<Instance>:CAGGregation:MAPing:PCC
	CONFigure:LTE:MEASurement<Instance>:CAGGregation:MAPing



.. autoclass:: RsCmwLteMeas.Implementations.Configure.CarrierAggregation.Maping.MapingCls
	:members:
	:undoc-members:
	:noindex:


.. rubric:: Cloning the Group

.. code-block:: python

	# Create a clone of the original group, that exists independently
	group2 = driver.configure.carrierAggregation.maping.clone()



.. rubric:: Subgroups
.. toctree::
	:maxdepth: 6
	:glob:

	Configure_CarrierAggregation_Maping_Scc.rst