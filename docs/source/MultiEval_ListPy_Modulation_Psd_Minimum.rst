Minimum
----------------------------------------



.. rubric:: SCPI Commands :

.. index::
	single: FETCh:LTE:MEASurement<Instance>:MEValuation:LIST:MODulation:PSD:MINimum
	single: CALCulate:LTE:MEASurement<Instance>:MEValuation:LIST:MODulation:PSD:MINimum

.. code-block:: python

	FETCh:LTE:MEASurement<Instance>:MEValuation:LIST:MODulation:PSD:MINimum
	CALCulate:LTE:MEASurement<Instance>:MEValuation:LIST:MODulation:PSD:MINimum



.. autoclass:: RsCmwLteMeas.Implementations.MultiEval.ListPy.Modulation.Psd.Minimum.MinimumCls
	:members:
	:undoc-members:
	:noindex: