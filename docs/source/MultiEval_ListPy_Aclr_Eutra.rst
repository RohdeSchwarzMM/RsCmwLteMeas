Eutra
----------------------------------------





.. autoclass:: RsCmwLteMeas.Implementations.MultiEval.ListPy.Aclr.Eutra.EutraCls
	:members:
	:undoc-members:
	:noindex:


.. rubric:: Cloning the Group

.. code-block:: python

	# Create a clone of the original group, that exists independently
	group2 = driver.multiEval.listPy.aclr.eutra.clone()



.. rubric:: Subgroups
.. toctree::
	:maxdepth: 6
	:glob:

	MultiEval_ListPy_Aclr_Eutra_Average.rst
	MultiEval_ListPy_Aclr_Eutra_Current.rst
	MultiEval_ListPy_Aclr_Eutra_Negativ.rst
	MultiEval_ListPy_Aclr_Eutra_Positiv.rst