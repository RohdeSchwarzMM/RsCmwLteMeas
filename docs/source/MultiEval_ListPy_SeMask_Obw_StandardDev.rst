StandardDev
----------------------------------------



.. rubric:: SCPI Command :

.. index::
	single: FETCh:LTE:MEASurement<Instance>:MEValuation:LIST:SEMask:OBW:SDEViation

.. code-block:: python

	FETCh:LTE:MEASurement<Instance>:MEValuation:LIST:SEMask:OBW:SDEViation



.. autoclass:: RsCmwLteMeas.Implementations.MultiEval.ListPy.SeMask.Obw.StandardDev.StandardDevCls
	:members:
	:undoc-members:
	:noindex: