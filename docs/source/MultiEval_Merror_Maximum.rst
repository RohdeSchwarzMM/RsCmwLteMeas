Maximum
----------------------------------------



.. rubric:: SCPI Commands :

.. index::
	single: READ:LTE:MEASurement<Instance>:MEValuation:MERRor:MAXimum
	single: FETCh:LTE:MEASurement<Instance>:MEValuation:MERRor:MAXimum

.. code-block:: python

	READ:LTE:MEASurement<Instance>:MEValuation:MERRor:MAXimum
	FETCh:LTE:MEASurement<Instance>:MEValuation:MERRor:MAXimum



.. autoclass:: RsCmwLteMeas.Implementations.MultiEval.Merror.Maximum.MaximumCls
	:members:
	:undoc-members:
	:noindex: