Average
----------------------------------------



.. rubric:: SCPI Commands :

.. index::
	single: READ:LTE:MEASurement<Instance>:MEValuation:TRACe:PDYNamics:AVERage
	single: FETCh:LTE:MEASurement<Instance>:MEValuation:TRACe:PDYNamics:AVERage

.. code-block:: python

	READ:LTE:MEASurement<Instance>:MEValuation:TRACe:PDYNamics:AVERage
	FETCh:LTE:MEASurement<Instance>:MEValuation:TRACe:PDYNamics:AVERage



.. autoclass:: RsCmwLteMeas.Implementations.MultiEval.Trace.Pdynamics.Average.AverageCls
	:members:
	:undoc-members:
	:noindex: