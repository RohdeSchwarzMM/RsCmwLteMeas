Merror
----------------------------------------



.. rubric:: SCPI Command :

.. index::
	single: CONFigure:LTE:MEASurement<Instance>:PRACh:LIMit:MERRor

.. code-block:: python

	CONFigure:LTE:MEASurement<Instance>:PRACh:LIMit:MERRor



.. autoclass:: RsCmwLteMeas.Implementations.Configure.Prach.Limit.Merror.MerrorCls
	:members:
	:undoc-members:
	:noindex: