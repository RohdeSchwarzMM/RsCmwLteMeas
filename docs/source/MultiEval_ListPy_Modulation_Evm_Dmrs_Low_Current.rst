Current
----------------------------------------



.. rubric:: SCPI Commands :

.. index::
	single: FETCh:LTE:MEASurement<Instance>:MEValuation:LIST:MODulation:EVM:DMRS:LOW:CURRent
	single: CALCulate:LTE:MEASurement<Instance>:MEValuation:LIST:MODulation:EVM:DMRS:LOW:CURRent

.. code-block:: python

	FETCh:LTE:MEASurement<Instance>:MEValuation:LIST:MODulation:EVM:DMRS:LOW:CURRent
	CALCulate:LTE:MEASurement<Instance>:MEValuation:LIST:MODulation:EVM:DMRS:LOW:CURRent



.. autoclass:: RsCmwLteMeas.Implementations.MultiEval.ListPy.Modulation.Evm.Dmrs.Low.Current.CurrentCls
	:members:
	:undoc-members:
	:noindex: