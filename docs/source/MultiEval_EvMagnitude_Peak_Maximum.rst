Maximum
----------------------------------------



.. rubric:: SCPI Commands :

.. index::
	single: READ:LTE:MEASurement<Instance>:MEValuation:EVMagnitude:PEAK:MAXimum
	single: FETCh:LTE:MEASurement<Instance>:MEValuation:EVMagnitude:PEAK:MAXimum

.. code-block:: python

	READ:LTE:MEASurement<Instance>:MEValuation:EVMagnitude:PEAK:MAXimum
	FETCh:LTE:MEASurement<Instance>:MEValuation:EVMagnitude:PEAK:MAXimum



.. autoclass:: RsCmwLteMeas.Implementations.MultiEval.EvMagnitude.Peak.Maximum.MaximumCls
	:members:
	:undoc-members:
	:noindex: