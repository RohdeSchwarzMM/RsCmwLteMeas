Current
----------------------------------------



.. rubric:: SCPI Command :

.. index::
	single: FETCh:LTE:MEASurement<Instance>:MEValuation:LIST:SEMask:MARGin:AREA<nr>:NEGativ:CURRent

.. code-block:: python

	FETCh:LTE:MEASurement<Instance>:MEValuation:LIST:SEMask:MARGin:AREA<nr>:NEGativ:CURRent



.. autoclass:: RsCmwLteMeas.Implementations.MultiEval.ListPy.SeMask.Margin.Area.Negativ.Current.CurrentCls
	:members:
	:undoc-members:
	:noindex: