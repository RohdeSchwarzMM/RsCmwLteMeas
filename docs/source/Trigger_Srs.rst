Srs
----------------------------------------



.. rubric:: SCPI Commands :

.. index::
	single: TRIGger:LTE:MEASurement<Instance>:SRS:SOURce
	single: TRIGger:LTE:MEASurement<Instance>:SRS:THReshold
	single: TRIGger:LTE:MEASurement<Instance>:SRS:SLOPe
	single: TRIGger:LTE:MEASurement<Instance>:SRS:TOUT
	single: TRIGger:LTE:MEASurement<Instance>:SRS:MGAP

.. code-block:: python

	TRIGger:LTE:MEASurement<Instance>:SRS:SOURce
	TRIGger:LTE:MEASurement<Instance>:SRS:THReshold
	TRIGger:LTE:MEASurement<Instance>:SRS:SLOPe
	TRIGger:LTE:MEASurement<Instance>:SRS:TOUT
	TRIGger:LTE:MEASurement<Instance>:SRS:MGAP



.. autoclass:: RsCmwLteMeas.Implementations.Trigger.Srs.SrsCls
	:members:
	:undoc-members:
	:noindex:


.. rubric:: Cloning the Group

.. code-block:: python

	# Create a clone of the original group, that exists independently
	group2 = driver.trigger.srs.clone()



.. rubric:: Subgroups
.. toctree::
	:maxdepth: 6
	:glob:

	Trigger_Srs_Catalog.rst