Power
----------------------------------------





.. autoclass:: RsCmwLteMeas.Implementations.MultiEval.ListPy.Segment.Power.PowerCls
	:members:
	:undoc-members:
	:noindex:


.. rubric:: Cloning the Group

.. code-block:: python

	# Create a clone of the original group, that exists independently
	group2 = driver.multiEval.listPy.segment.power.clone()



.. rubric:: Subgroups
.. toctree::
	:maxdepth: 6
	:glob:

	MultiEval_ListPy_Segment_Power_Average.rst
	MultiEval_ListPy_Segment_Power_Cc.rst
	MultiEval_ListPy_Segment_Power_Current.rst
	MultiEval_ListPy_Segment_Power_Maximum.rst
	MultiEval_ListPy_Segment_Power_Minimum.rst
	MultiEval_ListPy_Segment_Power_StandardDev.rst