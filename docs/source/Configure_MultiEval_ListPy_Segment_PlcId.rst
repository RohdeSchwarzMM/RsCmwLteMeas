PlcId
----------------------------------------



.. rubric:: SCPI Command :

.. index::
	single: CONFigure:LTE:MEASurement<Instance>:MEValuation:LIST:SEGMent<nr>:PLCid

.. code-block:: python

	CONFigure:LTE:MEASurement<Instance>:MEValuation:LIST:SEGMent<nr>:PLCid



.. autoclass:: RsCmwLteMeas.Implementations.Configure.MultiEval.ListPy.Segment.PlcId.PlcIdCls
	:members:
	:undoc-members:
	:noindex: