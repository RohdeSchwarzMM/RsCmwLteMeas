Pcc
----------------------------------------



.. rubric:: SCPI Command :

.. index::
	single: CONFigure:LTE:MEASurement<Instance>[:PCC]:CBANdwidth

.. code-block:: python

	CONFigure:LTE:MEASurement<Instance>[:PCC]:CBANdwidth



.. autoclass:: RsCmwLteMeas.Implementations.Configure.Pcc.PccCls
	:members:
	:undoc-members:
	:noindex: