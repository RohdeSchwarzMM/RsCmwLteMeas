Cc<CarrierComponent>
----------------------------------------

.. rubric:: RepCap Settings

.. code-block:: python

	# Range: Nr1 .. Nr4
	rc = driver.multiEval.referenceMarker.pmonitor.cc.repcap_carrierComponent_get()
	driver.multiEval.referenceMarker.pmonitor.cc.repcap_carrierComponent_set(repcap.CarrierComponent.Nr1)



.. rubric:: SCPI Command :

.. index::
	single: FETCh:LTE:MEASurement<Instance>:MEValuation:REFMarker:PMONitor:CC<Nr>

.. code-block:: python

	FETCh:LTE:MEASurement<Instance>:MEValuation:REFMarker:PMONitor:CC<Nr>



.. autoclass:: RsCmwLteMeas.Implementations.MultiEval.ReferenceMarker.Pmonitor.Cc.CcCls
	:members:
	:undoc-members:
	:noindex:


.. rubric:: Cloning the Group

.. code-block:: python

	# Create a clone of the original group, that exists independently
	group2 = driver.multiEval.referenceMarker.pmonitor.cc.clone()