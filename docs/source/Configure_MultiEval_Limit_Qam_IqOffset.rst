IqOffset
----------------------------------------



.. rubric:: SCPI Command :

.. index::
	single: CONFigure:LTE:MEASurement<Instance>:MEValuation:LIMit:QAM<ModOrder>:IQOFfset

.. code-block:: python

	CONFigure:LTE:MEASurement<Instance>:MEValuation:LIMit:QAM<ModOrder>:IQOFfset



.. autoclass:: RsCmwLteMeas.Implementations.Configure.MultiEval.Limit.Qam.IqOffset.IqOffsetCls
	:members:
	:undoc-members:
	:noindex: