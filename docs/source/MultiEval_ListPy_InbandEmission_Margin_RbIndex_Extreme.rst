Extreme
----------------------------------------



.. rubric:: SCPI Command :

.. index::
	single: FETCh:LTE:MEASurement<Instance>:MEValuation:LIST:IEMission:MARGin:RBINdex:EXTReme

.. code-block:: python

	FETCh:LTE:MEASurement<Instance>:MEValuation:LIST:IEMission:MARGin:RBINdex:EXTReme



.. autoclass:: RsCmwLteMeas.Implementations.MultiEval.ListPy.InbandEmission.Margin.RbIndex.Extreme.ExtremeCls
	:members:
	:undoc-members:
	:noindex: