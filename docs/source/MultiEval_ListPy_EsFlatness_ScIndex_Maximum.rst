Maximum<MaxRange>
----------------------------------------

.. rubric:: RepCap Settings

.. code-block:: python

	# Range: Nr1 .. Nr2
	rc = driver.multiEval.listPy.esFlatness.scIndex.maximum.repcap_maxRange_get()
	driver.multiEval.listPy.esFlatness.scIndex.maximum.repcap_maxRange_set(repcap.MaxRange.Nr1)





.. autoclass:: RsCmwLteMeas.Implementations.MultiEval.ListPy.EsFlatness.ScIndex.Maximum.MaximumCls
	:members:
	:undoc-members:
	:noindex:


.. rubric:: Cloning the Group

.. code-block:: python

	# Create a clone of the original group, that exists independently
	group2 = driver.multiEval.listPy.esFlatness.scIndex.maximum.clone()



.. rubric:: Subgroups
.. toctree::
	:maxdepth: 6
	:glob:

	MultiEval_ListPy_EsFlatness_ScIndex_Maximum_Current.rst