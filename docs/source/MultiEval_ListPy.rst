ListPy
----------------------------------------





.. autoclass:: RsCmwLteMeas.Implementations.MultiEval.ListPy.ListPyCls
	:members:
	:undoc-members:
	:noindex:


.. rubric:: Cloning the Group

.. code-block:: python

	# Create a clone of the original group, that exists independently
	group2 = driver.multiEval.listPy.clone()



.. rubric:: Subgroups
.. toctree::
	:maxdepth: 6
	:glob:

	MultiEval_ListPy_Aclr.rst
	MultiEval_ListPy_EsFlatness.rst
	MultiEval_ListPy_InbandEmission.rst
	MultiEval_ListPy_Modulation.rst
	MultiEval_ListPy_Pmonitor.rst
	MultiEval_ListPy_Power.rst
	MultiEval_ListPy_Segment.rst
	MultiEval_ListPy_SeMask.rst
	MultiEval_ListPy_Sreliability.rst