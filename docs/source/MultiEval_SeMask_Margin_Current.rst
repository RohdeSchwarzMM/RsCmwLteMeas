Current
----------------------------------------





.. autoclass:: RsCmwLteMeas.Implementations.MultiEval.SeMask.Margin.Current.CurrentCls
	:members:
	:undoc-members:
	:noindex:


.. rubric:: Cloning the Group

.. code-block:: python

	# Create a clone of the original group, that exists independently
	group2 = driver.multiEval.seMask.margin.current.clone()



.. rubric:: Subgroups
.. toctree::
	:maxdepth: 6
	:glob:

	MultiEval_SeMask_Margin_Current_Negativ.rst
	MultiEval_SeMask_Margin_Current_Positiv.rst