SingleCmw
----------------------------------------



.. rubric:: SCPI Command :

.. index::
	single: CONFigure:LTE:MEASurement<Instance>:MEValuation:LIST:CMWS:CMODe

.. code-block:: python

	CONFigure:LTE:MEASurement<Instance>:MEValuation:LIST:CMWS:CMODe



.. autoclass:: RsCmwLteMeas.Implementations.Configure.MultiEval.ListPy.SingleCmw.SingleCmwCls
	:members:
	:undoc-members:
	:noindex: