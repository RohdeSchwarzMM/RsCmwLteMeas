DchType
----------------------------------------



.. rubric:: SCPI Command :

.. index::
	single: FETCh:LTE:MEASurement<Instance>:MEValuation:SEMask:DCHType

.. code-block:: python

	FETCh:LTE:MEASurement<Instance>:MEValuation:SEMask:DCHType



.. autoclass:: RsCmwLteMeas.Implementations.MultiEval.SeMask.DchType.DchTypeCls
	:members:
	:undoc-members:
	:noindex: