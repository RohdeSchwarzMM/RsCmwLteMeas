Difference<Difference>
----------------------------------------

.. rubric:: RepCap Settings

.. code-block:: python

	# Range: Nr1 .. Nr2
	rc = driver.multiEval.listPy.esFlatness.difference.repcap_difference_get()
	driver.multiEval.listPy.esFlatness.difference.repcap_difference_set(repcap.Difference.Nr1)





.. autoclass:: RsCmwLteMeas.Implementations.MultiEval.ListPy.EsFlatness.Difference.DifferenceCls
	:members:
	:undoc-members:
	:noindex:


.. rubric:: Cloning the Group

.. code-block:: python

	# Create a clone of the original group, that exists independently
	group2 = driver.multiEval.listPy.esFlatness.difference.clone()



.. rubric:: Subgroups
.. toctree::
	:maxdepth: 6
	:glob:

	MultiEval_ListPy_EsFlatness_Difference_Average.rst
	MultiEval_ListPy_EsFlatness_Difference_Current.rst
	MultiEval_ListPy_EsFlatness_Difference_Extreme.rst
	MultiEval_ListPy_EsFlatness_Difference_StandardDev.rst