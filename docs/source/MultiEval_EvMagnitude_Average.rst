Average
----------------------------------------



.. rubric:: SCPI Commands :

.. index::
	single: READ:LTE:MEASurement<Instance>:MEValuation:EVMagnitude:AVERage
	single: FETCh:LTE:MEASurement<Instance>:MEValuation:EVMagnitude:AVERage

.. code-block:: python

	READ:LTE:MEASurement<Instance>:MEValuation:EVMagnitude:AVERage
	FETCh:LTE:MEASurement<Instance>:MEValuation:EVMagnitude:AVERage



.. autoclass:: RsCmwLteMeas.Implementations.MultiEval.EvMagnitude.Average.AverageCls
	:members:
	:undoc-members:
	:noindex: