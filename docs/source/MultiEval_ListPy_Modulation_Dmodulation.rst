Dmodulation
----------------------------------------



.. rubric:: SCPI Command :

.. index::
	single: FETCh:LTE:MEASurement<Instance>:MEValuation:LIST:MODulation:DMODulation

.. code-block:: python

	FETCh:LTE:MEASurement<Instance>:MEValuation:LIST:MODulation:DMODulation



.. autoclass:: RsCmwLteMeas.Implementations.MultiEval.ListPy.Modulation.Dmodulation.DmodulationCls
	:members:
	:undoc-members:
	:noindex: