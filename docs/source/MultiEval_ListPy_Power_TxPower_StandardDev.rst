StandardDev
----------------------------------------



.. rubric:: SCPI Command :

.. index::
	single: FETCh:LTE:MEASurement<Instance>:MEValuation:LIST:POWer:TXPower:SDEViation

.. code-block:: python

	FETCh:LTE:MEASurement<Instance>:MEValuation:LIST:POWer:TXPower:SDEViation



.. autoclass:: RsCmwLteMeas.Implementations.MultiEval.ListPy.Power.TxPower.StandardDev.StandardDevCls
	:members:
	:undoc-members:
	:noindex: