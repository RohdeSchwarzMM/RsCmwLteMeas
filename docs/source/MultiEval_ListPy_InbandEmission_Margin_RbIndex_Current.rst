Current
----------------------------------------



.. rubric:: SCPI Command :

.. index::
	single: FETCh:LTE:MEASurement<Instance>:MEValuation:LIST:IEMission:MARGin:RBINdex:CURRent

.. code-block:: python

	FETCh:LTE:MEASurement<Instance>:MEValuation:LIST:IEMission:MARGin:RBINdex:CURRent



.. autoclass:: RsCmwLteMeas.Implementations.MultiEval.ListPy.InbandEmission.Margin.RbIndex.Current.CurrentCls
	:members:
	:undoc-members:
	:noindex: