Minimum
----------------------------------------



.. rubric:: SCPI Commands :

.. index::
	single: FETCh:LTE:MEASurement<Instance>:MEValuation:LIST:MODulation:TPOWer:MINimum
	single: CALCulate:LTE:MEASurement<Instance>:MEValuation:LIST:MODulation:TPOWer:MINimum

.. code-block:: python

	FETCh:LTE:MEASurement<Instance>:MEValuation:LIST:MODulation:TPOWer:MINimum
	CALCulate:LTE:MEASurement<Instance>:MEValuation:LIST:MODulation:TPOWer:MINimum



.. autoclass:: RsCmwLteMeas.Implementations.MultiEval.ListPy.Modulation.Tpower.Minimum.MinimumCls
	:members:
	:undoc-members:
	:noindex: