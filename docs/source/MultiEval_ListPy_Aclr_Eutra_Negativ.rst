Negativ
----------------------------------------





.. autoclass:: RsCmwLteMeas.Implementations.MultiEval.ListPy.Aclr.Eutra.Negativ.NegativCls
	:members:
	:undoc-members:
	:noindex:


.. rubric:: Cloning the Group

.. code-block:: python

	# Create a clone of the original group, that exists independently
	group2 = driver.multiEval.listPy.aclr.eutra.negativ.clone()



.. rubric:: Subgroups
.. toctree::
	:maxdepth: 6
	:glob:

	MultiEval_ListPy_Aclr_Eutra_Negativ_Average.rst
	MultiEval_ListPy_Aclr_Eutra_Negativ_Current.rst