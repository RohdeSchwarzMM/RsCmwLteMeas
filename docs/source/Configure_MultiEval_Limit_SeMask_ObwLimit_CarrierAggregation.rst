CarrierAggregation
----------------------------------------



.. rubric:: SCPI Command :

.. index::
	single: CONFigure:LTE:MEASurement<Instance>:MEValuation:LIMit:SEMask:OBWLimit:CAGGregation:OCOMbination

.. code-block:: python

	CONFigure:LTE:MEASurement<Instance>:MEValuation:LIMit:SEMask:OBWLimit:CAGGregation:OCOMbination



.. autoclass:: RsCmwLteMeas.Implementations.Configure.MultiEval.Limit.SeMask.ObwLimit.CarrierAggregation.CarrierAggregationCls
	:members:
	:undoc-members:
	:noindex:


.. rubric:: Cloning the Group

.. code-block:: python

	# Create a clone of the original group, that exists independently
	group2 = driver.configure.multiEval.limit.seMask.obwLimit.carrierAggregation.clone()



.. rubric:: Subgroups
.. toctree::
	:maxdepth: 6
	:glob:

	Configure_MultiEval_Limit_SeMask_ObwLimit_CarrierAggregation_ChannelBw1st.rst