Sidelink
----------------------------------------



.. rubric:: SCPI Command :

.. index::
	single: CONFigure:LTE:MEASurement<Instance>:MEValuation:LIST:SEGMent<nr>:RBALlocation:SIDelink

.. code-block:: python

	CONFigure:LTE:MEASurement<Instance>:MEValuation:LIST:SEGMent<nr>:RBALlocation:SIDelink



.. autoclass:: RsCmwLteMeas.Implementations.Configure.MultiEval.ListPy.Segment.RbAllocation.Sidelink.SidelinkCls
	:members:
	:undoc-members:
	:noindex: