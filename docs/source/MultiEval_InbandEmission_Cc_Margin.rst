Margin
----------------------------------------





.. autoclass:: RsCmwLteMeas.Implementations.MultiEval.InbandEmission.Cc.Margin.MarginCls
	:members:
	:undoc-members:
	:noindex:


.. rubric:: Cloning the Group

.. code-block:: python

	# Create a clone of the original group, that exists independently
	group2 = driver.multiEval.inbandEmission.cc.margin.clone()



.. rubric:: Subgroups
.. toctree::
	:maxdepth: 6
	:glob:

	MultiEval_InbandEmission_Cc_Margin_Average.rst
	MultiEval_InbandEmission_Cc_Margin_Current.rst
	MultiEval_InbandEmission_Cc_Margin_Extreme.rst
	MultiEval_InbandEmission_Cc_Margin_StandardDev.rst