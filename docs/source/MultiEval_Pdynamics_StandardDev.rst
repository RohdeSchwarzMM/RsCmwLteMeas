StandardDev
----------------------------------------



.. rubric:: SCPI Commands :

.. index::
	single: READ:LTE:MEASurement<Instance>:MEValuation:PDYNamics:SDEViation
	single: FETCh:LTE:MEASurement<Instance>:MEValuation:PDYNamics:SDEViation

.. code-block:: python

	READ:LTE:MEASurement<Instance>:MEValuation:PDYNamics:SDEViation
	FETCh:LTE:MEASurement<Instance>:MEValuation:PDYNamics:SDEViation



.. autoclass:: RsCmwLteMeas.Implementations.MultiEval.Pdynamics.StandardDev.StandardDevCls
	:members:
	:undoc-members:
	:noindex: