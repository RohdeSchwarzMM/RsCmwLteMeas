Current
----------------------------------------



.. rubric:: SCPI Commands :

.. index::
	single: FETCh:LTE:MEASurement<Instance>:MEValuation:LIST:MODulation:PPOWer:CURRent
	single: CALCulate:LTE:MEASurement<Instance>:MEValuation:LIST:MODulation:PPOWer:CURRent

.. code-block:: python

	FETCh:LTE:MEASurement<Instance>:MEValuation:LIST:MODulation:PPOWer:CURRent
	CALCulate:LTE:MEASurement<Instance>:MEValuation:LIST:MODulation:PPOWer:CURRent



.. autoclass:: RsCmwLteMeas.Implementations.MultiEval.ListPy.Modulation.Ppower.Current.CurrentCls
	:members:
	:undoc-members:
	:noindex: