AtTolerance<EutraBand>
----------------------------------------

.. rubric:: RepCap Settings

.. code-block:: python

	# Range: Nr30 .. Nr50
	rc = driver.configure.multiEval.limit.seMask.atTolerance.repcap_eutraBand_get()
	driver.configure.multiEval.limit.seMask.atTolerance.repcap_eutraBand_set(repcap.EutraBand.Nr30)



.. rubric:: SCPI Command :

.. index::
	single: CONFigure:LTE:MEASurement<Instance>:MEValuation:LIMit:SEMask:ATTolerance<EUTRAband>

.. code-block:: python

	CONFigure:LTE:MEASurement<Instance>:MEValuation:LIMit:SEMask:ATTolerance<EUTRAband>



.. autoclass:: RsCmwLteMeas.Implementations.Configure.MultiEval.Limit.SeMask.AtTolerance.AtToleranceCls
	:members:
	:undoc-members:
	:noindex:


.. rubric:: Cloning the Group

.. code-block:: python

	# Create a clone of the original group, that exists independently
	group2 = driver.configure.multiEval.limit.seMask.atTolerance.clone()