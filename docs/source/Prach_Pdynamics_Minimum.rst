Minimum
----------------------------------------



.. rubric:: SCPI Commands :

.. index::
	single: READ:LTE:MEASurement<Instance>:PRACh:PDYNamics:MINimum
	single: FETCh:LTE:MEASurement<Instance>:PRACh:PDYNamics:MINimum
	single: CALCulate:LTE:MEASurement<Instance>:PRACh:PDYNamics:MINimum

.. code-block:: python

	READ:LTE:MEASurement<Instance>:PRACh:PDYNamics:MINimum
	FETCh:LTE:MEASurement<Instance>:PRACh:PDYNamics:MINimum
	CALCulate:LTE:MEASurement<Instance>:PRACh:PDYNamics:MINimum



.. autoclass:: RsCmwLteMeas.Implementations.Prach.Pdynamics.Minimum.MinimumCls
	:members:
	:undoc-members:
	:noindex: