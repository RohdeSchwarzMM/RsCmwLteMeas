Merror
----------------------------------------



.. rubric:: SCPI Command :

.. index::
	single: FETCh:LTE:MEASurement<Instance>:MEValuation:AMARker<No>:MERRor

.. code-block:: python

	FETCh:LTE:MEASurement<Instance>:MEValuation:AMARker<No>:MERRor



.. autoclass:: RsCmwLteMeas.Implementations.MultiEval.Amarker.Merror.MerrorCls
	:members:
	:undoc-members:
	:noindex: