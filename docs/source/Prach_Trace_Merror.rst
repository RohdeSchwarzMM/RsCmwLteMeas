Merror
----------------------------------------





.. autoclass:: RsCmwLteMeas.Implementations.Prach.Trace.Merror.MerrorCls
	:members:
	:undoc-members:
	:noindex:


.. rubric:: Cloning the Group

.. code-block:: python

	# Create a clone of the original group, that exists independently
	group2 = driver.prach.trace.merror.clone()



.. rubric:: Subgroups
.. toctree::
	:maxdepth: 6
	:glob:

	Prach_Trace_Merror_Average.rst
	Prach_Trace_Merror_Current.rst
	Prach_Trace_Merror_Maximum.rst