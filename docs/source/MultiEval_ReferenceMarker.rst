ReferenceMarker
----------------------------------------





.. autoclass:: RsCmwLteMeas.Implementations.MultiEval.ReferenceMarker.ReferenceMarkerCls
	:members:
	:undoc-members:
	:noindex:


.. rubric:: Cloning the Group

.. code-block:: python

	# Create a clone of the original group, that exists independently
	group2 = driver.multiEval.referenceMarker.clone()



.. rubric:: Subgroups
.. toctree::
	:maxdepth: 6
	:glob:

	MultiEval_ReferenceMarker_EvMagnitude.rst
	MultiEval_ReferenceMarker_Merror.rst
	MultiEval_ReferenceMarker_Pdynamics.rst
	MultiEval_ReferenceMarker_Perror.rst
	MultiEval_ReferenceMarker_Pmonitor.rst