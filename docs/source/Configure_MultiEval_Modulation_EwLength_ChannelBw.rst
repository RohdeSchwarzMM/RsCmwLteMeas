ChannelBw<ChannelBw>
----------------------------------------

.. rubric:: RepCap Settings

.. code-block:: python

	# Range: Bw14 .. Bw200
	rc = driver.configure.multiEval.modulation.ewLength.channelBw.repcap_channelBw_get()
	driver.configure.multiEval.modulation.ewLength.channelBw.repcap_channelBw_set(repcap.ChannelBw.Bw14)



.. rubric:: SCPI Command :

.. index::
	single: CONFigure:LTE:MEASurement<Instance>:MEValuation:MODulation:EWLength:CBANdwidth<Band>

.. code-block:: python

	CONFigure:LTE:MEASurement<Instance>:MEValuation:MODulation:EWLength:CBANdwidth<Band>



.. autoclass:: RsCmwLteMeas.Implementations.Configure.MultiEval.Modulation.EwLength.ChannelBw.ChannelBwCls
	:members:
	:undoc-members:
	:noindex:


.. rubric:: Cloning the Group

.. code-block:: python

	# Create a clone of the original group, that exists independently
	group2 = driver.configure.multiEval.modulation.ewLength.channelBw.clone()