EvMagnitude
----------------------------------------



.. rubric:: SCPI Command :

.. index::
	single: CONFigure:LTE:MEASurement<Instance>:PRACh:LIMit:EVMagnitude

.. code-block:: python

	CONFigure:LTE:MEASurement<Instance>:PRACh:LIMit:EVMagnitude



.. autoclass:: RsCmwLteMeas.Implementations.Configure.Prach.Limit.EvMagnitude.EvMagnitudeCls
	:members:
	:undoc-members:
	:noindex: