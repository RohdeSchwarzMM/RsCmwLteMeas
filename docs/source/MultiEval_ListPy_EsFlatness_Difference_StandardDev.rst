StandardDev
----------------------------------------



.. rubric:: SCPI Command :

.. index::
	single: FETCh:LTE:MEASurement<Instance>:MEValuation:LIST:ESFLatness:DIFFerence<nr>:SDEViation

.. code-block:: python

	FETCh:LTE:MEASurement<Instance>:MEValuation:LIST:ESFLatness:DIFFerence<nr>:SDEViation



.. autoclass:: RsCmwLteMeas.Implementations.MultiEval.ListPy.EsFlatness.Difference.StandardDev.StandardDevCls
	:members:
	:undoc-members:
	:noindex: