StandardDev
----------------------------------------



.. rubric:: SCPI Command :

.. index::
	single: FETCh:LTE:MEASurement<Instance>:MEValuation:LIST:SEGMent<nr>:IEMission:CC<c>:MARGin:SDEViation

.. code-block:: python

	FETCh:LTE:MEASurement<Instance>:MEValuation:LIST:SEGMent<nr>:IEMission:CC<c>:MARGin:SDEViation



.. autoclass:: RsCmwLteMeas.Implementations.MultiEval.ListPy.Segment.InbandEmission.Cc.Margin.StandardDev.StandardDevCls
	:members:
	:undoc-members:
	:noindex: