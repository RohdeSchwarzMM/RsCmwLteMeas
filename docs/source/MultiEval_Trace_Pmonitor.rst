Pmonitor
----------------------------------------





.. autoclass:: RsCmwLteMeas.Implementations.MultiEval.Trace.Pmonitor.PmonitorCls
	:members:
	:undoc-members:
	:noindex:


.. rubric:: Cloning the Group

.. code-block:: python

	# Create a clone of the original group, that exists independently
	group2 = driver.multiEval.trace.pmonitor.clone()



.. rubric:: Subgroups
.. toctree::
	:maxdepth: 6
	:glob:

	MultiEval_Trace_Pmonitor_Cc.rst