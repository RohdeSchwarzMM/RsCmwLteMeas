Bler
----------------------------------------





.. autoclass:: RsCmwLteMeas.Implementations.Configure.MultiEval.Bler.BlerCls
	:members:
	:undoc-members:
	:noindex:


.. rubric:: Cloning the Group

.. code-block:: python

	# Create a clone of the original group, that exists independently
	group2 = driver.configure.multiEval.bler.clone()



.. rubric:: Subgroups
.. toctree::
	:maxdepth: 6
	:glob:

	Configure_MultiEval_Bler_Sframes.rst