Ocombination
----------------------------------------



.. rubric:: SCPI Command :

.. index::
	single: CONFigure:LTE:MEASurement<Instance>:MEValuation:LIMit:ACLR:UTRA<nr>:CAGGregation:OCOMbination

.. code-block:: python

	CONFigure:LTE:MEASurement<Instance>:MEValuation:LIMit:ACLR:UTRA<nr>:CAGGregation:OCOMbination



.. autoclass:: RsCmwLteMeas.Implementations.Configure.MultiEval.Limit.Aclr.Utra.CarrierAggregation.Ocombination.OcombinationCls
	:members:
	:undoc-members:
	:noindex: