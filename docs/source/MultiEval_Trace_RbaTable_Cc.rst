Cc<CarrierComponent>
----------------------------------------

.. rubric:: RepCap Settings

.. code-block:: python

	# Range: Nr1 .. Nr4
	rc = driver.multiEval.trace.rbaTable.cc.repcap_carrierComponent_get()
	driver.multiEval.trace.rbaTable.cc.repcap_carrierComponent_set(repcap.CarrierComponent.Nr1)



.. rubric:: SCPI Commands :

.. index::
	single: READ:LTE:MEASurement<Instance>:MEValuation:TRACe:RBATable:CC<Nr>
	single: FETCh:LTE:MEASurement<Instance>:MEValuation:TRACe:RBATable:CC<Nr>

.. code-block:: python

	READ:LTE:MEASurement<Instance>:MEValuation:TRACe:RBATable:CC<Nr>
	FETCh:LTE:MEASurement<Instance>:MEValuation:TRACe:RBATable:CC<Nr>



.. autoclass:: RsCmwLteMeas.Implementations.MultiEval.Trace.RbaTable.Cc.CcCls
	:members:
	:undoc-members:
	:noindex:


.. rubric:: Cloning the Group

.. code-block:: python

	# Create a clone of the original group, that exists independently
	group2 = driver.multiEval.trace.rbaTable.cc.clone()