ScIndex
----------------------------------------





.. autoclass:: RsCmwLteMeas.Implementations.MultiEval.ListPy.EsFlatness.ScIndex.ScIndexCls
	:members:
	:undoc-members:
	:noindex:


.. rubric:: Cloning the Group

.. code-block:: python

	# Create a clone of the original group, that exists independently
	group2 = driver.multiEval.listPy.esFlatness.scIndex.clone()



.. rubric:: Subgroups
.. toctree::
	:maxdepth: 6
	:glob:

	MultiEval_ListPy_EsFlatness_ScIndex_Maximum.rst
	MultiEval_ListPy_EsFlatness_ScIndex_Minimum.rst