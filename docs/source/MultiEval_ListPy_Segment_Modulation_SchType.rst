SchType
----------------------------------------



.. rubric:: SCPI Command :

.. index::
	single: FETCh:LTE:MEASurement<Instance>:MEValuation:LIST:SEGMent<nr>:MODulation:SCHType

.. code-block:: python

	FETCh:LTE:MEASurement<Instance>:MEValuation:LIST:SEGMent<nr>:MODulation:SCHType



.. autoclass:: RsCmwLteMeas.Implementations.MultiEval.ListPy.Segment.Modulation.SchType.SchTypeCls
	:members:
	:undoc-members:
	:noindex: