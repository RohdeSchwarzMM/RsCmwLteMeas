Minimum
----------------------------------------



.. rubric:: SCPI Commands :

.. index::
	single: READ:LTE:MEASurement<Instance>:MEValuation:PMONitor:CC<Nr>:MINimum
	single: FETCh:LTE:MEASurement<Instance>:MEValuation:PMONitor:CC<Nr>:MINimum

.. code-block:: python

	READ:LTE:MEASurement<Instance>:MEValuation:PMONitor:CC<Nr>:MINimum
	FETCh:LTE:MEASurement<Instance>:MEValuation:PMONitor:CC<Nr>:MINimum



.. autoclass:: RsCmwLteMeas.Implementations.MultiEval.Pmonitor.Cc.Minimum.MinimumCls
	:members:
	:undoc-members:
	:noindex: