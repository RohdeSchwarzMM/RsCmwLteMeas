StandardDev
----------------------------------------



.. rubric:: SCPI Commands :

.. index::
	single: READ:LTE:MEASurement<Instance>:SRS:PDYNamics:SDEViation
	single: FETCh:LTE:MEASurement<Instance>:SRS:PDYNamics:SDEViation

.. code-block:: python

	READ:LTE:MEASurement<Instance>:SRS:PDYNamics:SDEViation
	FETCh:LTE:MEASurement<Instance>:SRS:PDYNamics:SDEViation



.. autoclass:: RsCmwLteMeas.Implementations.Srs.Pdynamics.StandardDev.StandardDevCls
	:members:
	:undoc-members:
	:noindex: