Sframes
----------------------------------------



.. rubric:: SCPI Command :

.. index::
	single: CONFigure:LTE:MEASurement<Instance>:MEValuation:BLER:SFRames

.. code-block:: python

	CONFigure:LTE:MEASurement<Instance>:MEValuation:BLER:SFRames



.. autoclass:: RsCmwLteMeas.Implementations.Configure.MultiEval.Bler.Sframes.SframesCls
	:members:
	:undoc-members:
	:noindex: