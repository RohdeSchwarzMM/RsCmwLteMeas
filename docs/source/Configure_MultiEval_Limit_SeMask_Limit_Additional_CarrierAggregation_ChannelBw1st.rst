ChannelBw1st<FirstChannelBw>
----------------------------------------

.. rubric:: RepCap Settings

.. code-block:: python

	# Range: Bw100 .. Bw200
	rc = driver.configure.multiEval.limit.seMask.limit.additional.carrierAggregation.channelBw1st.repcap_firstChannelBw_get()
	driver.configure.multiEval.limit.seMask.limit.additional.carrierAggregation.channelBw1st.repcap_firstChannelBw_set(repcap.FirstChannelBw.Bw100)





.. autoclass:: RsCmwLteMeas.Implementations.Configure.MultiEval.Limit.SeMask.Limit.Additional.CarrierAggregation.ChannelBw1st.ChannelBw1stCls
	:members:
	:undoc-members:
	:noindex:


.. rubric:: Cloning the Group

.. code-block:: python

	# Create a clone of the original group, that exists independently
	group2 = driver.configure.multiEval.limit.seMask.limit.additional.carrierAggregation.channelBw1st.clone()



.. rubric:: Subgroups
.. toctree::
	:maxdepth: 6
	:glob:

	Configure_MultiEval_Limit_SeMask_Limit_Additional_CarrierAggregation_ChannelBw1st_ChannelBw2nd.rst