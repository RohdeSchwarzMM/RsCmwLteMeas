Perror
----------------------------------------



.. rubric:: SCPI Command :

.. index::
	single: CONFigure:LTE:MEASurement<Instance>:MEValuation:LIMit:QAM<ModOrder>:PERRor

.. code-block:: python

	CONFigure:LTE:MEASurement<Instance>:MEValuation:LIMit:QAM<ModOrder>:PERRor



.. autoclass:: RsCmwLteMeas.Implementations.Configure.MultiEval.Limit.Qam.Perror.PerrorCls
	:members:
	:undoc-members:
	:noindex: