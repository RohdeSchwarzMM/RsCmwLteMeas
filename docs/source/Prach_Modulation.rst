Modulation
----------------------------------------





.. autoclass:: RsCmwLteMeas.Implementations.Prach.Modulation.ModulationCls
	:members:
	:undoc-members:
	:noindex:


.. rubric:: Cloning the Group

.. code-block:: python

	# Create a clone of the original group, that exists independently
	group2 = driver.prach.modulation.clone()



.. rubric:: Subgroups
.. toctree::
	:maxdepth: 6
	:glob:

	Prach_Modulation_Average.rst
	Prach_Modulation_Current.rst
	Prach_Modulation_DpfOffset.rst
	Prach_Modulation_DsIndex.rst
	Prach_Modulation_Extreme.rst
	Prach_Modulation_Nsymbol.rst
	Prach_Modulation_Preamble.rst
	Prach_Modulation_Scorrelation.rst
	Prach_Modulation_StandardDev.rst