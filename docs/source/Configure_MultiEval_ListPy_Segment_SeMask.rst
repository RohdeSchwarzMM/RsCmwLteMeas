SeMask
----------------------------------------



.. rubric:: SCPI Command :

.. index::
	single: CONFigure:LTE:MEASurement<Instance>:MEValuation:LIST:SEGMent<nr>:SEMask

.. code-block:: python

	CONFigure:LTE:MEASurement<Instance>:MEValuation:LIST:SEGMent<nr>:SEMask



.. autoclass:: RsCmwLteMeas.Implementations.Configure.MultiEval.ListPy.Segment.SeMask.SeMaskCls
	:members:
	:undoc-members:
	:noindex: