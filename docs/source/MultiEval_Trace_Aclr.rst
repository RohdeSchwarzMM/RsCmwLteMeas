Aclr
----------------------------------------





.. autoclass:: RsCmwLteMeas.Implementations.MultiEval.Trace.Aclr.AclrCls
	:members:
	:undoc-members:
	:noindex:


.. rubric:: Cloning the Group

.. code-block:: python

	# Create a clone of the original group, that exists independently
	group2 = driver.multiEval.trace.aclr.clone()



.. rubric:: Subgroups
.. toctree::
	:maxdepth: 6
	:glob:

	MultiEval_Trace_Aclr_Average.rst
	MultiEval_Trace_Aclr_Current.rst