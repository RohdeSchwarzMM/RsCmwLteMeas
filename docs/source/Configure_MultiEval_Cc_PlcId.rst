PlcId
----------------------------------------



.. rubric:: SCPI Command :

.. index::
	single: CONFigure:LTE:MEASurement<Instance>:MEValuation:CC<Nr>:PLCid

.. code-block:: python

	CONFigure:LTE:MEASurement<Instance>:MEValuation:CC<Nr>:PLCid



.. autoclass:: RsCmwLteMeas.Implementations.Configure.MultiEval.Cc.PlcId.PlcIdCls
	:members:
	:undoc-members:
	:noindex: