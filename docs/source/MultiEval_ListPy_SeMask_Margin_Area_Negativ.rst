Negativ
----------------------------------------





.. autoclass:: RsCmwLteMeas.Implementations.MultiEval.ListPy.SeMask.Margin.Area.Negativ.NegativCls
	:members:
	:undoc-members:
	:noindex:


.. rubric:: Cloning the Group

.. code-block:: python

	# Create a clone of the original group, that exists independently
	group2 = driver.multiEval.listPy.seMask.margin.area.negativ.clone()



.. rubric:: Subgroups
.. toctree::
	:maxdepth: 6
	:glob:

	MultiEval_ListPy_SeMask_Margin_Area_Negativ_Average.rst
	MultiEval_ListPy_SeMask_Margin_Area_Negativ_Current.rst
	MultiEval_ListPy_SeMask_Margin_Area_Negativ_Minimum.rst