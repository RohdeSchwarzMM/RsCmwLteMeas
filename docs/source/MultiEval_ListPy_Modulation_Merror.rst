Merror
----------------------------------------





.. autoclass:: RsCmwLteMeas.Implementations.MultiEval.ListPy.Modulation.Merror.MerrorCls
	:members:
	:undoc-members:
	:noindex:


.. rubric:: Cloning the Group

.. code-block:: python

	# Create a clone of the original group, that exists independently
	group2 = driver.multiEval.listPy.modulation.merror.clone()



.. rubric:: Subgroups
.. toctree::
	:maxdepth: 6
	:glob:

	MultiEval_ListPy_Modulation_Merror_Dmrs.rst
	MultiEval_ListPy_Modulation_Merror_Peak.rst
	MultiEval_ListPy_Modulation_Merror_Rms.rst