Maximum
----------------------------------------



.. rubric:: SCPI Commands :

.. index::
	single: READ:LTE:MEASurement<Instance>:MEValuation:TRACe:PDYNamics:MAXimum
	single: FETCh:LTE:MEASurement<Instance>:MEValuation:TRACe:PDYNamics:MAXimum

.. code-block:: python

	READ:LTE:MEASurement<Instance>:MEValuation:TRACe:PDYNamics:MAXimum
	FETCh:LTE:MEASurement<Instance>:MEValuation:TRACe:PDYNamics:MAXimum



.. autoclass:: RsCmwLteMeas.Implementations.MultiEval.Trace.Pdynamics.Maximum.MaximumCls
	:members:
	:undoc-members:
	:noindex: