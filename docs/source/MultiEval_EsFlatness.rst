EsFlatness
----------------------------------------





.. autoclass:: RsCmwLteMeas.Implementations.MultiEval.EsFlatness.EsFlatnessCls
	:members:
	:undoc-members:
	:noindex:


.. rubric:: Cloning the Group

.. code-block:: python

	# Create a clone of the original group, that exists independently
	group2 = driver.multiEval.esFlatness.clone()



.. rubric:: Subgroups
.. toctree::
	:maxdepth: 6
	:glob:

	MultiEval_EsFlatness_Average.rst
	MultiEval_EsFlatness_Current.rst
	MultiEval_EsFlatness_Extreme.rst
	MultiEval_EsFlatness_StandardDev.rst