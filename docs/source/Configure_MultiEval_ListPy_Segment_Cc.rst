Cc<CarrierComponent>
----------------------------------------

.. rubric:: RepCap Settings

.. code-block:: python

	# Range: Nr1 .. Nr4
	rc = driver.configure.multiEval.listPy.segment.cc.repcap_carrierComponent_get()
	driver.configure.multiEval.listPy.segment.cc.repcap_carrierComponent_set(repcap.CarrierComponent.Nr1)



.. rubric:: SCPI Command :

.. index::
	single: CONFigure:LTE:MEASurement<Instance>:MEValuation:LIST:SEGMent<nr>:CC<c>

.. code-block:: python

	CONFigure:LTE:MEASurement<Instance>:MEValuation:LIST:SEGMent<nr>:CC<c>



.. autoclass:: RsCmwLteMeas.Implementations.Configure.MultiEval.ListPy.Segment.Cc.CcCls
	:members:
	:undoc-members:
	:noindex:


.. rubric:: Cloning the Group

.. code-block:: python

	# Create a clone of the original group, that exists independently
	group2 = driver.configure.multiEval.listPy.segment.cc.clone()