Maximum
----------------------------------------



.. rubric:: SCPI Commands :

.. index::
	single: READ:LTE:MEASurement<Instance>:MEValuation:PMONitor:CC<Nr>:MAXimum
	single: FETCh:LTE:MEASurement<Instance>:MEValuation:PMONitor:CC<Nr>:MAXimum

.. code-block:: python

	READ:LTE:MEASurement<Instance>:MEValuation:PMONitor:CC<Nr>:MAXimum
	FETCh:LTE:MEASurement<Instance>:MEValuation:PMONitor:CC<Nr>:MAXimum



.. autoclass:: RsCmwLteMeas.Implementations.MultiEval.Pmonitor.Cc.Maximum.MaximumCls
	:members:
	:undoc-members:
	:noindex: