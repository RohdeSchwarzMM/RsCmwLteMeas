SeMask
----------------------------------------





.. autoclass:: RsCmwLteMeas.Implementations.MultiEval.SeMask.SeMaskCls
	:members:
	:undoc-members:
	:noindex:


.. rubric:: Cloning the Group

.. code-block:: python

	# Create a clone of the original group, that exists independently
	group2 = driver.multiEval.seMask.clone()



.. rubric:: Subgroups
.. toctree::
	:maxdepth: 6
	:glob:

	MultiEval_SeMask_Average.rst
	MultiEval_SeMask_Current.rst
	MultiEval_SeMask_Dallocation.rst
	MultiEval_SeMask_DchType.rst
	MultiEval_SeMask_Extreme.rst
	MultiEval_SeMask_Margin.rst
	MultiEval_SeMask_StandardDev.rst