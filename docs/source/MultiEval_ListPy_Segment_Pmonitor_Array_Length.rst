Length
----------------------------------------



.. rubric:: SCPI Command :

.. index::
	single: FETCh:LTE:MEASurement<Instance>:MEValuation:LIST:SEGMent<nr>:PMONitor:ARRay:LENGth

.. code-block:: python

	FETCh:LTE:MEASurement<Instance>:MEValuation:LIST:SEGMent<nr>:PMONitor:ARRay:LENGth



.. autoclass:: RsCmwLteMeas.Implementations.MultiEval.ListPy.Segment.Pmonitor.Array.Length.LengthCls
	:members:
	:undoc-members:
	:noindex: