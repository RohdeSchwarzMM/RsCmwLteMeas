Peak
----------------------------------------



.. rubric:: SCPI Command :

.. index::
	single: FETCh:LTE:MEASurement<Instance>:MEValuation:AMARker<No>:EVMagnitude:PEAK

.. code-block:: python

	FETCh:LTE:MEASurement<Instance>:MEValuation:AMARker<No>:EVMagnitude:PEAK



.. autoclass:: RsCmwLteMeas.Implementations.MultiEval.Amarker.EvMagnitude.Peak.PeakCls
	:members:
	:undoc-members:
	:noindex: