Aggregated
----------------------------------------



.. rubric:: SCPI Commands :

.. index::
	single: CONFigure:LTE:MEASurement<Instance>:CAGGregation:FREQuency:AGGRegated:LOW
	single: CONFigure:LTE:MEASurement<Instance>:CAGGregation:FREQuency:AGGRegated:CENTer
	single: CONFigure:LTE:MEASurement<Instance>:CAGGregation:FREQuency:AGGRegated:HIGH

.. code-block:: python

	CONFigure:LTE:MEASurement<Instance>:CAGGregation:FREQuency:AGGRegated:LOW
	CONFigure:LTE:MEASurement<Instance>:CAGGregation:FREQuency:AGGRegated:CENTer
	CONFigure:LTE:MEASurement<Instance>:CAGGregation:FREQuency:AGGRegated:HIGH



.. autoclass:: RsCmwLteMeas.Implementations.Configure.CarrierAggregation.Frequency.Aggregated.AggregatedCls
	:members:
	:undoc-members:
	:noindex: