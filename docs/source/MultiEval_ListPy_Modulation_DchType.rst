DchType
----------------------------------------



.. rubric:: SCPI Command :

.. index::
	single: FETCh:LTE:MEASurement<Instance>:MEValuation:LIST:MODulation:DCHType

.. code-block:: python

	FETCh:LTE:MEASurement<Instance>:MEValuation:LIST:MODulation:DCHType



.. autoclass:: RsCmwLteMeas.Implementations.MultiEval.ListPy.Modulation.DchType.DchTypeCls
	:members:
	:undoc-members:
	:noindex: