Network
----------------------------------------



.. rubric:: SCPI Commands :

.. index::
	single: CONFigure:LTE:MEASurement<Instance>:NETWork:RFPSharing
	single: CONFigure:LTE:MEASurement<Instance>:NETWork:DMODe

.. code-block:: python

	CONFigure:LTE:MEASurement<Instance>:NETWork:RFPSharing
	CONFigure:LTE:MEASurement<Instance>:NETWork:DMODe



.. autoclass:: RsCmwLteMeas.Implementations.Configure.Network.NetworkCls
	:members:
	:undoc-members:
	:noindex: