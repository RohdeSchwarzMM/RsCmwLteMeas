StandardDev
----------------------------------------



.. rubric:: SCPI Command :

.. index::
	single: FETCh:LTE:MEASurement<Instance>:MEValuation:LIST:MODulation:EVM:DMRS:HIGH:SDEViation

.. code-block:: python

	FETCh:LTE:MEASurement<Instance>:MEValuation:LIST:MODulation:EVM:DMRS:HIGH:SDEViation



.. autoclass:: RsCmwLteMeas.Implementations.MultiEval.ListPy.Modulation.Evm.Dmrs.High.StandardDev.StandardDevCls
	:members:
	:undoc-members:
	:noindex: