Merror
----------------------------------------



.. rubric:: SCPI Command :

.. index::
	single: CONFigure:LTE:MEASurement<Instance>:MEValuation:LIMit:QAM<ModOrder>:MERRor

.. code-block:: python

	CONFigure:LTE:MEASurement<Instance>:MEValuation:LIMit:QAM<ModOrder>:MERRor



.. autoclass:: RsCmwLteMeas.Implementations.Configure.MultiEval.Limit.Qam.Merror.MerrorCls
	:members:
	:undoc-members:
	:noindex: