Power
----------------------------------------



.. rubric:: SCPI Command :

.. index::
	single: CONFigure:LTE:MEASurement<Instance>:MEValuation:POWer:HDMode

.. code-block:: python

	CONFigure:LTE:MEASurement<Instance>:MEValuation:POWer:HDMode



.. autoclass:: RsCmwLteMeas.Implementations.Configure.MultiEval.Power.PowerCls
	:members:
	:undoc-members:
	:noindex: