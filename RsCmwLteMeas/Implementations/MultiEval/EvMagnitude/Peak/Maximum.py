from typing import List

from .....Internal.Core import Core
from .....Internal.CommandsGroup import CommandsGroup
from .....Internal.Types import DataType
from .....Internal.StructBase import StructBase
from .....Internal.ArgStruct import ArgStruct


# noinspection PyPep8Naming,PyAttributeOutsideInit,SpellCheckingInspection
class MaximumCls:
	"""Maximum commands group definition. 2 total commands, 0 Subgroups, 2 group commands"""

	def __init__(self, core: Core, parent):
		self._core = core
		self._cmd_group = CommandsGroup("maximum", core, parent)

	# noinspection PyTypeChecker
	class ResultData(StructBase):
		"""Response structure. Fields: \n
			- Reliability: int: decimal 'Reliability indicator'
			- Low: List[float]: float EVM value for low EVM window position Unit: %
			- High: List[float]: float EVM value for high EVM window position Unit: %"""
		__meta_args_list = [
			ArgStruct.scalar_int('Reliability', 'Reliability'),
			ArgStruct('Low', DataType.FloatList, None, False, True, 1),
			ArgStruct('High', DataType.FloatList, None, False, True, 1)]

		def __init__(self):
			StructBase.__init__(self, self)
			self.Reliability: int = None
			self.Low: List[float] = None
			self.High: List[float] = None

	def read(self) -> ResultData:
		"""SCPI: READ:LTE:MEASurement<Instance>:MEValuation:EVMagnitude:PEAK:MAXimum \n
		Snippet: value: ResultData = driver.multiEval.evMagnitude.peak.maximum.read() \n
		Returns the values of the EVM peak bar graphs for the SC-FDMA symbols in the measured slot. The results of the current,
		average and maximum bar graphs can be retrieved. There is one pair of EVM values per SC-FDMA symbol, returned in the
		following order: <Reliability>, {<Low>, <High>}symbol 0, {<Low>, <High>}symbol 1, ... See also 'View EVM'. \n
			:return: structure: for return value, see the help for ResultData structure arguments."""
		return self._core.io.query_struct(f'READ:LTE:MEASurement<Instance>:MEValuation:EVMagnitude:PEAK:MAXimum?', self.__class__.ResultData())

	def fetch(self) -> ResultData:
		"""SCPI: FETCh:LTE:MEASurement<Instance>:MEValuation:EVMagnitude:PEAK:MAXimum \n
		Snippet: value: ResultData = driver.multiEval.evMagnitude.peak.maximum.fetch() \n
		Returns the values of the EVM peak bar graphs for the SC-FDMA symbols in the measured slot. The results of the current,
		average and maximum bar graphs can be retrieved. There is one pair of EVM values per SC-FDMA symbol, returned in the
		following order: <Reliability>, {<Low>, <High>}symbol 0, {<Low>, <High>}symbol 1, ... See also 'View EVM'. \n
			:return: structure: for return value, see the help for ResultData structure arguments."""
		return self._core.io.query_struct(f'FETCh:LTE:MEASurement<Instance>:MEValuation:EVMagnitude:PEAK:MAXimum?', self.__class__.ResultData())
